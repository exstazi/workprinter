#ifndef INDICATOR_H_
#define INDICATOR_H_

namespace Indicator
{
  
  uint8_t seg2[11] = { 0x10, 0x76, 0x0C, 0x44, 0x62, 0x41, 0x01, 0x74, 0x00, 0x40, 0xFF };
  uint8_t seg3[11] = { 0x10, 0x73, 0x09, 0x41, 0x62, 0x44, 0x04, 0x71, 0x00, 0x40, 0xFF };
  
  
  void init()
  {
    DDRB = 0xFF;
    DDRA = 0xFF;
    DDRC = 0xFF;
    PORTB = 0xFF;
    PORTA = 0xFF;
    PORTC = 0xFF;     
  }
  
  void setDigitOnSegment(uint8_t nSeg, uint8_t digit)
  {
    if (nSeg == 0) {
      if (digit == 1) {
        PORTB &= 0b11100111;
      } else {
        PORTB |= 0b00011000;
      } 
    } else if (nSeg == 1) {
      PORTC = ((seg2[digit] << 1) & 0xF0) | (PORTC & 0x0F);
      PORTB = (seg2[digit] & 0x07) + (PORTB & 0xF8);
    } else if (nSeg == 2) {
      PORTA = (seg3[digit] & 0x7F) | (PORTA & 0x80);      
    }
  }
  
  void setDigit(uint8_t value)
  {
    setDigitOnSegment(0, 10);
    setDigitOnSegment(1, 10);
    uint8_t nSeg = 2;
    do
    {
      setDigitOnSegment(nSeg, value % 10);
      value /= 10;
      --nSeg;
    } while ( value );
  }
  
  void setValueSegment(uint8_t nSeg, uint8_t value)
  {
      
  }
  
};

#endif
