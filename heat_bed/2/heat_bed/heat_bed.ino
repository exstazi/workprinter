#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>
#include <EEPROM.h>

#include "Indicator.h"
#include "Button.h"
#include "HeatBed.h"

unsigned long long time = 0;
unsigned long long count = 0;

void setup()
{
  Indicator::init();
  Button::init();
  HeatBed::init();
  Indicator::setDigit( HeatBed::task );
  
  delay( 1000 );
  
  if (EEPROM.read(0) == 0xBB) {
    HeatBed::task = EEPROM.read(1);
  } else {
    EEPROM.write( 0, 0xBB );
    EEPROM.write( 1, HeatBed::task );
  }
  
  time = millis();
  
  sei();
}

void loop()
{
  if ((PINC & 0b10) == 0) {
    --HeatBed::task;
    time = millis();
  }
  
  if ((PINC & 0b100) == 0){
    ++HeatBed::task;
    time = millis();
  }
  
  HeatBed::execute();
  
  if ((millis() - time) < 3000) {
    Indicator::setDigit(HeatBed::task);
  } else {
    Indicator::setDigit(HeatBed::temp);
  }
  
  EEPROM.write( 1, HeatBed::task );  
  _delay_ms(200);
}

