#ifndef HEATBED_H_
#define HEATBED_H_

#include "OneWire.h"

namespace HeatBed
{
  #include <math.h>
  #include <avr/eeprom.h>
  

  OneWire ds(24);

  uint8_t ds_addr[8];
  uint8_t ds_data[12];
  uint8_t ds_state = 0;
  
  int task;
  int hist = 3;
  int temp = 0;  
  
  void init()
  {
    DDRC = DDRC & 0xFF;
    PORTC = PORTC & 0b11111110; 
   
    task = 133;
    
    while (!ds.search(ds_addr, false)) {
      ds.reset_search();
      task = 111;
    }
    if (OneWire::crc8(ds_addr, 7) == ds_addr[7]) {    
      ds_state = 1;
      task = 122;
    }
  }
  
  int readADC()
  {
    ADMUX = 0b00000111;
    _delay_ms( 10 );
    ADCSRA |= 0x40;
    while ( (ADCSRA & 0x10)==0 );
    return ADCW;
  }
  
  double getTemperature() {
    double temp = -999;
    ds.reset();
    ds.select(ds_addr);
    ds.write(0x44);
  
    //delay(1000);
  
    ds.reset();
    ds.select(ds_addr);
    ds.write(0xBE);
  
    for (uint8_t i = 0; i < 9; ++i) {
      ds_data[i] = ds.read();
    }
  
    int16_t raw = (ds_data[1] << 8) | ds_data[0];
    /*raw = raw << 3;
    if (ds_data[7] == 0x10) {
      raw = (raw & 0xFFF0) + 12 - ds_data[6];
    }
    temp = ((float)raw / 16);*/

    uint8_t cfg = (ds_data[4] & 0x60);
    // at lower res, the low bits are undefined, so let's zero them
    if (cfg == 0x00) raw = raw & ~7;  // 9 bit resolution, 93.75 ms
    else if (cfg == 0x20) raw = raw & ~3; // 10 bit res, 187.5 ms
    else if (cfg == 0x40) raw = raw & ~1; // 11 bit res, 375 ms

    temp = (double)raw / 16;
    
    return temp;
  }
  
  void execute()
  {
    temp = getTemperature();
    if (temp < task - hist) {
      PORTC |= 1;
    } else if (temp > task) {
      PORTC &= 0b11111110;
    }   
  }
  
  void executeCooling()
  {
    temp = getTemperature();
    
  }
}

#endif
