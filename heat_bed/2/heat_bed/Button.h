#ifndef BUTTON_H_
#define BUTTON_H_

namespace Button
{    
  #define BTN1 3
  #define BTN2 2
  #define BTN3 1
  
  char stateButton;
  
  char pressStateButton;
  char readStateButton;
  
  void init()
  {
    DDRC = 0b11110001;
    PORTC = 0b11111111;
    
    stateButton = 0x00;
  }
    
  void execute()
  {
    if ((PINC & 0b10) == 0) {
      if ((readStateButton && 0b10) == 1) {
        pressStateButton |= 0b10;
      }
    }
    if ((PINC & 0b10) != 0) {
      readStateButton &= 0b11111101;
      pressStateButton &= 0b11111101;
    }
    
    
    stateButton = PINC;
  }
  
  uint8_t isPressBtn(uint8_t channel) {
    if ((pressStateButton & (1<<channel)) != 0 && ((readStateButton & (1<<channel)) == 0)) {
      readStateButton |= (1<<channel);
      return 1;
    }
    return 0;
  }
  
  
  uint8_t getBtnPress()
  {
    return (PINC >> 1) & 0b111;
  }
}

#endif
