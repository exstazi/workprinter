#include <math.h>

#define PIN_TERM A0
#define PIN_HEAT_BED A1

double task = 90;
double hist = 0.5;



double Getterm(int RawADC) {
  double temp;
  temp = log(((10240000/RawADC) - 10000));
  temp = 1 / (0.001129148 + (0.000234125 * temp) + (0.0000000876741 * temp * temp * temp));
  temp = temp - 233.15;
  return temp;
}


void regul() {
  double temp = Getterm( analogRead(PIN_TERM) );

  if (temp < task - hist) {
    digitalWrite( PIN_HEAT_BED, 0 );
    Serial.print( "On. " );
  } else if (temp > task ) {
    digitalWrite( PIN_HEAT_BED, 1 );
    Serial.print( "Off. " );
  }
  Serial.print( "Temperature: " );
  Serial.println( temp );
}


int counter = 0;

void setup() {
  Serial.begin( 9600 );

  pinMode( PIN_HEAT_BED, OUTPUT );
  pinMode( PIN_TERM, INPUT );
}

void loop() {
  regul();
  delay( 1000 );
}
