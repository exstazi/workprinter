#define F_CPU 16000000L

#include <avr/io.h>
#include <avr/interrupt.h>
#include <util/delay.h>

#include "Indicator.h"
#include "Button.h"
#include "HeatBed.h"
#include "Util.h"

unsigned long long time = 0;
unsigned long long count = 0;

void setup()
{
	Indicator::init();
	Button::init();
	Util::initTimer();
	Indicator::setDigit(0);
	HeatBed::init();
	time = Util::millis();
	
	sei();
}

void loop()
{
	if ((PINC & 0b10) == 0) {
		--HeatBed::task;
		time = Util::millis();
	}
	
	if ((PINC & 0b100) == 0){
		++HeatBed::task;
		time = Util::millis();
	}
	
	HeatBed::execute();
	
	if ((Util::millis() - time) < 3000) {
		Indicator::setDigit(HeatBed::task);
	} else {
		Indicator::setDigit(HeatBed::temp);
	}
		
	_delay_ms(200);
}


int main(void)
{
	setup();
    while(1)
    {
        loop();
    }
	return 0;
}