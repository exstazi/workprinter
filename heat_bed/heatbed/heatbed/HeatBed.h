#ifndef HEATBED_H_
#define HEATBED_H_

#include "OneWire.h"

namespace HeatBed
{
	#include <math.h>
	#include <avr/eeprom.h>
	
	
	uint8_t id[8];
	
	#define PIN_CONTROL 0
	
	int task;
	int hist = 3;
	int temp = 0;
	
	
	
	void init()
	{
		DDRC = DDRC & 0xFF;
		PORTC = PORTC & 0b11111110;	
			
		/*uint8_t diff = OW_SEARCH_FIRST;
		
		OW_FindROM(&diff, &id[0]);
		
		task = 0;
		
		if (diff == OW_PRESENCE_ERR) task = 111;
		if (diff == OW_DATA_ERR) task = 122;*/
		
	}
	
	int readADC()
	{
		ADMUX = 0b00000111;
		_delay_ms( 10 );
		ADCSRA |= 0x40;
		while ( (ADCSRA & 0x10)==0 );
		return ADCW;
	}
	
	double getTemperature() {
		unsigned char data[2];
		double temp;
		
		
		/*
		int RawADC = readADC();
		double temp; 
		temp = log(((10240000/RawADC) - 10000));
		temp = 1 / (0.001129148 + (0.000234125 * temp) + (0.0000000876741 * temp * temp * temp));
		temp = temp - 242.15 - 10;
		*/
		return temp;
	}
	
	void execute()
	{
		temp = getTemperature();
		if (temp < task - hist) {
			PORTC |= 1;
		} else if (temp > task) {
			PORTC &= 0b11111110;
		}		
	}
	
	void executeCooling()
	{
		temp = getTemperature();
		
	}
}

#endif