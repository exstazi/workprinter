﻿//*****************************************************************************
// Copyright (C) 2013 Cognex Corporation
//
// Subject to Cognex Corporation's terms and conditions and license
// agreement, you are authorized to use and modify this source code in
// any way you find useful, provided the Software and/or the modified
// Software is used solely in conjunction with a Cognex Machine Vision
// System.  Furthermore you acknowledge and agree that Cognex has no
// warranty, obligations or liability for your use of the Software.
//*****************************************************************************
// This sample program is designed to illustrate certain VisionPro
// features or techniques in the simplest way possible. It is not
// intended as the framework for a complete application. In particular,
// the sample program may not provide proper error handling, event
// handling, cleanup, repeatability, and other mechanisms that a
// commercial quality application requires.
//
// This program assumes that you have some knowledge of C# and VisionPro
// programming.
//
// This sample demonstrates how to modify properties, and acquire
// Intensity and Range images using the Profile Camera.
//


using System;
using System.Windows.Forms;
using Cognex.VisionPro;
using Cognex.VisionPro.FGGigE;
using Cognex.VisionPro.Exceptions;
using System.Threading;

using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Drawing;
using System.Collections.Concurrent;
using System.Diagnostics;

using System.IO;
using System.Drawing.Imaging;
using System.Collections.Generic;

namespace ProfileCameraAcquisition
{
    public partial class Form1 : Form
    {
        // Delegates to help thread-safe control manipulation
        public delegate void messageTextBoxAppendDelegate(string s);        
        public delegate void setInputImageDelegate(ICogImage img);
        public delegate void enableProfileCamControlsDelegate(bool enabled);
        public delegate void displayErrorLabelDelegate(bool visible, string text);

        // Profile Camera helper object
        private ProfileCameraHelper mProfileCamHelper = null;
        private bool mIsFormShown = false;
        private bool mErrorShown = false;
        
        // Time to display the acq error label in ms
        private readonly int errorLabelDisplayTimeMS = 1500;

        private static Communicate com = new Communicate();

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

        private Dictionary<CodeHeader, System.Action<byte[]>> map = new Dictionary<CodeHeader, Action<byte[]>>();

        /// <summary>
        /// Form constructor
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            this.mCogDisplayStatusBar.Display = mCogDisplay;
            com.CommandReady += Communicate_CommandReady;


            map.Add(CodeHeader.ACQUIRE,     (buf) => this.AcquireImageMethod());
            map.Add(CodeHeader.CONTINUE,    (buf) => mProfileCamHelper.ProfileCamFifo.OwnedTriggerParams.TriggerModel = CogAcqTriggerModelConstants.FreeRun);
            map.Add(CodeHeader.STOP,        (buf) => mProfileCamHelper.ProfileCamFifo.OwnedTriggerParams.TriggerModel = CogAcqTriggerModelConstants.Manual);

            //map.Add(CodeHeader.CAMERA_MODE, (buf) => { });
            map.Add(CodeHeader.DETECTION_SENSITIVITY, (buf) => mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.DetectionSensitivity = BitConverter.ToInt32(buf, 0));
            map.Add(CodeHeader.HIGH_DYNAMIC_RANGE,  (buf) => mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.HighDynamicRange = BitConverter.ToInt32(buf, 0) != 0 ? true : false);
            //map.Add(CodeHeader.LASER_MODE,          (buf) => { });
            map.Add(CodeHeader.DETECTION_BASE, (buf) => mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionBase = BitConverter.ToInt32(buf, 0));
            map.Add(CodeHeader.DETECTION_HEIGHT, (buf) => mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionHeight = BitConverter.ToInt32(buf, 0));
            map.Add(CodeHeader.DETECTION_SAMPLING, (buf) => mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionSampling = BitConverter.ToInt32(buf, 0));

            map.Add(CodeHeader.AUTO_CORRECT_PIXEL_ROW_ORDER, (buf) => mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.AutoCorrectPixelRowOrder = BitConverter.ToInt32(buf, 0) != 0 ? true : false);
            map.Add(CodeHeader.FIRST_PIXEL_LOCATION, (buf) => mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.FirstPixelLocation = BitConverter.ToDouble(buf, 0));
            map.Add(CodeHeader.X_SCALE, (buf) => mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.XScale = BitConverter.ToDouble(buf, 0));
            map.Add(CodeHeader.Z_SCALE, (buf) => mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.ZScale = BitConverter.ToDouble(buf, 0));
            map.Add(CodeHeader.Z_OFFSET, (buf) => mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.ZOffset = BitConverter.ToDouble(buf, 0));

            map.Add(CodeHeader.ACQUIRE_DIRECTION_POSITIVE, (buf) => mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.AcquireDirectionPositive = BitConverter.ToInt32(buf, 0) != 0 ? true : false);
            map.Add(CodeHeader.DISTANCE_PER_CYCLE, (buf) => mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.DistancePerCycle = BitConverter.ToDouble(buf, 0));
            
        }

        private void Communicate_CommandReady(object sender, EventArgs e, CodeHeader command, byte[] val)
        {
            Console.WriteLine("Ready command: " + command + "; val: " + val.Length);
            
            Action<byte[]> action;
            if (map.ContainsKey(command))
            {
                map.TryGetValue(command, out action);
                action(val);
            }
            else
            {
                switch (command)
                {
                    case CodeHeader.CAMERA_MODE:
                        int mode = BitConverter.ToInt32(val, 0);
                        mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.CameraMode =
                            mode == 0 ? CogAcqCameraModeConstants.Intensity : mode == 1 ? CogAcqCameraModeConstants.IntensityWithGraphics : CogAcqCameraModeConstants.Range;
                        break;

                    case CodeHeader.LASER_MODE:
                        int lmode = BitConverter.ToInt32(val, 0);
                        mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.LaserMode =
                            lmode == 0 ? CogAcqLaserModeConstants.Off : lmode == 1 ? CogAcqLaserModeConstants.On : CogAcqLaserModeConstants.Strobed;
                        break;

                    default:
                        Console.WriteLine("Command: " + command + " not found!");
                        break;

                }
            }

        }

        private void ReadConstantParams()
        {
            Console.WriteLine("-----------------------------------------------");
            Console.WriteLine("Min Exposure = " + mProfileCamHelper.ProfileCamFifo.OwnedExposureParams.MinExposure);
            Console.WriteLine("Package Size Max = " + mProfileCamHelper.ProfileCamFifo.OwnedGigEVisionTransportParams.PacketSizeMax);
            Console.WriteLine("TimeoutEnabled = " + mProfileCamHelper.ProfileCamFifo.TimeoutEnabled);
            Console.WriteLine("Detection Sensitivity Max = " + mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.DetectionSensitivityMax);
            Console.WriteLine("ZDetection height Max = " + mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionHeightMax);
            Console.WriteLine("ZDetection sampling max = " + mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionSamplingMax);
            Console.WriteLine("Distance per cycle max = " + mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.DistancePerCycleMax);
            Console.WriteLine("Distance per cycle min = " + mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.DistancePerCycleMin);
            Console.WriteLine("First Pixel Location Range =" + mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.FirstPixelLocationRange);
            Console.WriteLine("XScale max = " + mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.XScaleMax);
            Console.WriteLine("XScale min = " + mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.XScaleMin);
            Console.WriteLine("ZOffsetMax = " + mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.ZOffsetMax);
            Console.WriteLine("ZOffsetMin = " + mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.ZOffsetMin);
            Console.WriteLine("ZScale max = " + mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.ZScaleMax);
            Console.WriteLine("ZScale min = " + mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.ZScaleMin);
            Console.WriteLine("Trigger model = " + mProfileCamHelper.ProfileCamFifo.OwnedTriggerParams.TriggerModel);
            Console.WriteLine("ROI = " + mProfileCamHelper.ProfileCamFifo.OwnedROIParams.ROIMode);
            Console.WriteLine("-----------------------------------------------");
        }



        // Syncronously acquire image in a separate method
        public void AcquireImageMethod()
        {
            EnableProfileCameraControls(false);

            try
            {
                mProfileCamHelper.SyncAcquire();
            }
            catch (CogAcqEncoderOverrunException)
            {
                // Encoder is too fast!
                MessageTextBoxAppend("Encoder overrun occured!");
                ShowError("Encoder overrun occured!", errorLabelDisplayTimeMS);

                return;
            }
            catch (CogAcqTimeoutException)
            {
                // Acquisition timeout!
                MessageTextBoxAppend("Acquisition timeout elapsed (possible reasons: timeout too short, trigger not detected or encoder too slow/not detected");
                ShowError("Acquisition timeout elapsed.", -1);
                return;
            }
            catch (Exception ex)
            {
                // Another error occured!
                MessageTextBoxAppend("Acquisition error: " + ex.Message);
                ShowError("Acquisition error.", -1);
                return;
            }
            finally
            {
                EnableProfileCameraControls(true);
            }
        }


        protected void InitializeUIAfterScanner()
        {
            txtExposure.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedExposureParams.Exposure;
            txtContrast.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedContrastParams.Contrast;
            txtTimeout.Value = (decimal)mProfileCamHelper.ProfileCamFifo.Timeout;

            cbTriggerEnabled.Checked = mProfileCamHelper.ProfileCamFifo.OwnedTriggerParams.TriggerEnabled;
            cbTriggerLowToHigh.Checked = mProfileCamHelper.ProfileCamFifo.OwnedTriggerParams.TriggerLowToHigh;
            cboxTriggerModel.SelectedText = mProfileCamHelper.ProfileCamFifo.OwnedTriggerParams.TriggerModel.ToString();

            txtLatencyLevel.Value = mProfileCamHelper.ProfileCamFifo.OwnedGigEVisionTransportParams.LatencyLevel;
            txtPacketSize.Value = mProfileCamHelper.ProfileCamFifo.OwnedGigEVisionTransportParams.PacketSize;
            txtTransportTimeout.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedGigEVisionTransportParams.TransportTimeout;

            nud_ZBase.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionBase;
            nud_ZHeight.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionHeight;
            txtDetectionSensitivity.Value = mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.DetectionSensitivity;
            cbHighDynamicRange.Checked = mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.HighDynamicRange;
            txtZDetectionSampling.Value = mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionSampling;

            int spl, spl16;
            mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.GetStepsPerLine(out spl, out spl16);
            nud_StepsPerLine.Value = spl;

            int x, y, w, h;
            mProfileCamHelper.ProfileCamFifo.OwnedROIParams.GetROIXYWidthHeight(out x, out y, out w, out h);
            nud_RangeHeight.Value = (decimal)h;

            lbl_SizeSpec.Text = "Range size: " + w + " x [userdefined]";


            txtDistancePerCycle.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.DistancePerCycle;
            cboxEndoderResolution.SelectedText = mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.EncoderResolution.ToString();
            cbAcquirePositiveEncoderDirection.Checked = mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.AcquireDirectionPositive;
            cbIgnoreTooFastEncoder.Checked = mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.IgnoreTooFastEncoder;
            cbIgnoreBackwardMotionBetweenAcquires.Checked = mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.IgnoreBackwardsMotionBetweenAcquires;
            txtEncoderCounter.Text = mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.CurrentEncoderCount.ToString();

            cbAutoCorrectPixelRowOrder.Checked = mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.AutoCorrectPixelRowOrder;
            txtFirstPixelLocation.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.FirstPixelLocation;
            txtXScale.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.XScale;
            txtYScale.Text = mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.YScale.ToString();
            txtZScale.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.ZScale;
            txtZOffset.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.ZOffset;
        }

        /////////////////////////////////////////////////////////////////////////////////////////////////////

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        
        #region Event-handlers
    
        // Event-handler hooked to synchronous acq start button
        private void btn_SyncAcquire_Click(object sender, EventArgs e)
        {
            if (mErrorShown)
            {
                mErrorShown = false;
                DisplayErrorLabel(false, "");
            }

            if (rdb_InstensityWG.Checked)
            {
                this.AcquireImageMethod();
            }
            else
            {
                // Asyncronous acquisition for range image to keep GUI responsive
                Thread t = new Thread(this.AcquireImageMethod);
                t.Start();
            }
        }

        // Event-handler hooked to RunContinuous Checkbox (button appearance)
        private void ckb_Continuous_CheckedChanged(object sender, EventArgs e)
        {
            // Clear error message on the GUI if necessary
            if (ckb_Continuous.Checked && mErrorShown)
            {
                mErrorShown = false;
                DisplayErrorLabel(false, "");
            }

            // Disable / enable controls
            rdb_InstensityWG.Enabled = !ckb_Continuous.Checked;
            rdb_Range.Enabled = !ckb_Continuous.Checked;
            btn_SyncAcquire.Enabled = !ckb_Continuous.Checked;
            ckb_TestEncoder.Enabled = !ckb_Continuous.Checked;
            
            if (ckb_Continuous.Checked)
            {
                mProfileCamHelper.ProfileCamFifo.OwnedTriggerParams.TriggerModel = CogAcqTriggerModelConstants.FreeRun;
            }
            else
            {
                mProfileCamHelper.ProfileCamFifo.OwnedTriggerParams.TriggerModel = CogAcqTriggerModelConstants.Manual;
            }
        }

        // Event-handler to turn test-encoder on and off
        private void ckb_TestEncoder_CheckedChanged(object sender, EventArgs e)
        {
            mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.TestEncoderEnabled = ckb_TestEncoder.Checked;
        }

        // Event-handler to update display when image is ready
        private void mProfileCamHelper_ImageReady(object sender, EventArgs e, ICogImage img)
        {
            // Clear error message on the GUI if successful acq happens
            if (mErrorShown)
            {
                mErrorShown = false;
                DisplayErrorLabel(false, "");
            }
            this.SetInputImage(img);

            // Call the garbage collector to free up old images that are no longer needed.
            GC.Collect();
        }

        // Event-handler to display error message when asyncronous acq fails
        private void mProfileCamHelper_AsynchronousAcqError(object sender, string errorMsg)
        {
            this.MessageTextBoxAppend(errorMsg);
            this.ShowError(errorMsg, errorLabelDisplayTimeMS);
        }

        private void ProfileCamera_ValueChanged(object sender, EventArgs e)
        {
            if (!mIsFormShown)
            {
                return;
            }

            try
            {
                if (sender == nud_ZBase || sender == nud_ZHeight)
                {
                    // Validate ZDetection value
                    if ((double)(nud_ZHeight.Value + nud_ZBase.Value) > mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionHeightMax)
                    {
                        nud_ZBase.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionBase;
                        nud_ZHeight.Value = (decimal)mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionHeight;
                    }
                    else
                    {
                        mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionBase = (double)nud_ZBase.Value;
                        mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionHeight = (double)nud_ZHeight.Value;
                    }
                }
                else if (sender == txtDetectionSensitivity)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.DetectionSensitivity = (int)txtDetectionSensitivity.Value;
                }
                else if (sender == txtZDetectionSampling)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.ZDetectionSampling = (int)txtZDetectionSampling.Value;
                }
                else if (sender == cbHighDynamicRange)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.HighDynamicRange = cbHighDynamicRange.Checked;
                }
                else if (sender == rdb_InstensityWG || sender == rdb_Range)
                {
                    if (rdb_InstensityWG.Checked)
                    {
                        mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.CameraMode = CogAcqCameraModeConstants.IntensityWithGraphics;
                    }
                    else
                    {
                        mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.CameraMode = CogAcqCameraModeConstants.Range;
                    }
                }
                else if (sender == rbLaserModeStrobed || sender == rbLaserModeOn || sender == rbLaserModeOff)
                {
                    if (rbLaserModeStrobed.Checked) 
                    {
                        mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.LaserMode = CogAcqLaserModeConstants.Strobed;
                    }
                    else if (rbLaserModeOn.Checked)
                    {
                        mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.LaserMode = CogAcqLaserModeConstants.On;
                    }
                    else if (rbLaserModeOff.Checked)
                    {
                        mProfileCamHelper.ProfileCamFifo.OwnedProfileCameraParams.LaserMode = CogAcqLaserModeConstants.Off;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageTextBoxAppend("Camera configuration error: " + ex.Message);
            }
        }

        private void LineScane_ValueChanged(object sender, EventArgs e)
        {
            if (!mIsFormShown)
            {
                return;
            }

            try
            {
                if (sender == txtDistancePerCycle)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.DistancePerCycle = (double)txtDistancePerCycle.Value;
                }
                else if (sender == cbIgnoreTooFastEncoder)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.IgnoreTooFastEncoder = cbIgnoreTooFastEncoder.Checked;
                }
                else if (sender == cbAcquirePositiveEncoderDirection)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.AcquireDirectionPositive = cbAcquirePositiveEncoderDirection.Checked;
                }
                else if (sender == cbIgnoreBackwardMotionBetweenAcquires)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.IgnoreBackwardsMotionBetweenAcquires = cbIgnoreBackwardMotionBetweenAcquires.Checked;
                }
                else if (sender == nud_StepsPerLine)
                {
                    // Apply steps per line setting to camera
                    int previousValue = (int)nud_StepsPerLine.Value;
                    try
                    {
                        mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.SetStepsPerLine((int)nud_StepsPerLine.Value, 0);
                    }
                    catch (Exception ex)
                    {
                        // Need to restore value on error, because NumericUpDown boundaries might be incorrect
                        mProfileCamHelper.ProfileCamFifo.OwnedLineScanParams.SetStepsPerLine(previousValue, 0);
                        nud_StepsPerLine.Value = previousValue;
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageTextBoxAppend("Camera configuration error: " + ex.Message);
            }
        }

        private void RangeImage_ValueChanged(object sender, EventArgs e)
        {
            if (!mIsFormShown)
            {
                return;
            }

            try
            {
                if (sender == cbAutoCorrectPixelRowOrder)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.AutoCorrectPixelRowOrder = cbAutoCorrectPixelRowOrder.Checked;
                }
                else if (sender == txtFirstPixelLocation)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.FirstPixelLocation = (double)txtFirstPixelLocation.Value;
                }
                else if (sender == txtXScale)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.XScale = (double)txtXScale.Value;
                }
                else if (sender == txtZScale)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.ZScale = (double)txtZScale.Value;
                }
                else if (sender == txtZOffset)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedRangeImageParams.ZOffset = (double)txtZOffset.Value;
                }
            }
            catch (Exception ex)
            {
                MessageTextBoxAppend("Camera configuration error: " + ex.Message);
            }
        }

        private void CameraPropery_ValueChanged(object sender, EventArgs e)
        {
            if (!mIsFormShown)
            {
                return;
            }

            try
            {
                if (sender == txtContrast)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedContrastParams.Contrast = (double)txtContrast.Value;
                }
                else if (sender == txtExposure)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedExposureParams.Exposure = (double)txtExposure.Value;
                }
                else if (sender == txtLatencyLevel)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedGigEVisionTransportParams.LatencyLevel = (int)txtLatencyLevel.Value;
                }
                else if (sender == txtPacketSize)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedGigEVisionTransportParams.PacketSize = (int)txtPacketSize.Value;
                }
                else if (sender == txtTransportTimeout)
                {
                    mProfileCamHelper.ProfileCamFifo.OwnedGigEVisionTransportParams.TransportTimeout = (double)txtTransportTimeout.Value;
                }
                else if (sender == txtTimeout)
                {
                    mProfileCamHelper.ProfileCamFifo.Timeout = (double)txtTimeout.Value;
                }
                else if (sender == nud_RangeHeight)
                {
                    // Need to restore value, because 
                    // Apply range image ROI setting to camera
                    int x, y, w, h;
                    mProfileCamHelper.ProfileCamFifo.OwnedROIParams.GetROIXYWidthHeight(out x, out y, out w, out h);
                    try
                    {
                        // Apply steps per line setting to camera
                        mProfileCamHelper.ProfileCamFifo.OwnedROIParams.SetROIXYWidthHeight(x, y, w, (int)nud_RangeHeight.Value);
                    }
                    catch (Exception ex)
                    {
                        // Need to restore value on error, because NumericUpDown boundaries might be incorrect
                        nud_RangeHeight.Value = h;
                        mProfileCamHelper.ProfileCamFifo.OwnedROIParams.SetROIXYWidthHeight(x, y, w, h);
                        throw new Exception(ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageTextBoxAppend("Camera configuration error: " + ex.Message);
            }
        }


        // Event-handler for form closing to dispose Profile Camera helper
        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (mProfileCamHelper != null)
            {
                mProfileCamHelper.Dispose();
            }
            com.Stop();
        }

        // Event-handler to initialize control values, and set flag after form is shown.
        // Flag is required to prevent other event-handlers from running to early 
        private void Form1_Shown(object sender, EventArgs e)
        {
            // Set the GUI controls to the property values read from the camera
            if (mProfileCamHelper != null)
            {
            }
            mIsFormShown = true;

            
        }
        #endregion

        //Инициализация сканера
        private void btn_initializeScanner(object sender, EventArgs e)
        {
            Console.WriteLine("Инициализация сканера");
            ICogAcqFifo profileCamFifo = null;
            CogFrameGrabberGigEs fgs = new CogFrameGrabberGigEs();

            if (fgs.Count < 1)
            {
                Console.WriteLine("Camera not found!");
                return;
            }
                        
            foreach (ICogFrameGrabber fg in fgs)
            {
                try
                {
                    MessageTextBoxAppend("Profile camera found, trying to connect...");
                    if (fg.AvailableVideoFormats[0] == "Cognex NullFormat")
                    {
                        profileCamFifo = fg.CreateAcqFifo(fg.AvailableVideoFormats[0],
                                                     CogAcqFifoPixelFormatConstants.Format16Grey,
                                                     0,
                                                     true);
                    }
                }
                catch (CogAcqHardwareInUseException)
                {
                    // Profile Camera is busy
                    MessageTextBoxAppend("Profile camera detected, but is busy!");
                    EnableProfileCameraControls(false);
                    profileCamFifo = null;
                    continue;
                }
                catch (Exception ex)
                {
                    // Other error
                    MessageTextBoxAppend("Profile camera initialization error: " + ex.Message);
                    EnableProfileCameraControls(false);
                    profileCamFifo = null;
                    continue;
                }

                if (profileCamFifo != null)
                {
                    break;
                }
            }

            if (profileCamFifo != null)
            {
                // Create helper object
                mProfileCamHelper = new ProfileCameraHelper(profileCamFifo);

                // Event subscriptions
                mProfileCamHelper.ImageReady += mProfileCamHelper_ImageReady;
                mProfileCamHelper.AsynchronousAcqError += mProfileCamHelper_AsynchronousAcqError;
                mProfileCamHelper.ProfileCamFifo.Changed += new CogChangedEventHandler(ProfileCamFifo_Changed);

                // Camera configuration
                int x, y, w, h;
                mProfileCamHelper.ProfileCamFifo.OwnedROIParams.GetROIXYWidthHeight(out x, out y, out w, out h);
                mProfileCamHelper.ProfileCamFifo.OwnedROIParams.SetROIXYWidthHeight(x, y, w, 500);
                mProfileCamHelper.ProfileCamFifo.Timeout = 10000;

                // Print GUI messages
                txt_Message.AppendText("Profile Camera detected!" + Environment.NewLine);
                txt_Message.AppendText(" Name: " + profileCamFifo.FrameGrabber.Name + Environment.NewLine);
                txt_Message.AppendText(" Serial: " + profileCamFifo.FrameGrabber.SerialNumber + Environment.NewLine);
                txt_Message.AppendText(" Videoformat: " + profileCamFifo.VideoFormat + Environment.NewLine);
                EnableProfileCameraControls(true);

                ReadConstantParams();
                InitializeUIAfterScanner();
            }
            else
            {
                txt_Message.AppendText("Profile Camera could not be found, or is busy!" + Environment.NewLine);
                EnableProfileCameraControls(false);
            }
        }

        void ProfileCamFifo_Changed(object sender, CogChangedEventArgs e)
        {
            Console.WriteLine(e.GetStateFlagNames(sender));
            //throw new NotImplementedException();
        }

        private void btn_startServer(object sender, EventArgs e)
        {

        }



        #region GUI helper methods
        /// <summary>
        /// Enable or disable camera GUI controls
        /// </summary>
        /// <param name="enabled">The enabled property of controls is set to this value</param>
        private void EnableProfileCameraControls(bool enabled)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new enableProfileCamControlsDelegate(this.EnableProfileCameraControls), enabled);
            }
            else
            {
                pnl_CameraMode.Enabled = enabled;
                btn_SyncAcquire.Enabled = enabled;
                ckb_Continuous.Enabled = enabled;
                pnl_ImageSourceSettings.Enabled = enabled;
            }
        }

        /// <summary>
        /// Append text to txt_Message textbox
        /// </summary>
        /// <param name="s">String to append</param>
        public void MessageTextBoxAppend(string s)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new messageTextBoxAppendDelegate(this.MessageTextBoxAppend), s);
            }
            else
            {
                DateTime now = DateTime.Now;
                this.txt_Message.AppendText("[" + now.Hour.ToString("D2") + ":" +
                                                  now.Minute.ToString("D2") + ":" +
                                                  now.Second.ToString("D2") + "] " + s + Environment.NewLine);
            }
        }

        /// <summary>
        /// Set the input image of the CogDisplay on the control (and zoom to fit)
        /// </summary>
        /// <param name="img">Image to be set as inputimage for the CogDisplay</param>
        public unsafe void SetInputImage(ICogImage img)
        {
            if (this.InvokeRequired)
            {
                // Catching the ObjectDisposedException is required, because upon closing the form,
                // a race condition emerges between the form disposal and the Invoke request from
                // the acquisition thread to the GUI thread. There is no trivial way to avoid this, but
                // to catch and suppress the arising exception
                try
                {
                    this.Invoke(new setInputImageDelegate(this.SetInputImage), img);
                }
                catch (ObjectDisposedException)
                {
                    // Do nothing here
                }
            }
            else
            {
                this.mCogDisplay.Image = img;
                // this.mCogDisplay.Fit(true);

                //list.Enqueue(img);
                //Console.WriteLine(list.Count);

                Bitmap map = img.ToBitmap();
                long min = 0;

                com.SendFrame(map);

                BitmapData mapData = map.LockBits(new Rectangle(0, 0, 1024, 768), ImageLockMode.ReadWrite, map.PixelFormat);

                byte* scan = (byte*)mapData.Scan0.ToPointer();
                for (int i = 0; i < 1024; ++i)
                {
                    for (int j = 0; j < 768; ++j)
                    {
                        byte* data = scan + j * mapData.Stride + i * 3;
                        //data[2] == red
                        if (data[2] > 200 && data[1] < 150 && data[0] < 150)
                        {
                            if (j > min)
                            {
                                min = j;
                            }
                        }
                    }
                }

                map.UnlockBits(mapData);


                Console.WriteLine("Min = " + min);
            }
        }

        /// <summary>
        /// Show or hide error label on the form
        /// </summary>
        /// <param name="visible">True to display, false to hide the label</param>
        /// <param name="msg">Error message to show on the label</param>
        public void DisplayErrorLabel(bool visible, string msg)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new displayErrorLabelDelegate(this.DisplayErrorLabel), visible, msg);
            }
            else
            {
                this.lbl_AcqError.Visible = visible;
                this.lbl_AcqError.Text = msg;
            }
        }

        /// <summary>
        /// Displays the error label using a non-blocking thread with a specified text, for a 
        /// specified time period
        /// </summary>
        /// <param name="errorMsg">Message to display in the label</param>
        /// <param name="timeMS">Time period to display the label for</param>
        public void ShowError(string errorMsg, int timeMS)
        {
            if (mErrorShown && lbl_AcqError.Text == errorMsg)
            {
                return;
            }
            else
            {
                // Create a separate thread to avoid blocking the GUI thread.
                Thread t = new Thread(delegate()
                {
                    mErrorShown = true;
                    DisplayErrorLabel(true, errorMsg);
                    if (timeMS == -1)
                    {
                        return;
                    }
                    Thread.Sleep(timeMS);
                    DisplayErrorLabel(false, "");
                    mErrorShown = false;
                }
                );
                t.Start();
            }
        }
        #endregion

    }
}
