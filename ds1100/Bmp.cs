﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Drawing;

namespace ProfileCameraAcquisition
{
  
    class Bmp
    {
        private byte[] bmpFile;
        public uint w = 0;
        public uint h = 0;
        
        /*public void loop()
        {
            long offset = 2359350;
            long min = 0;

            for (int i = 0; i < w; ++i)
            {
                for (int j = 0; j < h; ++j)
                {
                    offset = 2359350 + 3 * (-j * w + i - 1024);
                    byte r = bmpFile[offset+2];
                    byte g = bmpFile[offset+1];
                    byte b = bmpFile[offset];

                    if (j > min)
                    {
                        min = j;
                    }
                }
            }
        }*/

        public void loop()
        {
            long offset = 2359350;
            long min = 0;

            for (int i = 0; i < w; ++i)
            {
                for (int j = 0; j < h; ++j)
                {
                    offset = 2359350 + 3 * (-j * w + i - 1024);
                    byte r = bmpFile[offset + 2];
                    byte g = bmpFile[offset + 1];
                    byte b = bmpFile[offset];

                    if (r > 200 && g < 150 && b < 150)
                    {
                        min = j;
                    }
                }
            }
            Console.WriteLine(min);
        }


        public Bmp(byte[] data)
        {
            bmpFile = data;
            readHeader();
        }

        public Bmp(Bitmap bmp)
        {
            bmpFile = Bmp.ImageToByte(bmp);
            readHeader();
            Console.WriteLine("Size = " + bmpFile.Length);
        }

        private void readHeader()
        {
            w = (uint)(bmpFile[21] << 24) + (uint)(bmpFile[20] << 16) + (uint)(bmpFile[19] << 8) + (uint)bmpFile[18];
            h = (uint)(bmpFile[25] << 24) + (uint)(bmpFile[24] << 16) + (uint)(bmpFile[23] << 8) + (uint)bmpFile[22];
        }

        public static void getJpeg()
        {

        }

        public static byte[] ImageToByte(Bitmap img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }
    }
}
