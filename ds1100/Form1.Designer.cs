﻿//*****************************************************************************
// Copyright (C) 2013 Cognex Corporation
//
// Subject to Cognex Corporation's terms and conditions and license
// agreement, you are authorized to use and modify this source code in
// any way you find useful, provided the Software and/or the modified
// Software is used solely in conjunction with a Cognex Machine Vision
// System.  Furthermore you acknowledge and agree that Cognex has no
// warranty, obligations or liability for your use of the Software.
//*****************************************************************************
// This sample program is designed to illustrate certain VisionPro
// features or techniques in the simplest way possible. It is not
// intended as the framework for a complete application. In particular,
// the sample program may not provide proper error handling, event
// handling, cleanup, repeatability, and other mechanisms that a
// commercial quality application requires.
//
// This program assumes that you have some knowledge of C# and VisionPro
// programming.
//

namespace ProfileCameraAcquisition
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pnl_Controls = new System.Windows.Forms.Panel();
            this.txtZOffset = new System.Windows.Forms.NumericUpDown();
            this.txtYScale = new System.Windows.Forms.Label();
            this.txtZScale = new System.Windows.Forms.NumericUpDown();
            this.txtXScale = new System.Windows.Forms.NumericUpDown();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.txtFirstPixelLocation = new System.Windows.Forms.NumericUpDown();
            this.label15 = new System.Windows.Forms.Label();
            this.cbAutoCorrectPixelRowOrder = new System.Windows.Forms.CheckBox();
            this.cbIgnoreTooFastEncoder = new System.Windows.Forms.CheckBox();
            this.cbIgnoreBackwardMotionBetweenAcquires = new System.Windows.Forms.CheckBox();
            this.txtEncoderCounter = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.cbAcquirePositiveEncoderDirection = new System.Windows.Forms.CheckBox();
            this.cboxEndoderResolution = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtDistancePerCycle = new System.Windows.Forms.NumericUpDown();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.rbLaserModeOff = new System.Windows.Forms.RadioButton();
            this.rbLaserModeOn = new System.Windows.Forms.RadioButton();
            this.rbLaserModeStrobed = new System.Windows.Forms.RadioButton();
            this.label13 = new System.Windows.Forms.Label();
            this.cboxTriggerModel = new System.Windows.Forms.ComboBox();
            this.cbTriggerLowToHigh = new System.Windows.Forms.CheckBox();
            this.cbTriggerEnabled = new System.Windows.Forms.CheckBox();
            this.txtTimeout = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.txtTransportTimeout = new System.Windows.Forms.NumericUpDown();
            this.txtPacketSize = new System.Windows.Forms.NumericUpDown();
            this.txtLatencyLevel = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtExposure = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.txtContrast = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pnl_ImageSourceSettings = new System.Windows.Forms.Panel();
            this.txtZDetectionSampling = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.cbHighDynamicRange = new System.Windows.Forms.CheckBox();
            this.txtDetectionSensitivity = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.ckb_TestEncoder = new System.Windows.Forms.CheckBox();
            this.lbl_ZBase = new System.Windows.Forms.Label();
            this.lbl_RangeHeight = new System.Windows.Forms.Label();
            this.lbl_ZHeight = new System.Windows.Forms.Label();
            this.nud_RangeHeight = new System.Windows.Forms.NumericUpDown();
            this.nud_StepsPerLine = new System.Windows.Forms.NumericUpDown();
            this.lbl_SPL = new System.Windows.Forms.Label();
            this.nud_ZHeight = new System.Windows.Forms.NumericUpDown();
            this.nud_ZBase = new System.Windows.Forms.NumericUpDown();
            this.lbl_AcqError = new System.Windows.Forms.Label();
            this.ckb_Continuous = new System.Windows.Forms.CheckBox();
            this.pnl_CameraMode = new System.Windows.Forms.Panel();
            this.rdb_Range = new System.Windows.Forms.RadioButton();
            this.rdb_InstensityWG = new System.Windows.Forms.RadioButton();
            this.btn_SyncAcquire = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lbl_SizeSpec = new System.Windows.Forms.Label();
            this.mCogDisplay = new Cognex.VisionPro.Display.CogDisplay();
            this.mCogDisplayStatusBar = new Cognex.VisionPro.CogDisplayStatusBarV2();
            this.txt_Message = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.pnl_Controls.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtZOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtXScale)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstPixelLocation)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistancePerCycle)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransportTimeout)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPacketSize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLatencyLevel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExposure)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContrast)).BeginInit();
            this.pnl_ImageSourceSettings.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtZDetectionSampling)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetectionSensitivity)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_RangeHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_StepsPerLine)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ZHeight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ZBase)).BeginInit();
            this.pnl_CameraMode.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCogDisplay)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 445F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Controls.Add(this.pnl_Controls, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.mCogDisplay, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.mCogDisplayStatusBar, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txt_Message, 0, 2);
            this.tableLayoutPanel1.Cursor = System.Windows.Forms.Cursors.Default;
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1047, 704);
            this.tableLayoutPanel1.TabIndex = 3;
            // 
            // pnl_Controls
            // 
            this.pnl_Controls.Controls.Add(this.txtZOffset);
            this.pnl_Controls.Controls.Add(this.txtYScale);
            this.pnl_Controls.Controls.Add(this.txtZScale);
            this.pnl_Controls.Controls.Add(this.txtXScale);
            this.pnl_Controls.Controls.Add(this.label19);
            this.pnl_Controls.Controls.Add(this.label18);
            this.pnl_Controls.Controls.Add(this.label17);
            this.pnl_Controls.Controls.Add(this.label16);
            this.pnl_Controls.Controls.Add(this.txtFirstPixelLocation);
            this.pnl_Controls.Controls.Add(this.label15);
            this.pnl_Controls.Controls.Add(this.cbAutoCorrectPixelRowOrder);
            this.pnl_Controls.Controls.Add(this.cbIgnoreTooFastEncoder);
            this.pnl_Controls.Controls.Add(this.cbIgnoreBackwardMotionBetweenAcquires);
            this.pnl_Controls.Controls.Add(this.txtEncoderCounter);
            this.pnl_Controls.Controls.Add(this.label14);
            this.pnl_Controls.Controls.Add(this.cbAcquirePositiveEncoderDirection);
            this.pnl_Controls.Controls.Add(this.cboxEndoderResolution);
            this.pnl_Controls.Controls.Add(this.label6);
            this.pnl_Controls.Controls.Add(this.label4);
            this.pnl_Controls.Controls.Add(this.txtDistancePerCycle);
            this.pnl_Controls.Controls.Add(this.label2);
            this.pnl_Controls.Controls.Add(this.panel1);
            this.pnl_Controls.Controls.Add(this.label13);
            this.pnl_Controls.Controls.Add(this.cboxTriggerModel);
            this.pnl_Controls.Controls.Add(this.cbTriggerLowToHigh);
            this.pnl_Controls.Controls.Add(this.cbTriggerEnabled);
            this.pnl_Controls.Controls.Add(this.txtTimeout);
            this.pnl_Controls.Controls.Add(this.label10);
            this.pnl_Controls.Controls.Add(this.txtTransportTimeout);
            this.pnl_Controls.Controls.Add(this.txtPacketSize);
            this.pnl_Controls.Controls.Add(this.txtLatencyLevel);
            this.pnl_Controls.Controls.Add(this.label9);
            this.pnl_Controls.Controls.Add(this.label8);
            this.pnl_Controls.Controls.Add(this.label7);
            this.pnl_Controls.Controls.Add(this.txtExposure);
            this.pnl_Controls.Controls.Add(this.label3);
            this.pnl_Controls.Controls.Add(this.txtContrast);
            this.pnl_Controls.Controls.Add(this.label5);
            this.pnl_Controls.Controls.Add(this.button2);
            this.pnl_Controls.Controls.Add(this.button1);
            this.pnl_Controls.Controls.Add(this.pnl_ImageSourceSettings);
            this.pnl_Controls.Controls.Add(this.lbl_AcqError);
            this.pnl_Controls.Controls.Add(this.ckb_Continuous);
            this.pnl_Controls.Controls.Add(this.pnl_CameraMode);
            this.pnl_Controls.Controls.Add(this.btn_SyncAcquire);
            this.pnl_Controls.Controls.Add(this.label1);
            this.pnl_Controls.Controls.Add(this.lbl_SizeSpec);
            this.pnl_Controls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnl_Controls.Location = new System.Drawing.Point(3, 3);
            this.pnl_Controls.Name = "pnl_Controls";
            this.tableLayoutPanel1.SetRowSpan(this.pnl_Controls, 2);
            this.pnl_Controls.Size = new System.Drawing.Size(439, 598);
            this.pnl_Controls.TabIndex = 3;
            // 
            // txtZOffset
            // 
            this.txtZOffset.DecimalPlaces = 2;
            this.txtZOffset.Location = new System.Drawing.Point(350, 465);
            this.txtZOffset.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtZOffset.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.txtZOffset.Name = "txtZOffset";
            this.txtZOffset.Size = new System.Drawing.Size(66, 20);
            this.txtZOffset.TabIndex = 49;
            this.txtZOffset.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtZOffset.ValueChanged += new System.EventHandler(this.RangeImage_ValueChanged);
            // 
            // txtYScale
            // 
            this.txtYScale.AutoSize = true;
            this.txtYScale.Location = new System.Drawing.Point(347, 422);
            this.txtYScale.Name = "txtYScale";
            this.txtYScale.Size = new System.Drawing.Size(13, 13);
            this.txtYScale.TabIndex = 48;
            this.txtYScale.Text = "0";
            // 
            // txtZScale
            // 
            this.txtZScale.DecimalPlaces = 2;
            this.txtZScale.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtZScale.Location = new System.Drawing.Point(350, 439);
            this.txtZScale.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.txtZScale.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            -2147418112});
            this.txtZScale.Name = "txtZScale";
            this.txtZScale.Size = new System.Drawing.Size(66, 20);
            this.txtZScale.TabIndex = 47;
            this.txtZScale.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtZScale.ValueChanged += new System.EventHandler(this.RangeImage_ValueChanged);
            // 
            // txtXScale
            // 
            this.txtXScale.DecimalPlaces = 2;
            this.txtXScale.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.txtXScale.Location = new System.Drawing.Point(350, 396);
            this.txtXScale.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.txtXScale.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtXScale.Name = "txtXScale";
            this.txtXScale.Size = new System.Drawing.Size(66, 20);
            this.txtXScale.TabIndex = 46;
            this.txtXScale.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtXScale.ValueChanged += new System.EventHandler(this.RangeImage_ValueChanged);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(299, 467);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(45, 13);
            this.label19.TabIndex = 45;
            this.label19.Text = "ZOffset:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(300, 443);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(44, 13);
            this.label18.TabIndex = 44;
            this.label18.Text = "ZScale:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(297, 422);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(44, 13);
            this.label17.TabIndex = 43;
            this.label17.Text = "YScale:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(300, 398);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(44, 13);
            this.label16.TabIndex = 42;
            this.label16.Text = "XScale:";
            // 
            // txtFirstPixelLocation
            // 
            this.txtFirstPixelLocation.DecimalPlaces = 2;
            this.txtFirstPixelLocation.Location = new System.Drawing.Point(350, 370);
            this.txtFirstPixelLocation.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtFirstPixelLocation.Minimum = new decimal(new int[] {
            1000,
            0,
            0,
            -2147483648});
            this.txtFirstPixelLocation.Name = "txtFirstPixelLocation";
            this.txtFirstPixelLocation.Size = new System.Drawing.Size(66, 20);
            this.txtFirstPixelLocation.TabIndex = 41;
            this.txtFirstPixelLocation.ValueChanged += new System.EventHandler(this.RangeImage_ValueChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(252, 372);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(92, 13);
            this.label15.TabIndex = 40;
            this.label15.Text = "FirstPixelLocation:";
            // 
            // cbAutoCorrectPixelRowOrder
            // 
            this.cbAutoCorrectPixelRowOrder.AutoSize = true;
            this.cbAutoCorrectPixelRowOrder.Location = new System.Drawing.Point(255, 350);
            this.cbAutoCorrectPixelRowOrder.Name = "cbAutoCorrectPixelRowOrder";
            this.cbAutoCorrectPixelRowOrder.Size = new System.Drawing.Size(152, 17);
            this.cbAutoCorrectPixelRowOrder.TabIndex = 39;
            this.cbAutoCorrectPixelRowOrder.Text = "AutoCorrectPixelRowOrder";
            this.cbAutoCorrectPixelRowOrder.UseVisualStyleBackColor = true;
            this.cbAutoCorrectPixelRowOrder.CheckedChanged += new System.EventHandler(this.RangeImage_ValueChanged);
            // 
            // cbIgnoreTooFastEncoder
            // 
            this.cbIgnoreTooFastEncoder.AutoSize = true;
            this.cbIgnoreTooFastEncoder.Location = new System.Drawing.Point(19, 463);
            this.cbIgnoreTooFastEncoder.Name = "cbIgnoreTooFastEncoder";
            this.cbIgnoreTooFastEncoder.Size = new System.Drawing.Size(135, 17);
            this.cbIgnoreTooFastEncoder.TabIndex = 38;
            this.cbIgnoreTooFastEncoder.Text = "IgnoreTooFastEncoder";
            this.cbIgnoreTooFastEncoder.UseVisualStyleBackColor = true;
            this.cbIgnoreTooFastEncoder.CheckedChanged += new System.EventHandler(this.LineScane_ValueChanged);
            // 
            // cbIgnoreBackwardMotionBetweenAcquires
            // 
            this.cbIgnoreBackwardMotionBetweenAcquires.AutoSize = true;
            this.cbIgnoreBackwardMotionBetweenAcquires.Location = new System.Drawing.Point(19, 439);
            this.cbIgnoreBackwardMotionBetweenAcquires.Name = "cbIgnoreBackwardMotionBetweenAcquires";
            this.cbIgnoreBackwardMotionBetweenAcquires.Size = new System.Drawing.Size(224, 17);
            this.cbIgnoreBackwardMotionBetweenAcquires.TabIndex = 37;
            this.cbIgnoreBackwardMotionBetweenAcquires.Text = "IgnoreBackwardsMotionBetweenAcquires";
            this.cbIgnoreBackwardMotionBetweenAcquires.UseVisualStyleBackColor = true;
            this.cbIgnoreBackwardMotionBetweenAcquires.CheckedChanged += new System.EventHandler(this.LineScane_ValueChanged);
            // 
            // txtEncoderCounter
            // 
            this.txtEncoderCounter.AutoSize = true;
            this.txtEncoderCounter.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.txtEncoderCounter.Location = new System.Drawing.Point(127, 489);
            this.txtEncoderCounter.Name = "txtEncoderCounter";
            this.txtEncoderCounter.Size = new System.Drawing.Size(14, 13);
            this.txtEncoderCounter.TabIndex = 36;
            this.txtEncoderCounter.Text = "0";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label14.Location = new System.Drawing.Point(16, 489);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(105, 13);
            this.label14.TabIndex = 16;
            this.label14.Text = "Encoder counter:";
            // 
            // cbAcquirePositiveEncoderDirection
            // 
            this.cbAcquirePositiveEncoderDirection.AutoSize = true;
            this.cbAcquirePositiveEncoderDirection.Location = new System.Drawing.Point(19, 416);
            this.cbAcquirePositiveEncoderDirection.Name = "cbAcquirePositiveEncoderDirection";
            this.cbAcquirePositiveEncoderDirection.Size = new System.Drawing.Size(190, 17);
            this.cbAcquirePositiveEncoderDirection.TabIndex = 35;
            this.cbAcquirePositiveEncoderDirection.Text = "Acquire Positive Encoder Direction";
            this.cbAcquirePositiveEncoderDirection.UseVisualStyleBackColor = true;
            this.cbAcquirePositiveEncoderDirection.CheckedChanged += new System.EventHandler(this.LineScane_ValueChanged);
            // 
            // cboxEndoderResolution
            // 
            this.cboxEndoderResolution.FormattingEnabled = true;
            this.cboxEndoderResolution.Items.AddRange(new object[] {
            "1x",
            "2x",
            "4x"});
            this.cboxEndoderResolution.Location = new System.Drawing.Point(116, 389);
            this.cboxEndoderResolution.Name = "cboxEndoderResolution";
            this.cboxEndoderResolution.Size = new System.Drawing.Size(66, 21);
            this.cboxEndoderResolution.TabIndex = 34;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(16, 393);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(95, 13);
            this.label6.TabIndex = 33;
            this.label6.Text = "Encode resolution:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(185, 367);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 13);
            this.label4.TabIndex = 32;
            this.label4.Text = "mm/cycle:";
            // 
            // txtDistancePerCycle
            // 
            this.txtDistancePerCycle.DecimalPlaces = 2;
            this.txtDistancePerCycle.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtDistancePerCycle.Location = new System.Drawing.Point(117, 365);
            this.txtDistancePerCycle.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            458752});
            this.txtDistancePerCycle.Name = "txtDistancePerCycle";
            this.txtDistancePerCycle.Size = new System.Drawing.Size(66, 20);
            this.txtDistancePerCycle.TabIndex = 31;
            this.txtDistancePerCycle.Value = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtDistancePerCycle.ValueChanged += new System.EventHandler(this.LineScane_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 368);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 13);
            this.label2.TabIndex = 30;
            this.label2.Text = "Distance per Cycle:";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.rbLaserModeOff);
            this.panel1.Controls.Add(this.rbLaserModeOn);
            this.panel1.Controls.Add(this.rbLaserModeStrobed);
            this.panel1.Location = new System.Drawing.Point(245, 241);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(75, 72);
            this.panel1.TabIndex = 9;
            // 
            // rbLaserModeOff
            // 
            this.rbLaserModeOff.AutoSize = true;
            this.rbLaserModeOff.Location = new System.Drawing.Point(3, 49);
            this.rbLaserModeOff.Name = "rbLaserModeOff";
            this.rbLaserModeOff.Size = new System.Drawing.Size(39, 17);
            this.rbLaserModeOff.TabIndex = 8;
            this.rbLaserModeOff.TabStop = true;
            this.rbLaserModeOff.Text = "Off";
            this.rbLaserModeOff.UseVisualStyleBackColor = true;
            this.rbLaserModeOff.CheckedChanged += new System.EventHandler(this.ProfileCamera_ValueChanged);
            // 
            // rbLaserModeOn
            // 
            this.rbLaserModeOn.AutoSize = true;
            this.rbLaserModeOn.Location = new System.Drawing.Point(3, 26);
            this.rbLaserModeOn.Name = "rbLaserModeOn";
            this.rbLaserModeOn.Size = new System.Drawing.Size(39, 17);
            this.rbLaserModeOn.TabIndex = 6;
            this.rbLaserModeOn.TabStop = true;
            this.rbLaserModeOn.Text = "On";
            this.rbLaserModeOn.UseVisualStyleBackColor = true;
            this.rbLaserModeOn.CheckedChanged += new System.EventHandler(this.ProfileCamera_ValueChanged);
            // 
            // rbLaserModeStrobed
            // 
            this.rbLaserModeStrobed.AutoSize = true;
            this.rbLaserModeStrobed.Checked = true;
            this.rbLaserModeStrobed.Location = new System.Drawing.Point(3, 3);
            this.rbLaserModeStrobed.Name = "rbLaserModeStrobed";
            this.rbLaserModeStrobed.Size = new System.Drawing.Size(62, 17);
            this.rbLaserModeStrobed.TabIndex = 7;
            this.rbLaserModeStrobed.TabStop = true;
            this.rbLaserModeStrobed.Text = "Strobed";
            this.rbLaserModeStrobed.UseVisualStyleBackColor = true;
            this.rbLaserModeStrobed.CheckedChanged += new System.EventHandler(this.ProfileCamera_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label13.Location = new System.Drawing.Point(244, 225);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(76, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Laser mode:";
            // 
            // cboxTriggerModel
            // 
            this.cboxTriggerModel.FormattingEnabled = true;
            this.cboxTriggerModel.Items.AddRange(new object[] {
            "FreeRun",
            "Slave",
            "Semi",
            "Auto",
            "Manual"});
            this.cboxTriggerModel.Location = new System.Drawing.Point(14, 518);
            this.cboxTriggerModel.Name = "cboxTriggerModel";
            this.cboxTriggerModel.Size = new System.Drawing.Size(121, 21);
            this.cboxTriggerModel.TabIndex = 28;
            this.cboxTriggerModel.SelectedValueChanged += new System.EventHandler(this.CameraPropery_ValueChanged);
            // 
            // cbTriggerLowToHigh
            // 
            this.cbTriggerLowToHigh.AutoSize = true;
            this.cbTriggerLowToHigh.Location = new System.Drawing.Point(259, 545);
            this.cbTriggerLowToHigh.Name = "cbTriggerLowToHigh";
            this.cbTriggerLowToHigh.Size = new System.Drawing.Size(114, 17);
            this.cbTriggerLowToHigh.TabIndex = 27;
            this.cbTriggerLowToHigh.Text = "TriggerLowToHigh";
            this.cbTriggerLowToHigh.UseVisualStyleBackColor = true;
            // 
            // cbTriggerEnabled
            // 
            this.cbTriggerEnabled.AutoSize = true;
            this.cbTriggerEnabled.Location = new System.Drawing.Point(155, 546);
            this.cbTriggerEnabled.Name = "cbTriggerEnabled";
            this.cbTriggerEnabled.Size = new System.Drawing.Size(98, 17);
            this.cbTriggerEnabled.TabIndex = 26;
            this.cbTriggerEnabled.Text = "TriggerEnabled";
            this.cbTriggerEnabled.UseVisualStyleBackColor = true;
            // 
            // txtTimeout
            // 
            this.txtTimeout.Location = new System.Drawing.Point(83, 545);
            this.txtTimeout.Maximum = new decimal(new int[] {
            40000,
            0,
            0,
            0});
            this.txtTimeout.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtTimeout.Name = "txtTimeout";
            this.txtTimeout.Size = new System.Drawing.Size(66, 20);
            this.txtTimeout.TabIndex = 25;
            this.txtTimeout.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtTimeout.ValueChanged += new System.EventHandler(this.CameraPropery_ValueChanged);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(9, 547);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(69, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "Timeout acq:";
            // 
            // txtTransportTimeout
            // 
            this.txtTransportTimeout.DecimalPlaces = 1;
            this.txtTransportTimeout.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.txtTransportTimeout.Location = new System.Drawing.Point(365, 573);
            this.txtTransportTimeout.Maximum = new decimal(new int[] {
            400000,
            0,
            0,
            0});
            this.txtTransportTimeout.Name = "txtTransportTimeout";
            this.txtTransportTimeout.Size = new System.Drawing.Size(71, 20);
            this.txtTransportTimeout.TabIndex = 23;
            this.txtTransportTimeout.ValueChanged += new System.EventHandler(this.CameraPropery_ValueChanged);
            // 
            // txtPacketSize
            // 
            this.txtPacketSize.Location = new System.Drawing.Point(193, 573);
            this.txtPacketSize.Maximum = new decimal(new int[] {
            8000,
            0,
            0,
            0});
            this.txtPacketSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtPacketSize.Name = "txtPacketSize";
            this.txtPacketSize.Size = new System.Drawing.Size(66, 20);
            this.txtPacketSize.TabIndex = 22;
            this.txtPacketSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtPacketSize.ValueChanged += new System.EventHandler(this.CameraPropery_ValueChanged);
            // 
            // txtLatencyLevel
            // 
            this.txtLatencyLevel.Location = new System.Drawing.Point(86, 573);
            this.txtLatencyLevel.Maximum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.txtLatencyLevel.Name = "txtLatencyLevel";
            this.txtLatencyLevel.Size = new System.Drawing.Size(31, 20);
            this.txtLatencyLevel.TabIndex = 11;
            this.txtLatencyLevel.ValueChanged += new System.EventHandler(this.CameraPropery_ValueChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(265, 575);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 21;
            this.label9.Text = "TransportTimeout:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(124, 575);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 13);
            this.label8.TabIndex = 20;
            this.label8.Text = "PacketSize:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(9, 575);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(74, 13);
            this.label7.TabIndex = 19;
            this.label7.Text = "LatencyLevel:";
            // 
            // txtExposure
            // 
            this.txtExposure.DecimalPlaces = 1;
            this.txtExposure.Increment = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.txtExposure.Location = new System.Drawing.Point(294, 94);
            this.txtExposure.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.txtExposure.Name = "txtExposure";
            this.txtExposure.Size = new System.Drawing.Size(110, 20);
            this.txtExposure.TabIndex = 18;
            this.txtExposure.ValueChanged += new System.EventHandler(this.CameraPropery_ValueChanged);
            this.txtExposure.VisibleChanged += new System.EventHandler(this.CameraPropery_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(202, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 13);
            this.label3.TabIndex = 17;
            this.label3.Text = "Exposure (min: 0):";
            // 
            // txtContrast
            // 
            this.txtContrast.DecimalPlaces = 2;
            this.txtContrast.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
            this.txtContrast.Location = new System.Drawing.Point(61, 93);
            this.txtContrast.Maximum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.txtContrast.Name = "txtContrast";
            this.txtContrast.Size = new System.Drawing.Size(110, 20);
            this.txtContrast.TabIndex = 16;
            this.txtContrast.ValueChanged += new System.EventHandler(this.CameraPropery_ValueChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(11, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Contrast:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(201, 9);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(187, 23);
            this.button2.TabIndex = 13;
            this.button2.Text = "Инициализация сканера";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.btn_initializeScanner);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(9, 9);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(176, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Запустить сервер";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btn_startServer);
            // 
            // pnl_ImageSourceSettings
            // 
            this.pnl_ImageSourceSettings.Controls.Add(this.txtZDetectionSampling);
            this.pnl_ImageSourceSettings.Controls.Add(this.label12);
            this.pnl_ImageSourceSettings.Controls.Add(this.cbHighDynamicRange);
            this.pnl_ImageSourceSettings.Controls.Add(this.txtDetectionSensitivity);
            this.pnl_ImageSourceSettings.Controls.Add(this.label11);
            this.pnl_ImageSourceSettings.Controls.Add(this.ckb_TestEncoder);
            this.pnl_ImageSourceSettings.Controls.Add(this.lbl_ZBase);
            this.pnl_ImageSourceSettings.Controls.Add(this.lbl_RangeHeight);
            this.pnl_ImageSourceSettings.Controls.Add(this.lbl_ZHeight);
            this.pnl_ImageSourceSettings.Controls.Add(this.nud_RangeHeight);
            this.pnl_ImageSourceSettings.Controls.Add(this.nud_StepsPerLine);
            this.pnl_ImageSourceSettings.Controls.Add(this.lbl_SPL);
            this.pnl_ImageSourceSettings.Controls.Add(this.nud_ZHeight);
            this.pnl_ImageSourceSettings.Controls.Add(this.nud_ZBase);
            this.pnl_ImageSourceSettings.Location = new System.Drawing.Point(14, 144);
            this.pnl_ImageSourceSettings.Name = "pnl_ImageSourceSettings";
            this.pnl_ImageSourceSettings.Size = new System.Drawing.Size(212, 209);
            this.pnl_ImageSourceSettings.TabIndex = 11;
            // 
            // txtZDetectionSampling
            // 
            this.txtZDetectionSampling.Location = new System.Drawing.Point(147, 84);
            this.txtZDetectionSampling.Maximum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.txtZDetectionSampling.Name = "txtZDetectionSampling";
            this.txtZDetectionSampling.Size = new System.Drawing.Size(50, 20);
            this.txtZDetectionSampling.TabIndex = 15;
            this.txtZDetectionSampling.ValueChanged += new System.EventHandler(this.ProfileCamera_ValueChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label12.Location = new System.Drawing.Point(2, 86);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(133, 13);
            this.label12.TabIndex = 14;
            this.label12.Text = "Z Detection Sampling:";
            // 
            // cbHighDynamicRange
            // 
            this.cbHighDynamicRange.AutoSize = true;
            this.cbHighDynamicRange.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cbHighDynamicRange.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbHighDynamicRange.Location = new System.Drawing.Point(60, 185);
            this.cbHighDynamicRange.Name = "cbHighDynamicRange";
            this.cbHighDynamicRange.Size = new System.Drawing.Size(137, 17);
            this.cbHighDynamicRange.TabIndex = 13;
            this.cbHighDynamicRange.Text = "HighDynamicRange";
            this.cbHighDynamicRange.UseVisualStyleBackColor = true;
            this.cbHighDynamicRange.CheckedChanged += new System.EventHandler(this.ProfileCamera_ValueChanged);
            // 
            // txtDetectionSensitivity
            // 
            this.txtDetectionSensitivity.Location = new System.Drawing.Point(147, 58);
            this.txtDetectionSensitivity.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.txtDetectionSensitivity.Name = "txtDetectionSensitivity";
            this.txtDetectionSensitivity.Size = new System.Drawing.Size(50, 20);
            this.txtDetectionSensitivity.TabIndex = 12;
            this.txtDetectionSensitivity.ValueChanged += new System.EventHandler(this.ProfileCamera_ValueChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label11.Location = new System.Drawing.Point(9, 60);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(128, 13);
            this.label11.TabIndex = 11;
            this.label11.Text = "Detection Sensitivity:";
            // 
            // ckb_TestEncoder
            // 
            this.ckb_TestEncoder.AutoSize = true;
            this.ckb_TestEncoder.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ckb_TestEncoder.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.ckb_TestEncoder.Location = new System.Drawing.Point(74, 162);
            this.ckb_TestEncoder.Name = "ckb_TestEncoder";
            this.ckb_TestEncoder.Size = new System.Drawing.Size(123, 17);
            this.ckb_TestEncoder.TabIndex = 10;
            this.ckb_TestEncoder.Text = "Use test encoder";
            this.ckb_TestEncoder.UseVisualStyleBackColor = true;
            this.ckb_TestEncoder.CheckedChanged += new System.EventHandler(this.ckb_TestEncoder_CheckedChanged);
            // 
            // lbl_ZBase
            // 
            this.lbl_ZBase.AutoSize = true;
            this.lbl_ZBase.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_ZBase.Location = new System.Drawing.Point(27, 12);
            this.lbl_ZBase.Name = "lbl_ZBase";
            this.lbl_ZBase.Size = new System.Drawing.Size(110, 13);
            this.lbl_ZBase.TabIndex = 1;
            this.lbl_ZBase.Text = "Z Detection Base:";
            // 
            // lbl_RangeHeight
            // 
            this.lbl_RangeHeight.AutoSize = true;
            this.lbl_RangeHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_RangeHeight.Location = new System.Drawing.Point(13, 118);
            this.lbl_RangeHeight.Name = "lbl_RangeHeight";
            this.lbl_RangeHeight.Size = new System.Drawing.Size(124, 13);
            this.lbl_RangeHeight.TabIndex = 1;
            this.lbl_RangeHeight.Text = "Range image height:";
            // 
            // lbl_ZHeight
            // 
            this.lbl_ZHeight.AutoSize = true;
            this.lbl_ZHeight.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_ZHeight.Location = new System.Drawing.Point(18, 37);
            this.lbl_ZHeight.Name = "lbl_ZHeight";
            this.lbl_ZHeight.Size = new System.Drawing.Size(119, 13);
            this.lbl_ZHeight.TabIndex = 1;
            this.lbl_ZHeight.Text = "Z Detection Height:";
            // 
            // nud_RangeHeight
            // 
            this.nud_RangeHeight.Location = new System.Drawing.Point(147, 116);
            this.nud_RangeHeight.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.nud_RangeHeight.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nud_RangeHeight.Name = "nud_RangeHeight";
            this.nud_RangeHeight.Size = new System.Drawing.Size(50, 20);
            this.nud_RangeHeight.TabIndex = 9;
            this.nud_RangeHeight.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nud_RangeHeight.ValueChanged += new System.EventHandler(this.CameraPropery_ValueChanged);
            // 
            // nud_StepsPerLine
            // 
            this.nud_StepsPerLine.Location = new System.Drawing.Point(147, 138);
            this.nud_StepsPerLine.Maximum = new decimal(new int[] {
            32767,
            0,
            0,
            0});
            this.nud_StepsPerLine.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nud_StepsPerLine.Name = "nud_StepsPerLine";
            this.nud_StepsPerLine.Size = new System.Drawing.Size(50, 20);
            this.nud_StepsPerLine.TabIndex = 9;
            this.nud_StepsPerLine.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nud_StepsPerLine.ValueChanged += new System.EventHandler(this.LineScane_ValueChanged);
            // 
            // lbl_SPL
            // 
            this.lbl_SPL.AutoSize = true;
            this.lbl_SPL.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_SPL.Location = new System.Drawing.Point(48, 140);
            this.lbl_SPL.Name = "lbl_SPL";
            this.lbl_SPL.Size = new System.Drawing.Size(89, 13);
            this.lbl_SPL.TabIndex = 1;
            this.lbl_SPL.Text = "Steps per line:";
            // 
            // nud_ZHeight
            // 
            this.nud_ZHeight.Location = new System.Drawing.Point(147, 34);
            this.nud_ZHeight.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nud_ZHeight.Name = "nud_ZHeight";
            this.nud_ZHeight.Size = new System.Drawing.Size(50, 20);
            this.nud_ZHeight.TabIndex = 9;
            this.nud_ZHeight.ValueChanged += new System.EventHandler(this.ProfileCamera_ValueChanged);
            // 
            // nud_ZBase
            // 
            this.nud_ZBase.Location = new System.Drawing.Point(147, 10);
            this.nud_ZBase.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
            this.nud_ZBase.Name = "nud_ZBase";
            this.nud_ZBase.Size = new System.Drawing.Size(50, 20);
            this.nud_ZBase.TabIndex = 9;
            this.nud_ZBase.Value = new decimal(new int[] {
            35,
            0,
            0,
            0});
            this.nud_ZBase.ValueChanged += new System.EventHandler(this.ProfileCamera_ValueChanged);
            // 
            // lbl_AcqError
            // 
            this.lbl_AcqError.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_AcqError.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lbl_AcqError.Location = new System.Drawing.Point(15, 123);
            this.lbl_AcqError.Name = "lbl_AcqError";
            this.lbl_AcqError.Size = new System.Drawing.Size(204, 20);
            this.lbl_AcqError.TabIndex = 10;
            this.lbl_AcqError.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lbl_AcqError.Visible = false;
            // 
            // ckb_Continuous
            // 
            this.ckb_Continuous.Appearance = System.Windows.Forms.Appearance.Button;
            this.ckb_Continuous.Location = new System.Drawing.Point(201, 47);
            this.ckb_Continuous.Name = "ckb_Continuous";
            this.ckb_Continuous.Size = new System.Drawing.Size(187, 26);
            this.ckb_Continuous.TabIndex = 6;
            this.ckb_Continuous.Text = "Continuous";
            this.ckb_Continuous.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.ckb_Continuous.UseVisualStyleBackColor = true;
            this.ckb_Continuous.CheckedChanged += new System.EventHandler(this.ckb_Continuous_CheckedChanged);
            // 
            // pnl_CameraMode
            // 
            this.pnl_CameraMode.Controls.Add(this.rdb_Range);
            this.pnl_CameraMode.Controls.Add(this.rdb_InstensityWG);
            this.pnl_CameraMode.Location = new System.Drawing.Point(245, 171);
            this.pnl_CameraMode.Name = "pnl_CameraMode";
            this.pnl_CameraMode.Size = new System.Drawing.Size(142, 46);
            this.pnl_CameraMode.TabIndex = 8;
            // 
            // rdb_Range
            // 
            this.rdb_Range.AutoSize = true;
            this.rdb_Range.Location = new System.Drawing.Point(3, 26);
            this.rdb_Range.Name = "rdb_Range";
            this.rdb_Range.Size = new System.Drawing.Size(57, 17);
            this.rdb_Range.TabIndex = 6;
            this.rdb_Range.TabStop = true;
            this.rdb_Range.Text = "Range";
            this.rdb_Range.UseVisualStyleBackColor = true;
            this.rdb_Range.CheckedChanged += new System.EventHandler(this.ProfileCamera_ValueChanged);
            // 
            // rdb_InstensityWG
            // 
            this.rdb_InstensityWG.AutoSize = true;
            this.rdb_InstensityWG.Checked = true;
            this.rdb_InstensityWG.Location = new System.Drawing.Point(3, 3);
            this.rdb_InstensityWG.Name = "rdb_InstensityWG";
            this.rdb_InstensityWG.Size = new System.Drawing.Size(136, 17);
            this.rdb_InstensityWG.TabIndex = 7;
            this.rdb_InstensityWG.TabStop = true;
            this.rdb_InstensityWG.Text = "Instensity with Graphics";
            this.rdb_InstensityWG.UseVisualStyleBackColor = true;
            this.rdb_InstensityWG.CheckedChanged += new System.EventHandler(this.ProfileCamera_ValueChanged);
            // 
            // btn_SyncAcquire
            // 
            this.btn_SyncAcquire.Location = new System.Drawing.Point(9, 47);
            this.btn_SyncAcquire.Name = "btn_SyncAcquire";
            this.btn_SyncAcquire.Size = new System.Drawing.Size(176, 26);
            this.btn_SyncAcquire.TabIndex = 5;
            this.btn_SyncAcquire.Text = "Acquire";
            this.btn_SyncAcquire.UseVisualStyleBackColor = true;
            this.btn_SyncAcquire.Click += new System.EventHandler(this.btn_SyncAcquire_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(242, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Camera mode:";
            // 
            // lbl_SizeSpec
            // 
            this.lbl_SizeSpec.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lbl_SizeSpec.ForeColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.lbl_SizeSpec.Location = new System.Drawing.Point(242, 316);
            this.lbl_SizeSpec.Name = "lbl_SizeSpec";
            this.lbl_SizeSpec.Size = new System.Drawing.Size(165, 20);
            this.lbl_SizeSpec.TabIndex = 1;
            // 
            // mCogDisplay
            // 
            this.mCogDisplay.ColorMapLowerClipColor = System.Drawing.Color.Black;
            this.mCogDisplay.ColorMapLowerRoiLimit = 0D;
            this.mCogDisplay.ColorMapPredefined = Cognex.VisionPro.Display.CogDisplayColorMapPredefinedConstants.None;
            this.mCogDisplay.ColorMapUpperClipColor = System.Drawing.Color.Black;
            this.mCogDisplay.ColorMapUpperRoiLimit = 1D;
            this.mCogDisplay.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mCogDisplay.Location = new System.Drawing.Point(448, 3);
            this.mCogDisplay.MouseWheelMode = Cognex.VisionPro.Display.CogDisplayMouseWheelModeConstants.Zoom1;
            this.mCogDisplay.MouseWheelSensitivity = 1D;
            this.mCogDisplay.Name = "mCogDisplay";
            this.mCogDisplay.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("mCogDisplay.OcxState")));
            this.mCogDisplay.Size = new System.Drawing.Size(596, 568);
            this.mCogDisplay.TabIndex = 0;
            // 
            // mCogDisplayStatusBar
            // 
            this.mCogDisplayStatusBar.CoordinateSpaceName = "*\\#";
            this.mCogDisplayStatusBar.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mCogDisplayStatusBar.Location = new System.Drawing.Point(448, 577);
            this.mCogDisplayStatusBar.Name = "mCogDisplayStatusBar";
            this.mCogDisplayStatusBar.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.mCogDisplayStatusBar.Size = new System.Drawing.Size(596, 24);
            this.mCogDisplayStatusBar.TabIndex = 1;
            // 
            // txt_Message
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.txt_Message, 2);
            this.txt_Message.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_Message.Location = new System.Drawing.Point(5, 609);
            this.txt_Message.Margin = new System.Windows.Forms.Padding(5);
            this.txt_Message.Multiline = true;
            this.txt_Message.Name = "txt_Message";
            this.txt_Message.ReadOnly = true;
            this.txt_Message.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txt_Message.Size = new System.Drawing.Size(1037, 90);
            this.txt_Message.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1047, 704);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(532, 437);
            this.Name = "Form1";
            this.Text = "Cognex server";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Shown += new System.EventHandler(this.Form1_Shown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.pnl_Controls.ResumeLayout(false);
            this.pnl_Controls.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtZOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtZScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtXScale)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtFirstPixelLocation)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDistancePerCycle)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtTimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtTransportTimeout)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtPacketSize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtLatencyLevel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtExposure)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtContrast)).EndInit();
            this.pnl_ImageSourceSettings.ResumeLayout(false);
            this.pnl_ImageSourceSettings.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtZDetectionSampling)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtDetectionSensitivity)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_RangeHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_StepsPerLine)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ZHeight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nud_ZBase)).EndInit();
            this.pnl_CameraMode.ResumeLayout(false);
            this.pnl_CameraMode.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mCogDisplay)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Cognex.VisionPro.Display.CogDisplay mCogDisplay;
        private Cognex.VisionPro.CogDisplayStatusBarV2 mCogDisplayStatusBar;
        private System.Windows.Forms.TextBox txt_Message;
        private System.Windows.Forms.Panel pnl_Controls;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown txtContrast;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown txtExposure;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel pnl_ImageSourceSettings;
        private System.Windows.Forms.CheckBox ckb_TestEncoder;
        private System.Windows.Forms.Label lbl_ZBase;
        private System.Windows.Forms.Label lbl_RangeHeight;
        private System.Windows.Forms.Label lbl_ZHeight;
        private System.Windows.Forms.NumericUpDown nud_RangeHeight;
        private System.Windows.Forms.NumericUpDown nud_StepsPerLine;
        private System.Windows.Forms.Label lbl_SPL;
        private System.Windows.Forms.NumericUpDown nud_ZHeight;
        private System.Windows.Forms.NumericUpDown nud_ZBase;
        private System.Windows.Forms.Label lbl_AcqError;
        private System.Windows.Forms.CheckBox ckb_Continuous;
        private System.Windows.Forms.Panel pnl_CameraMode;
        private System.Windows.Forms.RadioButton rdb_Range;
        private System.Windows.Forms.RadioButton rdb_InstensityWG;
        private System.Windows.Forms.Button btn_SyncAcquire;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbl_SizeSpec;
        private System.Windows.Forms.NumericUpDown txtTransportTimeout;
        private System.Windows.Forms.NumericUpDown txtPacketSize;
        private System.Windows.Forms.NumericUpDown txtLatencyLevel;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown txtTimeout;
        private System.Windows.Forms.ComboBox cboxTriggerModel;
        private System.Windows.Forms.CheckBox cbTriggerLowToHigh;
        private System.Windows.Forms.CheckBox cbTriggerEnabled;
        private System.Windows.Forms.NumericUpDown txtDetectionSensitivity;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox cbHighDynamicRange;
        private System.Windows.Forms.NumericUpDown txtZDetectionSampling;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton rbLaserModeOff;
        private System.Windows.Forms.RadioButton rbLaserModeOn;
        private System.Windows.Forms.RadioButton rbLaserModeStrobed;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.CheckBox cbAcquirePositiveEncoderDirection;
        private System.Windows.Forms.ComboBox cboxEndoderResolution;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown txtDistancePerCycle;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label txtEncoderCounter;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.NumericUpDown txtFirstPixelLocation;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox cbAutoCorrectPixelRowOrder;
        private System.Windows.Forms.CheckBox cbIgnoreTooFastEncoder;
        private System.Windows.Forms.CheckBox cbIgnoreBackwardMotionBetweenAcquires;
        private System.Windows.Forms.NumericUpDown txtZOffset;
        private System.Windows.Forms.Label txtYScale;
        private System.Windows.Forms.NumericUpDown txtZScale;
        private System.Windows.Forms.NumericUpDown txtXScale;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label16;

    }
}

