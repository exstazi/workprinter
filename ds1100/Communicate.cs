﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using System.Drawing;
using System.Drawing.Imaging;

using System.IO;

namespace ProfileCameraAcquisition
{
    enum CodeHeader : byte
    {
        CONNECT = 0x01,
        DISCONNECT = 0x02,
        STATUS = 0x03,
        ERROR = 0x04,

        WRITE_PARAM = 0x10,

        LIVE_FRAME = 0x20,

        ACQUIRE = 0x30,
        CONTINUE = 0x31,
        STOP = 0x32,

        //Profile camera params
        CAMERA_MODE = 0x50,
        DETECTION_SENSITIVITY = 0x51,
        HIGH_DYNAMIC_RANGE = 0x53,
        LASER_MODE = 0x54,
        DETECTION_BASE = 0x55,
        DETECTION_HEIGHT = 0x56,
        DETECTION_SAMPLING = 0x58,

        //Range image params
        AUTO_CORRECT_PIXEL_ROW_ORDER = 0x5a,
        FIRST_PIXEL_LOCATION = 0x5c,
        X_SCALE = 0x5f,
        Y_SCALE = 0x62,
        Z_OFFSET = 0x63,
        Z_SCALE = 0x66,

        //Line scane params
        ACQUIRE_DIRECTION_POSITIVE= 0x69,
        CURRENT_ENCODER_COUNT = 0x6a,
        DISTANCE_PER_CYCLE = 0x6b,
        ENCODER_OFFSET = 0x6e,
        ENCODER_RESOLUTION = 0x70,
        IGNORE_BACKWARDS_MOTION_BETWEEN_ACQUIRES = 0x71,
        IGNORE_TOO_FAST_ENCODER = 0x72,
        PROFILE_CAMERA_ACQUIRE_DIRECTION = 0x74,
        PROFILE_CAMERA_POSITIVE_ENCODER_DIRECTION = 0x75,
        RESET_COUNTER_ON_HARDWARE_TRIGGER = 0x76,
        START_ACQ_ON_ENCODER_COUNT = 0x77,
        TRIGGER_FROM_ENCODER = 0x7a,
        USE_SINGLE_CHANNEL = 0x7b,   

        //GigEVision transport params
        LATENCY_LEVEL = 0x7c,
        PACKET_SIZE = 0x7d,
        TRANSPORT_TIMEOUT = 0x7f,

        //Trigger params
        TRIGGER_ENABLED = 0x80,
        TRIGGER_LOW_TO_HIGH = 0x81,
        TRIGGER_MODEL = 0x82,

        //Timeout params
        TIMEOUT = 0x83,
        TIMEOUT_ENABLED = 0x84,

        //
        CONSTRAST = 0x90,
        EXPOSURE = 0x91,
    }

    class Communicate
    {
        private Thread ThreadProcces;
        private Socket Listener;
        private Socket Handler;

        private IPEndPoint ipEndPoint = new IPEndPoint(IPAddress.Any, 12321);

        private static ImageCodecInfo jpgEncoder = GetEncoder(ImageFormat.Jpeg);
        private static EncoderParameters encParams = new EncoderParameters(1);
        private static EncoderParameter encParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, 80L);

        public delegate void CommandReadyEventHandler(object sender, EventArgs e, CodeHeader command, byte[] val);
        public event CommandReadyEventHandler CommandReady;

        public Communicate()
        {
            encParams.Param[0] = encParam;

            ThreadProcces = new Thread(Start);
            ThreadProcces.Start();
        }

        public void Stop() 
        {
            if (Handler != null) 
            {
                Handler.Close();
            }
            Listener.Close();
            ThreadProcces.Abort();
        }

        private void Send(byte[] msg)
        {
            if (Handler != null)
            {
                Handler.Send(msg);
            }
        }

        public void SendFrame(Bitmap map)
        {
            Bitmap reMap = new Bitmap(map, 640, 480);

            Stream stream = new MemoryStream();

            reMap.Save(stream, jpgEncoder, encParams);
            Console.WriteLine(stream.Length);

            stream.Position = 0;
            byte[] bytes = new byte[stream.Length + 5];
            stream.Read(bytes, 5, (int)stream.Length);

            byte[] size = BitConverter.GetBytes(stream.Length);

            bytes[0] = 0x20;
            bytes[1] = size[0];
            bytes[2] = size[1];
            bytes[3] = size[2];
            bytes[4] = size[3];

            Send(bytes);
        }

        private void Start()
        {
            Listener = new Socket(IPAddress.Any.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            byte[] bytes = new byte[128];
            byte[] buffer = new byte[1024];
            int offset = 0;


            CodeHeader command = 0;
            int size = 0;
            byte[] data;

            try
            {
                Listener.Bind(ipEndPoint);
                Listener.Listen(1);

                while (true)
                {
                    Console.WriteLine("Ожидаем соединение через порт {0}", ipEndPoint);
                    Handler = Listener.Accept();
                    Console.WriteLine("Соединение установлено.");

                    while (true)
                    {
                        try
                        {
                            int bytesRec = Handler.Receive(bytes);
                            Buffer.BlockCopy(bytes, 0, buffer, offset, bytesRec);
                            offset += bytesRec;

                            Console.WriteLine("Recieve data length: " + offset);

                            if (offset >= 5)
                            {
                                command = (CodeHeader)buffer[0];
                                size = BitConverter.ToInt32(buffer, 1);
                                if (offset >= size + 5)
                                {
                                    data = new byte[size];
                                    Buffer.BlockCopy(buffer, 5, data, 0, size);
                                    Buffer.BlockCopy(buffer, size + 5, buffer, 0, offset - size - 5);
                                    offset -= size + 5;
                                    this.CommandReady(this, EventArgs.Empty, command, data);
                                    Console.WriteLine("Command: " + command + "; Size: " + size);
                                }
                                else
                                {
                                    Console.WriteLine("Wait next message");
                                }
                            }                            
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine(ex.Message);
                            break;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }

        private void AnalyzeResponce(CodeHeader command, byte[] buffer)
        {
            switch (command)
            {
                case CodeHeader.ACQUIRE:
                    break;

                case CodeHeader.CONTINUE:

                    break;

            }
        }

        private static ImageCodecInfo GetEncoder(ImageFormat format)
        {

            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageDecoders();

            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }

    }
}
