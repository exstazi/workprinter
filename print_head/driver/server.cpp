#include "server.h"

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>



SocketServer::SocketServer(PrintHead* head)
{
	buffer = new char[SIZE_BUFFER];
	_status = WORK_SERVER;
	_printhead = head;
}

void SocketServer::listenerClient()
{
	_printhead->setLedStatus( LED_CLIENT_CONNECT, true );
	uint32_t param = 0;
	char command = 0x00;
	sendStatus( READ_STATE_HEAD, _printhead->_param->state );

	while (1) {
		command = recvHeader( param );
		#ifdef DEBUG
			std::cout << "Read command " << (int)command << ". Value param = " << param << std::endl;
		#endif
		
		switch (command) {
			case TRANSFER_DATA:
				downloadFile( param );
				_printhead->setDataForPrint( buffer, param );
				sendStatus( TRANSFER_DATA );
			break;
			
			case SIGNAL_PRINT:			
			case SIGNAL_PRINT_PERIOD:
			case SIGNAL_PRINT_ENCODER:
			case SIGNAL_PRINT_STOP:
				_printhead->setCommand( command );
				sendStatus( command );
			break;

			case GET_BASE_PARAM:
				sendStatus( READ_COUNT_PRINT, _printhead->getCountPrint() );
				sendStatus( READ_TEMP_1, _printhead->getTemperature(0) );
				sendStatus( READ_TEMP_2, _printhead->getTemperature(1) );
				sendStatus( READ_TEMP_3, _printhead->getTemperature(2) );
				
				sendStatus( READ_STATUS, _printhead->getStatusHead() );
				sendStatus( READ_STATE_HEAD, _printhead->_param->state );
			break;

			case USE_TRIGGER:
				_printhead->setPrintingUsingTrigger( param );
			break;

			case USE_DELAY_FOR_PRINT:
				_printhead->setDelayBeforePrint( param );
			break;

			case READ_STATUS:
				sendStatus( READ_STATUS, _printhead->getStatusHead() );
			break;

			case WRITE_PRINT_PERIOD:
				_printhead->setPrintPeriod( param );
			break;

			case WRITE_RESOLUTION:
				_printhead->setResolution( param );
			break;

			case WRITE_HEATING_TIME:
				_printhead->setHeatingTime( param );
			break;

			case WRITE_IP_ADDRESS:
				downloadIp();	
				_status = WORK_SERVER;
				_printhead->setCommand( SIGNAL_PRINT_STOP );
				return;			
			break;

			case WRITE_TYPE_HEAD:
				_printhead->setTypeHead( param );
			break;

			case READ_COUNT_PRINT:
				sendStatus( READ_COUNT_PRINT, _printhead->getCountPrint() );
			break;
			
			case READ_TEMP_1:
				sendStatus( READ_TEMP_1, _printhead->getTemperature(0) );
			break;
			
			case READ_TEMP_2:
				sendStatus( READ_TEMP_1, _printhead->getTemperature(1) );
			break;
			
			case READ_TEMP_3:
				sendStatus( READ_TEMP_1, _printhead->getTemperature(2) );
			break;
			
			case READ_TEMP_ALL:
				sendStatus( READ_TEMP_1, _printhead->getTemperature(0) );
				sendStatus( READ_TEMP_2, _printhead->getTemperature(1) );
				sendStatus( READ_TEMP_3, _printhead->getTemperature(2) );
			break;
			
			case READ_STATE_HEAD:
				sendStatus( READ_STATE_HEAD, _printhead->_param->state );
			break;
			
			case WRITE_TASK_HEATER:
				_printhead->setTaskHeater( param );
			break;
			
			case WRITE_WORK_HEATER:
				_printhead->setWorkHeater( param );
			break;
			
			case WRITE_FIRMWARE:
				downloadFirmware(param);
				sendStatus( WRITE_FIRMWARE );				 
			break;
			
			case UPDATE_FIRMWARE:
				#ifdef DEBUG
					std::cout << "Command update firmware" << std::cout;
					std::cout << "Start watchdog" << std::cout;					
				#endif
				sendStatus( UPDATE_FIRMWARE );
				_status = CLOSE_SERVER;
				_printhead->setCommand( CLOSE );
				system( "exec /home/pi/printhead/watchdog");
				return ;
				
			case UPDATE_WATCHDOG:
				downloadFirmware(param);
				system( "rm watchdog" );
				system( "mv a.out.new watchdog" );
				sendStatus( UPDATE_WATCHDOG );
			break;
						
			case READ_VERSION:
				sendStatus( READ_VERSION, 123 );
			break;
		
			case CLOSE:
				_status = CLOSE_SERVER;
				_printhead->setCommand( CLOSE );
				return ;

			case REBOOT:
				_status = CLOSE_SERVER;
				_printhead->setCommand( CLOSE );
				system( "reboot" );
				return ;

			case DISCONNECT:
				_status = WORK_SERVER;
				_printhead->setCommand( SIGNAL_PRINT_STOP );
				return;
		}
	}
}

ParamPrintHead* SocketServer::getParam()
{
	return _printhead->_param;
}

char SocketServer::recvHeader(uint32_t& param)
{	
	char bufHeader[SIZE_HEADER];
	int offset = 0;
	while (offset < SIZE_HEADER) {
		offset += recv( _sock, bufHeader + offset, SIZE_HEADER - offset, 0 );
	}
	param = *((uint32_t*)(bufHeader+1));
	return bufHeader[0];
}

int SocketServer::createSocket()
{
	_listener = socket(AF_INET, SOCK_STREAM, 0);
	if (_listener < 0) {
		return _listener;
	}
	
	addr.sin_family = AF_INET;
    addr.sin_port = htons( PORT );
    addr.sin_addr.s_addr = htonl(INADDR_ANY);
    //addr.sin_addr.s_addr = inet_addr( IP_ADDR );
	
	int bindStatus = bind(_listener, (struct sockaddr *)&addr, sizeof(addr));	
	if (bindStatus < 0) {
		return bindStatus;
    }	
	listen(_listener, 1);
	return _listener;	
}

void SocketServer::waitAcceptClient()
{	
	while( _status != CLOSE_SERVER )
	{
		#ifdef DEBUG
			std::cout << "Wait accept client" << std::endl;
		#endif
		_sock = accept(_listener, NULL, NULL);
		if (_sock < 0) {
			continue ;
		}
		#ifdef DEBUG
			std::cout << "Connected client" << std::endl;		
		#endif
		_status = LISTEN_SERVER;
		listenerClient();
		close( _sock );				
		_printhead->setLedStatus( LED_CLIENT_CONNECT, false );
	}	 
	#ifdef DEBUG
		std::cout << "Close wait accept client" << std::endl;
	#endif
}

void SocketServer::closeSocket()
{
	_status = CLOSE_SERVER;
	close( _listener );
	delete buffer;
}

void SocketServer::downloadFile( uint32_t size )
{
	#ifdef DEBUG
	std::cout << "Command transfer data with size " << size  << std::endl; 	
	#endif
	
	if (size < 0) {
		return ;
	}
	uint32_t offset = 0;
	
	while ((size - offset) > 0) {
		offset += recv( _sock, buffer + offset, SIZE_BUFFER, 0 );
		#ifdef DEBUG
			std::cout << "Read " << offset << " bytes" << std::endl;
		#endif
	}
	
	#ifdef DEBUG
	std::cout << "End transfer. Size - offset = " <<  size - offset << std::endl;
	#endif
}

void SocketServer::downloadIp()
{
	uint32_t* ipAddr = new uint32_t[4];
	#ifdef DEBUG
	std::cout << "Command download ip addr " << std::endl;
	#endif
	uint32_t val;
	for (int i = 0; i < 4; ++i) {
		recv( _sock, &ipAddr[i], sizeof( val ), 0 );
	}
	#ifdef DEBUG
	std::cout << "End transfer." << std::endl;
	#endif
	
	std::ofstream confStream( "/home/pi/printhead/conf/ip.conf" );
	std::ostringstream oss;
	oss << "ifconfig eth0 " << ipAddr[0] << '.' << ipAddr[1] << '.' << ipAddr[2] << '.' << ipAddr[3];
	confStream << ipAddr[0] << " " << ipAddr[1] << " " << ipAddr[2] << " " << ipAddr[3];			
	confStream.close();
	
	std::cout << oss.str().c_str();
	
	system( oss.str().c_str() );
	oss.clear();
}

void SocketServer::downloadFirmware(uint32_t size)
{
	#ifdef DEBUG
	std::cout << "Command download firmware with size " << size  << std::endl; 	
	#endif
	
	if (size < 0) {
		return ;
	}
	
	uint32_t offset = 0;
	uint32_t len = 0;
	char buf[SIZE_BUFFER];
	std::ofstream confstream( "/home/pi/printhead/a.out.new" );
	
	while ((size - offset) > 0) {
		len = recv( _sock, buf, SIZE_BUFFER, 0 );
		offset += len;
		buf[len] = '\0';
		confstream << buf;
		#ifdef DEBUG
			std::cout << "Read " << offset << " bytes" << std::endl;
		#endif
	}
	confstream.close();
	#ifdef DEBUG
	std::cout << "End transfer. Size - offset = " <<  size - offset << std::endl;
	#endif
}

void SocketServer::sendStatus( char command, uint32_t param )
{
	send( _sock, &command, sizeof( command ), 0 );
	send( _sock, &param, sizeof( param ), 0 );
}


SocketServer::~SocketServer()
{
	closeSocket();
}
