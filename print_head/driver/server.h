#ifndef __SERVER_H__
#define __SERVER_H__

#define DEBUG 

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>


#include "codeheadermessages.h"
#include "printhead.h"

#define PORT 55255

#define SIZE_HEADER 5
#define SIZE_BUFFER 65535

class SocketServer
{	
public:
	SocketServer( PrintHead* head );
	
	int createSocket();
	void waitAcceptClient();
	void closeSocket();
	~SocketServer();

protected:

	ParamPrintHead* getParam();
	void listenerClient();
	void downloadFile( uint32_t size );
	void downloadIp();
	void downloadFirmware(uint32_t size);
	inline char recvHeader( uint32_t& param );
	void sendStatus( char command, uint32_t param = 0 );

public:
	static const uint8_t WORK_SERVER = 0x01;
	static const uint8_t LISTEN_SERVER = 0x02;
	static const uint8_t CLOSE_SERVER = 0xAB;

protected:
	int _sock;
	int _listener; 
    struct sockaddr_in addr;

	char _status;
	char *buffer;
	
	PrintHead* _printhead;
};

#endif
