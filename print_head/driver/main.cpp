#include <iostream>
#include <fstream>
#include <string>
#include <sstream>

//#define DEBUG

#include "server.h"
#include "printhead.h"

int main()
{
    #ifdef DEBUG
    std::cout << "Start programm." << std::endl;    
    #endif
    
    std::ifstream confStream( "/home/pi/printhead/conf/ip.conf" );
	std::ostringstream oss;
    int ip[4];
	confStream >> ip[0] >> ip[1] >> ip[2] >> ip[3];	
	oss << "ifconfig eth0 " << ip[0] << '.' << ip[1] << '.' << ip[2] << '.' << ip[3];
	confStream.close();
	
    system( oss.str().c_str() );
    oss.clear();
    
    PrintHead* head = new PrintHead();
   
    SocketServer *server = new SocketServer( head );
	while (server->createSocket() < 0) {
		perror( "Error create socket: " );
		sleep( 3 );
	}	
	
	if (fork()) {
		
		/*if (fork()) {
			head->executeTemperature();			
			head->setLedStatus( LED_DRIVER_START, false );
			
			std::cout << "Fork read temperature closed" << std::endl;
			exit(0);
		}	*/
		
		head->execute();
		head->setLedStatus( LED_DRIVER_START, false );
		#ifdef DEBUG
		std::cout << "Fork execute printing closed" << std::endl;
		#endif
		exit( 0 );
	}
	
	server->waitAcceptClient();
	server->closeSocket();
	head->setLedStatus( LED_DRIVER_START, false );
	//*/
	#ifdef DEBUG
	std::cout << "Thread main closed" << std::endl;
	#endif
	return 0;
}

