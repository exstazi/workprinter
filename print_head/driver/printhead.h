#ifndef PRINTHEAD_H
#define PRINTHEAD_H

#include <iostream>
#include <fstream>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include "codeheadermessages.h"
#include "rpi.h"


//#define DEBUG

typedef unsigned char uint8_t;
typedef unsigned short uint16_t;
typedef unsigned int uint32_t;
typedef unsigned long long uint64_t;

struct ParamPrintHead
{
    uint8_t command;
    uint8_t errorCode;
    uint8_t typeHead;
    uint8_t state;
    uint32_t resolution;
    uint32_t printPeriod;
    uint32_t heatingTime;
    uint32_t countPrint;
    uint32_t temp1;
    uint32_t temp2;
    uint32_t temp3;
    uint32_t taskHeater;
    uint32_t sizeBuffer;
    
    uint8_t delayBeforePrint;
    uint8_t printWithTrigger;
    uint8_t workHeater;
};

#define CONFIG_FILE_NAME "conf/main.conf"
#define SM_BUFFER_SIZE 65535
#define COUNT_TEMPERATURE_SENSORS 3
#define OFFSET_BUFFER sizeof( ParamPrintHead )
#define SM_SIZE OFFSET_BUFFER + SM_BUFFER_SIZE

#define HIST 0.5

class PrintHead
{
public:
	friend class SocketServer;
	
    PrintHead();
    ~PrintHead();

    void initGPIO();

    void setCommand(char command);
    char getCommand();
    void clearCommand();
    
    void setTypeHead(uint8_t type);
    void setResolution(uint32_t resolution);
    void setPrintPeriod(uint32_t period);
    void setDelayBeforePrint(bool status);
    void setHeatingTime(uint32_t time);
    void setPrintingUsingTrigger(bool status);
    void setDataForPrint(char* data, uint32_t size);
    void setTaskHeater(int temperature);
    void setWorkHeater(bool status);
    
    void setLedStatus(uint8_t led, bool status);

    uint8_t getTypeHead();
    uint8_t getStatusHead();
    uint8_t getErrorCode();
    uint32_t getResolution();
    uint32_t getPrintPeriod();
    uint32_t getHeatingTime();
    uint32_t getCountPrint();

    bool isReadyPrint();
    bool isPrintTrigger();
    bool isDelayBeforePrint();
    bool isWorkHeater();

    void execute();
    void executeTemperature();
    
	uint32_t getTemperature(uint8_t sensor);

    void setPointToHead();
    bool isFirstSequence();
    char* next();

    void initDefaultSettings();
    void serialize();
    void deserialize();
    
protected:    
    void waitTrigger();
    void waitSignalEncoder();
    void writeSignal(char* data);
	inline void delay( uint32_t time );
	
	inline void setCountPrint(uint32_t count);

protected:
    char* _shmBuffer;

    bool _firstSequence;
    uint32_t _offset;
    uint32_t _sizeDataForPrint;

    uint32_t _countPrint;
    uint8_t _sizeCharForTypeHead;
    
    uint32_t* _temperature;
    
    ParamPrintHead* _param;
	
    int mem_fd;
    void* gpio_map;
    volatile unsigned *gpio;   
};

#endif
