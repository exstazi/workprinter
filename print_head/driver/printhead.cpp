#include "printhead.h"

PrintHead::PrintHead()
{
    int shm = 0;
    if ((shm = shm_open( "MEMORY_SHARED_KEY", O_CREAT |  O_RDWR , 0777 )) == -1) {
        return ;
    }   
    if (ftruncate( shm, SM_SIZE ) == -1) {
        return ;
    }
    
    _shmBuffer = (char *)mmap( 0, SM_SIZE, PROT_WRITE | PROT_READ, MAP_SHARED, shm, 0 );
    
    if (_shmBuffer == (char*)-1) {
        return ;
    }
    memset( _shmBuffer, 0, SM_SIZE );
	
	_param = (ParamPrintHead*)_shmBuffer;

    _sizeCharForTypeHead = 16;
    _countPrint = 0;
    
    _temperature = new uint32_t[COUNT_TEMPERATURE_SENSORS];
    
    deserialize();
    initGPIO();
    
    
}

PrintHead::~PrintHead()
{
    delete _shmBuffer;
}

void PrintHead::initGPIO()
{
    if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
        std::cout << "Can't open /dev/mem" << std::endl;
        return ;
    }

    gpio_map = mmap(
        NULL,             //Any adddress in our space will do
        BLOCK_SIZE,       //Map length
        PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
        MAP_SHARED,       //Shared with other processes
        mem_fd,           //File to map
        GPIO_BASE         //Offset to GPIO peripheral
    );

    close(mem_fd); //No need to keep mem_fd open after mmap

    if (gpio_map == MAP_FAILED) {
        std::cout << "nmap error " << (int)gpio_map << std::endl;
        return ;
    }

    gpio = (volatile unsigned *)gpio_map;

    INP_GPIO( CLK );
    INP_GPIO( DATA );
    INP_GPIO( DATA2 );
    INP_GPIO( LATCH );
    INP_GPIO( OE );
    
    INP_GPIO( HVOK );
    INP_GPIO( HTTERM );
    
    INP_GPIO( INPUT1 );
    INP_GPIO( INPUT2 );
    
    INP_GPIO( I2C_SDA );
    INP_GPIO( I2C_SCL );
    
    INP_GPIO( HEATER );
    
    INP_GPIO( LED_DRIVER_START );
    INP_GPIO( LED_CLIENT_CONNECT );
    INP_GPIO( LED_STATUS_PRINT );
    INP_GPIO( LED_ERROR );
    
    
    GPIO_PULL |= ( 1 << CLK ) | ( 1 << DATA ) | ( 1 << DATA2 ) | ( 1 << LATCH ) | ( 1 << OE ) | ( 1 << I2C_SDA ) | ( 1 << I2C_SCL );
    
    OUT_GPIO( CLK );
    OUT_GPIO( DATA );
    OUT_GPIO( DATA2 );
    OUT_GPIO( LATCH );
    OUT_GPIO( OE );
    
    OUT_GPIO( HEATER );
    
    OUT_GPIO( LED_DRIVER_START );
    OUT_GPIO( LED_CLIENT_CONNECT );
    OUT_GPIO( LED_STATUS_PRINT );
    OUT_GPIO( LED_ERROR );
    
    GPIO_CLR = 1 << HEATER;
    
    GPIO_CLR = 1 << LED_DRIVER_START;
    GPIO_SET = 1 << LED_CLIENT_CONNECT;
    GPIO_SET = 1 << LED_STATUS_PRINT;
    GPIO_SET = 1 << LED_ERROR;
}

uint8_t PrintHead::getStatusHead()
{
    return (uint8_t)((GET_VALUE_GPIO( HVOK ) | ( GET_VALUE_GPIO( HTTERM ) << 1 )) & 255);
}

uint8_t PrintHead::getErrorCode()
{
    return _param->errorCode;
}

void PrintHead::setLedStatus(uint8_t led, bool status)
{
	if (status) {
		GPIO_CLR = 1 << led;
	} else {
		GPIO_SET = 1 << led;
	}
}

bool PrintHead::isReadyPrint() 
{
    return ( _sizeDataForPrint > 0 );
}

void PrintHead::execute()
{
	#ifdef DEBUG
	std::cout << "Start execute print head" << std::endl;
	#endif
    if (isDelayBeforePrint()) {
		#ifdef DEBUG
		std::cout << isDelayBeforePrint() << std::endl;
        std::cout << "Wait " << getHeatingTime() << " sec." << std::endl;
        #endif
        _param->state = STATE_WARMING_UP;
        sleep( getHeatingTime() );
    }
    
    _param->state = STATE_CONNECT;

    while (1) {		
		GPIO_SET = 1 << LED_STATUS_PRINT;
		if (isReadyPrint() ) {
			_param->state = STATE_READY_PRINT;
		} else {
			_param->state = STATE_CONNECT;
		}
        clearCommand();
        while (getCommand() == 0x00) { usleep( 100 ); }
        if (getCommand() == CLOSE) {
			break ;
		}
        if (!isReadyPrint() && getCommand() != TRANSFER_DATA) {
            _param->state = STATE_ERROR;
            continue ;
        }

        switch (getCommand()) {
			case TRANSFER_DATA:
				_sizeDataForPrint = _param->sizeBuffer;
                _countPrint = 0;
                _param->state = STATE_READY_PRINT;
			break;
            case SIGNAL_PRINT:
				GPIO_CLR = 1 << LED_STATUS_PRINT;  
				_param->state = STATE_PRINTING;
				          
				setPointToHead();
                while (getCommand() != SIGNAL_PRINT_STOP
                            && isFirstSequence()) {
                    waitTrigger();
                    writeSignal( next() );
                    usleep( _param->printPeriod );
                }
            break;

            case SIGNAL_PRINT_PERIOD:
				GPIO_CLR = 1 << LED_STATUS_PRINT;
				_param->state = STATE_PRINTING;
				
                while (getCommand() != SIGNAL_PRINT_STOP) {
                    waitTrigger();
                    writeSignal( next() );
                    usleep( _param->printPeriod );
                }
            break;

            case SIGNAL_PRINT_ENCODER:
				GPIO_CLR = 1 << LED_STATUS_PRINT;
				_param->state = STATE_PRINTING;
				
                while (getCommand() != SIGNAL_PRINT_STOP) {
                    waitTrigger();
                    waitSignalEncoder();
                    writeSignal( next() );
                }
            break;
            
            case CLOSE:
				GPIO_SET = 1 << LED_STATUS_PRINT;
				return ;
            break;
        }
    }
}

/*
 * uint8_t stat - 
 * 0 - wait combination start
 * 1 - read address slave
 * 2 - read type transfer
 */
void PrintHead::executeTemperature()
{
	#ifdef DEBUG
	std::cout << "Start execute read temperature" << std::endl;
	#endif
	uint8_t scl = 0x00,
			sda = 0x00,
			state = 0x00,
			address = 0x00,
			offset = 0x00;
	uint32_t temp = 0;
	uint32_t cycle = 0;
	while (getCommand() != CLOSE) {
		
		scl = (GET_VALUE_GPIO( I2C_SCL ) | (scl & 0x02)) & 0x03;
		sda = (GET_VALUE_GPIO( I2C_SDA ) | (sda & 0x02)) & 0x03;
		
		if (state == 0x00 && sda == 0x02 && scl == 0x03) {
			state = 0x01;
			address = 0x00;
			temp = 0;
		} else if (state == 0x01 && scl == 0x01) {
			address = (address << 1) | (sda & 0x01);
			if (++offset == 7) {
				offset = 0x00;
				state = 0x02;
			}
		} else if (state == 0x02 && scl == 0x01) {
			if (sda & 0x01) {
				state = 0x03;
			} else {
				state = 0x00;
			}
		} else if (state == 0x03 && scl == 0x01) {
			state = 0x04;
		} else if (state == 0x04 && scl == 0x01) {
			temp = (temp << 1) | (sda & 0x01);
			if (++offset == 8) {
				offset = 0;
				state = 0x05;
			}
		} else if (state == 0x05 && scl == 0x01) {
			state = 0x06;			
		} else if (state == 0x06 && scl == 0x01) {
			temp = (temp << 1) | (sda & 0x01);
			if (++offset == 8) {
				offset = 0;
				state = 0x07;
			}
		}else if (state == 0x07 && scl == 0x01) {
			temp = temp >> 4;
			if (address == 0x48) {
				_param->temp1 = temp;				
			} else if (address == 0x49) {				
				_param->temp2 = temp;				
			} else {				
				_param->temp3 = temp;				
			}
			usleep( 100 );
			state = 0x00;
		}
				
		scl = scl << 1;
		sda = sda << 1;
		++cycle;
		
		if (isWorkHeater() && cycle > 1000) {
			cycle = 0;
			double avgTemp = ( (_param->temp1 + _param->temp2 + _param->temp3) * 0.0625 ) / 3;
			if (avgTemp > _param->taskHeater + HIST) {
				GPIO_CLR = 1 << HEATER;
			} else if (avgTemp < _param->taskHeater - HIST) {
				GPIO_SET = 1 << HEATER;
			}						
		}
	}
}

uint32_t PrintHead::getTemperature(uint8_t sensor)
{
	if (sensor == 0) {
		return _param->temp1;
	} else if (sensor == 1) {
		return _param->temp2;
	}
	return _param->temp3;
}


void PrintHead::waitTrigger() 
{
    while (getCommand() != SIGNAL_PRINT_STOP
                && isPrintTrigger() && GET_VALUE_GPIO( INPUT1 ) == 0 ) {}
}

void PrintHead::waitSignalEncoder() 
{
    uint8_t value = 0x01, 
			lastValue = 0x01;
    uint32_t currCycleEncoder = 0;
    do
    {
        lastValue = value;
        value = GET_VALUE_GPIO( INPUT2 );
        if (value != 0x00 && lastValue == 0x00) {
			++currCycleEncoder;
            if (currCycleEncoder >= _param->resolution) {
                break ;
            }
        }
    } while (getCommand() != SIGNAL_PRINT_STOP);
}

void PrintHead::writeSignal(char* buf)
{
    GPIO_CLR = ( 1 << DATA ) | ( 1 << DATA2 );  
    uint8_t x = 0x00,
			notx = 0x00;
    
    if (_param->typeHead == 0x00 ) {
		for( int k = 0; k < 64; ++k )
		{
			GPIO_SET = 1 << CLK;
			delay( 60 );
			GPIO_CLR = 1 << CLK;
			delay( 60 );    
		}
	}
				
    for (uint8_t i = 0; i < _sizeCharForTypeHead; ++i) {
		for (int j = 6; j >= 0; j-= 2) {
			x = ( buf[i] >> j ) & 0x03;
			notx = (~x) & 0x03;
			GPIO_CLR = ( (notx>>0x01) << DATA) | ( (notx&0x01) << DATA2);
			GPIO_SET = (1 << CLK) | ((x>>0x01) << DATA) | ( (x&0x01) << DATA2);
			delay( 60 );
            GPIO_CLR = 1 << CLK;
            delay( 60 ); 
		}
	}
       
    GPIO_SET = 1 << DATA2;
    GPIO_CLR = 1 << DATA;
    delay( 70 );


    GPIO_SET = 1 << LATCH;
    delay( 130 );
    GPIO_CLR = ( 1 << LATCH );
    delay( 130 );

    GPIO_SET = 1 << OE;
    delay( 300 );
    GPIO_CLR = 1 << OE;
}

void PrintHead::delay(uint32_t time)
{
	while (--time > 0) {
		asm( "nop" );
	}
}

void PrintHead::setPointToHead()
{
    _offset = 0;
    _firstSequence = true;
}

bool PrintHead::isFirstSequence()
{
    return _firstSequence;
}

char* PrintHead::next() 
{
    char* point = (_shmBuffer + OFFSET_BUFFER) + _offset;
    _offset += _sizeCharForTypeHead;
    if (_offset >= _sizeDataForPrint) {
        _offset = 0;
        _firstSequence = false;
        setCountPrint( ++_countPrint );
    }
    return point;
}

void PrintHead::serialize()
{
    std::ofstream confStream( CONFIG_FILE_NAME );
    
    if (confStream.is_open()) {
        confStream << (isDelayBeforePrint()?1:0) << " " << _param->heatingTime << " " << _param->typeHead;
        confStream.close();
    }
}

void PrintHead::deserialize()
{
    std::ifstream confStream( CONFIG_FILE_NAME );
    
    if (confStream.is_open() ) {
        confStream >> _param->delayBeforePrint >> _param->heatingTime >> _param->typeHead;
        setTypeHead( _param->typeHead );
        confStream.close();
    } else {
        initDefaultSettings();
    }
}

void PrintHead::initDefaultSettings()
{
	setTypeHead( 0x00 );
	setResolution( 100 );
	setPrintPeriod( 1000 );
	setDelayBeforePrint( false );
	setHeatingTime( 60 );
	setPrintingUsingTrigger( false );
	serialize();
}

void PrintHead::setCommand(char command)
{
	_param->command = command;
}

char PrintHead::getCommand()
{
    return _param->command;
}

void PrintHead::clearCommand()
{
    _param->command = 0x00;
}

void PrintHead::setTypeHead(uint8_t type)
{
    _param->typeHead = type;
    _sizeCharForTypeHead = (_param->typeHead==0x01) ? 32 : 16;
    serialize();
}

uint8_t PrintHead::getTypeHead()
{
    return _param->typeHead;
}

void PrintHead::setResolution(uint32_t resolution)
{
	_param->resolution = resolution;
}

uint32_t PrintHead::getResolution()
{
    return _param->resolution;
}

void PrintHead::setPrintPeriod(uint32_t period)
{
	_param->printPeriod = period;
}

uint32_t PrintHead::getPrintPeriod()
{
    return _param->printPeriod;
}

void PrintHead::setDelayBeforePrint(bool status)
{
	_param->delayBeforePrint = (status?1:0);
    serialize();
}

bool PrintHead::isDelayBeforePrint()
{
    return _param->delayBeforePrint;
}

void PrintHead::setHeatingTime(uint32_t time)
{    
	_param->heatingTime = time;
}

uint32_t PrintHead::getHeatingTime()
{
    return _param->heatingTime;
}

void PrintHead::setPrintingUsingTrigger(bool status)
{
	_param->printWithTrigger = (status?1:0);
}

bool PrintHead::isPrintTrigger()
{
	return (_param->printWithTrigger != 0x00 ?  true : false);
}

void PrintHead::setWorkHeater(bool status)
{
	if (status) {
		GPIO_SET = 1 << HEATER;
	} else {
		GPIO_CLR = 1 << HEATER;
	}
	_param->workHeater = (status?1:0);
}

bool PrintHead::isWorkHeater()
{
	return (_param->workHeater != 0x00 ? true : false);
}

void PrintHead::setTaskHeater(int task)
{
	_param->taskHeater = task;
}

void PrintHead::setDataForPrint(char* data, uint32_t size)
{
	_param->sizeBuffer = size;
    memcpy( _shmBuffer + OFFSET_BUFFER, data, sizeof( char ) * size );
    setCommand( TRANSFER_DATA );
}

void PrintHead::setCountPrint(uint32_t count)
{
	_param->countPrint = count;
}

uint32_t PrintHead::getCountPrint()
{
    return _param->countPrint;
}
