/*#include "rpi.h"

#include <iostream>
#include <unistd.h>


void GpioController::waitCommands()
{
	/*int size = 0;
	int offset = 0;
	long tmp = 0;

	while( 1 )
	{
		switch( _mess->getCommandHead() )
		{
			case TRANSFER_DATA:
			    _sizeDataForPrint = _mess->getData2();
			    _mess->commandMade();
			    #ifdef DEBUG
			    std::cout << "RPI. Transfer data. Size = " << _sizeDataForPrint << std::endl;
			    #endif			
			    //size = _mess->getData2();
			break;
		
			case SIGNAL_PRINT:
			    #ifdef DEBUG			    
			    std::cout << "RPI. Signal print." << std::endl;
			    #endif
			    
			    for (unsigned int i = 0; 
			             (i <_sizeDataForPrint) && (_mess->getCommandHead() == SIGNAL_PRINT);
				     i += _printhead.getHeight() ) {
			        
			        waitTrigger( SIGNAL_RPINT );
				writeSignal( _mess->getBuffer() + i );    
				++_printhead.countPrint; 
			    }
			    
			    _mess->commandMade();
			    _mess->writeCommand( readStatus(), 1 );
			    
			    #ifdef DEBUG			    
			    std::cout << "PRI. End print." << std::endl;
			    #endif
				
				for( int i = 0; ( i < size ) && ( _mess->getValue( 0 ) == SIGNAL_PRINT); i += (16 * head) )
				{
					if (head ==0x01 )
						writeSignal( _mess->getBuffer() + i );
					else if (head == 0x02)
						writeFullSignal( _mess->getBuffer() + i );
				}
				
				_mess->writeCommand( 0x00, 0 );	
				_mess->writeCommand( this->readStatus(), 1 );
			break;
			
			case SIGNAL_PRINT_PERIOD:
		            _sizeDataForPrint = _mess->getData2();
			    #ifdef DEBUG
			    std::cout << "Print with period " << period 
			                  << ", use trigger = " << trigger 
					  << ", curr = " << (int)getTrigger()
					  << ". Size data = " << size << std::endl;
			    #endif
			    
			    int i = 0;
			    while (_mess->getCommandHead() == SIGNAL_PRINT_PERIOD) {
			        waitTrigger( SIGNAL_PRINT_PERIOD );
				writeSignal( _mess->getBuffer() + i );
				i += _printhead.getHeight();
				++_printhead.countPrint;
				if ( i > _sizeDataForPrint ) {
				    i = 0;
			        }
			    }
			    
			    #ifdef DEBUG
			    std::cout << "RPI. Stop print with period" << std::endl;
			    #endif
				trigger = _mess->getData1();
				
				
				
				while( _mess->getValue( 0 ) == SIGNAL_PRINT_PERIOD )
				{				
					for( int i = 0; ( i < size ) 
					                  && ( _mess->getValue( 0 ) == SIGNAL_PRINT_PERIOD )
							  && ( trigger == 0x00 || (trigger == 0x01 && getTrigger() == 0x01 )); i += (16 * head) )
					{
                                             writeSignal( _mess->getBuffer() + i );
					}
				}			
			break;
			
			case SIGNAL_PRINT_ENCODER:
			
				offset = 0;
				tmp = period;
				period = 0;
				while( true )
				{
					this->waitSignalEncoder();
					if( _mess->getValue( 0 ) != SIGNAL_PRINT_ENCODER )
					{
						break;
					}
					if (head ==0x01 )
						writeSignal( _mess->getBuffer() + offset );
					else if (head == 0x02)
						writeFullSignal( _mess->getBuffer() + offset );
				
					offset += (16*head);
					if( offset > _mess->getData2() )
					{
						offset = 0;
					}
					//std::cout << "PRINT" << std::endl;
				}
				period = tmp;
				std::cout << "STOP print writh encoder" << std::endl;
				
			break;
			
			case WRITE_PERIOD:
				std::cout << "Edit period value. Last value = " << period;
				this->period = _mess->getData1();
				std::cout << " new value = " << period << std::endl;
				_mess->writeCommand( 0x00, 0 );			
			break;
			
			case WRITE_RESOLUTION:
				std::cout << "Edit resolution value. Last value = " << cycleEncoder;
				this->cycleEncoder = _mess->getData1();
				std::cout << " new value = " << cycleEncoder << std::endl;
				_mess->writeCommand( 0x00, 0 );
			break;
			
			
			
			case CHANGE_HEAD_368:
				std::cout << "Changed head to 368";
				head = 0x01;
				_mess->writeCommand( 0x00, 0 );
			break;
			
			case CHANGE_HEAD_768:
				std::cout << "Change head to 768" << std::endl;
				head = 0x02;
				_mess->writeCommand( 0x00, 0 );
			break;
			
			case SIGNAL_PRINT_STOP:
				std::cout << "Stop signals print" << std::endl;
				_mess->writeCommand( 0x00, 0 );
			break;
			
			case READ_STATUS:
				_mess->writeCommand( 0x00, 0 );
				_mess->writeCommand( this->readStatus(), 1 );
				std::cout << "Status = " << (int)this->readStatus() << std::endl;
				//usleep( 100 );
				//std::cout << "Status: " << (int)GET_VALUE_GPIO( ENCODER1 ) << " " << (int)GET_VALUE_GPIO( ENCODER2 ) << std::endl;		
				//usleep( 100000 );
			break;
		
		
			case CLOSE:
				return ;
			break;		
		}
	}
}

void GpioController::writeFullSignal( char *buf )
{
	GPIO_CLR = ( 1 << DATA ) | ( 1 << DATA2 );
			
	int set = 0;
	int clr = 0;
	
	for( int i = 0; i < 16; ++i )
	{
		for( int j = 7; j >= 0; --j )
		{
			set = clr = 0;
			if( ( buf[i] && ( 1 << j ) ) != 0 )
			{
				set = 1 << DATA;
			}
			else
			{
				clr = 1 << DATA;
			}
			
			if( ( buf[i+8] && ( 1 << j ) ) != 0 )
			{
				set |= 1 << DATA2;
			}
			else
			{
				clr |= 1 << DATA2;
			}
			
			GPIO_SET = ( 1 << CLK ) | set;
			GPIO_CLR = clr;
			delay( 60 );
			GPIO_CLR = 1 << CLK;
			delay( 60 );			
		}
	}
	
	GPIO_SET = 1 << DATA2;
	GPIO_CLR = 1 << DATA;
	delay( 50 );


	GPIO_SET = 1 << LATCH;
	delay( 100 );
	GPIO_CLR = ( 1 << LATCH );
	delay( 100 );

	GPIO_SET = 1 << OE;
	//delay( period );
	delay( 300 );
	GPIO_CLR = 1 << OE;
	usleep( period * 1000 );
}

void GpioController::writeSignal( char *buf )
{
	GPIO_CLR = ( 1 << DATA ) | ( 1 << DATA2 );
		
	for( int k = 0; k < 64; ++k )
	{
		GPIO_SET = 1 << CLK;
		delay( 60 );
		GPIO_CLR = 1 << CLK;
		delay( 60 );	
	}
	
	//GPIO_SET = ( 1 << DATA ) | ( 1 << DATA2 );
	
	int set = 0;
	int clr = 0;
	
	for( int i = 0; i < 8; ++i )
	{
		for( int j = 7; j >= 0; --j )
		{
			set = clr = 0;
			if( ( buf[i] && ( 1 << j ) ) != 0 )
			{
				set = 1 << DATA;
			}
			else
			{
				clr = 1 << DATA;
			}
			
			if( ( buf[i+8] && ( 1 << j ) ) != 0 )
			{
				set |= 1 << DATA2;
			}
			else
			{
				clr |= 1 << DATA2;
			}
			
			GPIO_SET = ( 1 << CLK ) | set;
			GPIO_CLR = clr;
			delay( 60 );
			GPIO_CLR = 1 << CLK;
			delay( 60 );			
		}
	}
	
	GPIO_SET = 1 << DATA2;
	GPIO_CLR = 1 << DATA;
	delay( 50 );


	GPIO_SET = 1 << LATCH;
	delay( 100 );
	GPIO_CLR = ( 1 << LATCH );
	delay( 100 );

	GPIO_SET = 1 << OE;
	delay( 300 );
	GPIO_CLR = 1 << OE;
	//delay( period );
	usleep( period );
}

void GpioController::waitSignalEncoder()
{
	char value = 0x01, 
		 lastValue = 0x01;
	int currCycleEncoder = 0;
	do
	{
		lastValue = value;
		value = GET_VALUE_GPIO( ENCODER1 );
		if( value == 0x01 && lastValue == 0x00 )
		{
			++currCycleEncoder;
			if( currCycleEncoder >= cycleEncoder )
			{
				return ;
			}
		}
	} while( _mess->getValue( 0 ) != SIGNAL_PRINT_STOP );
	
}

int GpioController::getTrigger()
{
	return GET_VALUE_GPIO( ENCODER1 );
}

char GpioController::readStatus()
{
	char status = (GET_VALUE_GPIO( HVOK ) | ( GET_VALUE_GPIO( HTTERM ) << 1 )) & 255;
	return status;
}

void GpioController::delay( int time )
{
	while( --time > 0 ) {
		asm( "nop" );
	}
}

void GpioController::setMessanger( Messanger* _messanger )
{
	_messanger = _messanger;
}
*/
