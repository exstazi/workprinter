#include <iostream>
#include <stdlib.h>
#include <unistd.h>

int main() 
{
	sleep( 1 );
	std::cout << "Start update firmware" << std::cout;
	system( "service printhead stop" );
	system( "chmod 755 a.out.new" );
	
	system( "rm a.out.prev" );
	system( "mv a.out a.out.prev" );
	system( "mv a.out.new a.out" );
	system( "service printhead start" );
	system( "reboot" );
	return 0;
}
