#include <Wire.h>

uint8_t currAddress = 0x48;
uint8_t nextAddress = 0x48;
uint8_t currState = 0x00;

uint8_t currCommand = 0x00;
uint8_t currSetting = 0x00;

char temperature[4][2] = { { 0x08, 0x20 },
                           { 0x01, 0x40 },
                           { 0x20, 0x80 },
                           { 0x41, 0x00 } };

void setup() {
  Wire.onReceive( receiveEvent );
  Wire.onRequest( requestEvent );
  Wire.begin( currAddress );
}

void loop() {
  if (nextAddress > 0x4A ) {
    nextAddress = 0x48;
  }

  if (nextAddress != currAddress ) {
    currAddress = nextAddress;
    Wire.begin(currAddress);
  }
  delay( 10 );
}

void receiveEvent(int howMath) {
  currCommand = Wire.read();
  if ( currCommand == 0x01 ) {
    currSetting = Wire.read();
    Serial.println( (int)currSetting );
  }
}

void requestEvent() {
  if ( currCommand == 0x00 ) {
    Wire.write( temperature[(currSetting >> 5 ) & 0x03], 2 );
    ++nextAddress;
  }
}

