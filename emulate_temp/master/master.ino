#include <Wire.h>

void setup() {
  Wire.begin();
  Serial.begin(9600);
}

void loop() {

  for (uint8_t addr = 0x48; addr < 0x4B; ++addr ) {
    Wire.beginTransmission(addr);
    Wire.write( 0x01 );
    Wire.write( 0x60 );
    Wire.endTransmission();
    
    delayMicroseconds(40);
  
    Wire.beginTransmission(addr);
    Wire.write( 0x00 );
    Wire.endTransmission();
  
    delayMicroseconds(40);
  
    Wire.requestFrom( (int)addr, 2 );
    uint8_t first = Wire.read();
    uint8_t second = Wire.read();
    Serial.print( first );
    Serial.print( " " );
    Serial.println( second );
  
    delay( 262 );
  }

  delay( 300 );
}
