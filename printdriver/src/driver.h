#ifndef __DRIVER_H__
#define __DRIVER_H__

#include "rpi.h"

namespace printhead {

class Driver {

public:
	Driver();
	~Driver();

	void start();

protected:
	int mem_fd;
	void* gpio_map;
	volatile unsigned* gpio;

};

}


#endif
