#include <node.h>
#include "driver.h"

namespace printhead {

	using namespace v8;

	Driver* driver;


	void writeBuffer(const FunctionCallbackInfo<Value>& args) {
<<<<<<< HEAD
		Isolate* isolate = args.GetIsolate();
		
		Local<String> str;

		if (driver) {
			str = String::NewFromUtf8(isolate, "Not null");
			driver->start();
		} else {
			str = String::NewFromUtf8(isolate, "Null");
		}

		args.GetReturnValue().Set(str);
=======
		driver->start();
>>>>>>> 18b3d1a7cc73afe897e5c9f4c00c39f0fae99edd
	}

	void print(const FunctionCallbackInfo<Value>& args) {
		Isolate* isolate = args.GetIsolate();

		Local<String> str = String::NewFromUtf8(isolate, "Print buffer");
		args.GetReturnValue().Set(str);
	}

	void printPeriod(const FunctionCallbackInfo<Value>& args) {
		Isolate* isolate = args.GetIsolate();

		Local<String> str = String::NewFromUtf8(isolate, "Print buffer");
		args.GetReturnValue().Set(str);
	}

	void init(Local<Object> exports) {
		driver = new Driver();

		NODE_SET_METHOD(exports, "writeBuffer", writeBuffer);
		NODE_SET_METHOD(exports, "print", print);
		NODE_SET_METHOD(exports, "printPeriod", printPeriod);
	}

	NODE_MODULE(driver, init);
}

