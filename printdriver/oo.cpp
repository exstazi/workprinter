#include <sys/mman.h>

#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <time.h>
#include <sys/time.h>

int mem_fd;
void* gpio_map;
volatile unsigned* gpio;

#define BCM2708_PERI_BASE        0x3F000000
#define GPIO_BASE                (BCM2708_PERI_BASE + 0x200000)

#define PAGE_SIZE (4*1024)
#define BLOCK_SIZE (4*1024)

#define INP_GPIO(g) *(gpio+((g)/10)) &= ~(7<<(((g)%10)*3))
#define OUT_GPIO(g) *(gpio+((g)/10)) |=  (1<<(((g)%10)*3))
#define SET_GPIO_ALT(g,a) *(gpio+(((g)/10))) |= (((a)<=3?(a)+4:(a)==4?3:2)<<(((g)%10)*3))
 
#define GPIO_SET *(gpio+7)  // sets   bits which are 1 ignores bits which are 0
#define GPIO_CLR *(gpio+10) // clears bits which are 1 ignores bits which are 0
 
#define GET_GPIO(g) (*(gpio+13)&(1<<g)) // 0 if LOW, (1<<g) if HIGH
#define GET_VALUE_GPIO( g ) ( ((*(gpio+13)) >> g ) & 0x01 )

#define GET_GPIO_ALL (*(gpio+13))
 
#define GPIO_PULL *(gpio+37) // Pull up/pull down
#define GPIO_PULLCLK0 *(gpio+38) // Pull up/pull down clock

#define CLK 6
#define DATA 13
#define DATA2 19
#define LATCH 5
#define OE 11

#define HVOK 16
#define HTTERM 26

#define INPUT1 17
#define INPUT2 27

#define I2C_SDA 21
#define I2C_SCL 20

#define HEATER 22

#define LED_DRIVER_START 10
#define LED_CLIENT_CONNECT 9
#define LED_STATUS_PRINT 4
#define LED_ERROR 3



int main() {
	if ((mem_fd = open("/dev/mem", O_RDWR|O_SYNC) ) < 0) {
        //std::cout << "Can't open /dev/mem" << std::endl;
        return -1;
    }

    gpio_map = mmap(
        NULL,             //Any adddress in our space will do
        BLOCK_SIZE,       //Map length
        PROT_READ|PROT_WRITE,// Enable reading & writting to mapped memory
        MAP_SHARED,       //Shared with other processes
        mem_fd,           //File to map
        GPIO_BASE         //Offset to GPIO peripheral
    );

    close(mem_fd);

    if (gpio_map == MAP_FAILED) {
        //std::cout << "nmap error " << (int)gpio_map << std::endl;
        return -1;
    }

    gpio = (volatile unsigned *)gpio_map;


    INP_GPIO( CLK );
	OUT_GPIO( CLK );

	for (int i = 0; i < 10000000; ++i) {
		GPIO_CLR = 1 << CLK;
		GPIO_SET = 1 << CLK;
	}

	return 0;
}