var twincat = (function($) {

	var statePLC = false;
	var stateMain = false;
	var stateNCI = false;

	var selectedMachine = undefined;

	function init() {
		initCallback();
		updateElements();
	}

	function updateElements() {
	}

	function initCallback() {
		$('body').on('click', '#plc-start', plcStart);
		$('body').on('click', '#active-axis', activateAxis);
	}

	function plcStart() {
		if (statePLC) {
			console.log("PLC alredy start!!");
		} else {
			selectedMachine = $('#vm').prop('checked') ? 'Virtual' : 'Real';
			console.log("Selected machine: ", selectedMachine);
		}
	}

	function activateAxis() {
		console.log("Activeate axis");

	}

	return {
		init: init
	}


})(jQuery);

jQuery(document).ready(twincat.init);