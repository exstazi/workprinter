module.exports = {

	ProfileCameraParams: {

		CameraMode: {
			description: "Reads or sets the operational mode of the camera. The camera mode setting will affect the type of image acquired. Intensity and IntensityWithGraphics images are intended for diagnostic purposes only. Range is for calibrated height profile images. Default mode is IntensityWithGraphics.",
			translate: "",
			remark: "Determines the acquisition mode of the camera.",
			type: 'CogAcqCameraModeConstants',
			values: {
				Range: {
					value: 2,
					description: 'Camera operates as a linescan camera and outputs range data in real world units. Output format is 16-bit greyscale.'
				},
				IntensityWithGraphics: {
					value: 1,
					description: 'Camera outputs intensity image from sensor with added graphics showing the results of the laser peak detection algorithm. Output format is RGB planar.'
				},
				Intensity: {
					value: 0,
					description: 'Camera outputs intensity image from sensor. Ouput format is 8-bit greyscale.'
				}
			}
		},

		DetectionSensitivity: {
			description: 'Reads or sets the sensitivity of the laser detection. A higher value makes the laser easier to find and allows a lower exposure setting, which in turn allows a higher line rate. A lower value will prevent mistaking noise for the laser in areas where the laser is obscured.',
			type: 'Integer',
			remark: 'Used to detect where the laser is.',
		},

		DetectionSensitivityMax: {
			description: 'Defines the maximum threshold used for detecting the laser. Maximum value will vary with camera model and firmware version.',
			type: 'Integer',
			remark: 'The maximum value can be applied to DetectionSensitivity.',
			defined: 1000
		},

		HighDynamicRange: {
			description: 'Read or sets high dynamic range operation. When enabled, the camera uses multiple exposures to increase the dynamic range. This is useful when the laser line passes over both dark and highly reflective regions. Enabling high dynamic range will reduce the maximum line rate. Default value is False.',
			type: 'Boolean',
			remark: 'Controls the maximum line rate by acquiring images with multiple expsures.',
		},

		LaserMode: {
			description: 'Reads or sets the mode of the laser. Default is Strobed. Laser is on when an image is acquired. "On" and "Off" are intended for setup and diagnostic purposes only.',
			type: 'CogAcqLaserModeConstants',
			remark: 'Determines the state of laser.',
			values: {
				Strobed: {
					value: 2,
					description: 'Laser only on during image exposure.'
				},
				Off: {
					value: 0,
					description: 'Laser always off.'
				},
				On: {
					value: 1,
					description: 'Laser always on.'
				}
			}
		},

		ZDetectionBase: {
			description: 'Reads or sets the area of the sensor used for laser detection. ZDetectionBase is the distance, in mm, relative camera defined origin of the Z axis, where the detection range begins. When converting ZDetectionBase to discrete sensor rows, the range may be expanded slightly to match the sensor capabilities. The range will only be reduced if it extends off the end of the sensor. This function can be used to ignore reflections and other artifacts that are outside the expected range of heights.',
			type: 'Double',
			remark: 'Applies to the area of the sensor used for laser detection.',
		},

		ZDetectionHeight: {
			description: 'Reads or sets the area of the sensor used for laser detection. ZDetectionHeight is the height, in mm, of the detection range stating at ZDetectionBase. A smaller ZDetectionHeight increases the maximum line rate when acquiring range images, but reduces the measurement range. When converting ZDetectionHeight to discrete sensor rows, the range may be expanded slightly to match the sensor capabilities. The range will only be reduced if it extends off the end of the sensor. This function can be used to ignore reflections and other artifacts that are outside the expected range of heights.',
			type: 'Double',
			remark: 'Applies to the area of the sensor used for laser detection.',
		},

		ZDetectionHeightMax: {
			description: 'Defines the maximum range of heights (in mm) that can be detected.',
			type: 'Double',
			remark: 'The maximum value applies to the sum of ZDetectionBase and ZDetectionHeight.',
			defined: 500
		},

		ZDetectionSampling: {
			description: 'Reads or sets the number of rows used for laser finding to be reduced. 1 processes every row, 2 processes every other row, 3 processes every third row, and so forth. Increasing ZDetectionSampling increases the maximum line rate when acquiring range images, but reduces the measurement precision. The increase in maximum line rate is in addition to the increase associated with adjusting ZDetectionBase and ZDetectionHeight. Default value is 1.',
			type: 'Integer',
			remark: 'May improve maximum line rate by reducing the number of rows used for laser finding.'
		},

		ZDetectionSamplingMax: {
			description: 'Defines the maximum number of rows can be reduced.',
			type: 'Integer',
			remark: 'The maximum value can be applied to ZDetectionSampling.',
			defined: 8
		}
	},

	LineScanParams: {

		AcquireDirectionPositive: {
			description: 'Reads or sets whether the Acquire direction is positive.',
			type: 'Boolean',
			remark: "Set/get the current acquire direction. If true, the encoder needs to be travelling in the positive direction (i.e. increasing encoder values) for acquisition to begin. If false, the encoder needs to be travelling in the negative direction (i.e. decreasing values) for acquisition to begin. Acqusition can be started either by the hardware means determined by TriggerFromEncoder or a software command (with manual trigger model)." +
						"For platforms which support encoder-triggered acquisitions, the specified acquire direction is only enforced when" +
						"- TriggerFromEncoder is set to true, " +
						"- there is a valid setting for EncoderOffset, and," +
						"- the trigger model is not Manual." +
					"Unlike TestEncoderDirectionPositive, this method does not dictate whether the encoder count increments or decrements. The encoder's direction of travel determines that." +
					"Default Value: The default value of direction is true.",
		},

		CurrentEncoderCount: {
			description: 'Current value of the encoder.',
			type: 'Integer',
			remark: 'The current value of the encoder. The Cognex MVS-8600 does not provide an absolute encoder position counter. On that platform, this property returns the product of the number of the line being acquired and the steps per line encoder setting. This value may not be accurate if the encoder travels backwards during acquisition. If you are using the test encoder the steps per line value is ignored, so this property returns the currently acquired line number. If there is no acquisition in progress, the value of this property is zero. '
		},

		DistancePerCycle: {
			description: 'Specifies the distance moved for each cycle of the encoder signal.',
			type: 'Double',
			remark: 'This currently only applies to the DS1000 series.'
		},

		DistancePerCycleMax: {
			description: 'Maximum DistancePerCycle value allowed.',
			type: 'Double',
			remark: 'The maximum value can be applied to DistancePerCycle.',
			defined: 100
		},

		DistancePerCycleMin: {
			description: 'Minimum DistancePerCycle value allowed.',
			type: 'Double',
			remark: 'The minimum value can be applied to DistancePerCycle',
			defined: 1E-06,
		},

		EncoderOffset: {
			description: 'Used to calculate the encoder count at which line zero of the region of interest occurs.',
			type: 'Integer'
		},

		EncoderPort: {
			description: 'Port for Encoder.',
			type: 'Integer'
		},

		EncoderResolution: {
			description: 'Controls the number of edges required for each encoder count.',
			type: 'CogEncoderResolutionConstants',
			values: {
				cogEncoderResolution4x: {
					value: 2,
					description: 'One count on every encoder edge (recommended, behavior of 8120/CVM11).'
				},
				cogEncoderResolution2x: {
					value: 1,
					description: 'One count every 2 edges.'
				},
				cogEncoderResolution1x: {
					value: 0,
					description: 'One count every 4 edges (default for 8600).'
				}
			}
		},

		IgnoreBackwardsMotionBetweenAcquires: {
			description: 'When true, any backwards motion after an acquisition will not delay the start of the next acquisition.',
			type: 'Boolean',
			remark: 'This property only applies when an acquisition is not in progress (manual or semi-automatic trigger mode before StartAcquire() has been called). Once StartAcquire() has been called, all backwards motion is accounted for regardless of the value of this property. When this property is false, acquisition always begins at the encoder position where the previous acquired image completed; you cannot back up and re-acquire an image of the previous part. When this property is true, acquisition begins at the current encoder position when StartAcquire()()()() is called. This allows you to re-acquire an image of a part by first reversing the encoder, then calling StartAcquire()()()(). Changes to this property take effect after the next acquisition. This means if you want to be able to back up and re-acquire an image of a part, you must set the property to true before the first image is acquired.'
		},

		IgnoreTooFastEncoder: {
			description: 'Read or set whether isTooFastEncoder acqfailure can be generated due to a line overrun situation. Default is true (line overruns will not be reported).',
			type: 'Boolean',
		},

		NumEncoderPorts: {
			description: 'Number of encoder ports available.',
			type: 'Integer',
			remark: 'Gets the number of encoder ports available.'
		},

		ProfileCameraAcquireDirection: {
			description: 'The scene motion for the next image acquisition. If you are using a quadrature encoder, only motion in the specified direction will cause image data to be acquired. If you are using the test encoder or a single-channel encoder, then any motion will cause image data to be acquired, and all motion will be treated as if it were in the specified direction.',
			type: 'CogProfileCameraDirectionConstants',
			values: {
				LaserToLens: {
					value: 1,
					description: 'Scene moves in the laser-to-lens direction.'
				},
				LensToLaser: {
					value: 0,
					description: 'Scene moves in the lens-to-laser direction.'
				}
			}
		},

		ProfileCameraPositiveEncoderDirection: {
			description: 'The scene motion that causes the encoder to generate positive-direction pulses. Set this value to LensToLaser if motion in that direction generates positive encoder pulses; otherwise, set this value to LaserToLens.',
			type: 'CogProfileCameraDirectionConstants',
			values: {
				LaserToLens: {
					value: 1,
					description: 'Scene moves in the laser-to-lens direction.'
				},
				LensToLaser: {
					value: 0,
					description: 'Scene moves in the lens-to-laser direction.'
				}
			}
		},

		ResetCounterOnHardwareTrigger: {
			description: 'Reads or sets whether encoder counter is reset on a hardware trigger.',
			type: 'Boolean',
			remark: 'Reads or sets whether the encoder counter is reset when the FIFO gets a trigger signal from the frame grabber. Default Value: True',
		},

		StartAcqOnEncoderCount: {
			description: 'Sets the number of encoder counts to wait before acquisition starts.',
			type: 'Integer'
		},

		TestEncoderDirectionPositive: {
			description: 'Reads or sets whether the test encoder direction is positive.',
			type: 'Boolean',
			remark: 'Sets or gets whether the test encoder values increase (True) or decrease (False) during image acquisition. Default Value: True',
		},

		TestEncoderEnabled: {
			description: 'Reads or sets whether the test encoder is used instead of the external encoder interface.',
			type: 'Boolean',
			remark: "Sets or gets whether the test encoder on the CVM is used instead of the external encoder interface. TestEncoderDirectionPositive lets you specify whether the test encoder's counts increase or decrease. Use SetStepsPerLine(Int32, Int32) to set the steps per line for the test encoder. There is no test encoder availble on Cognex MVS-8600 frame grabbers. On those frame grabbers,when the test encoder is enabled, the hardware runs freely and acquires lines as fast as possible without using an encoder input, and the SetStepsPerLine(Int32, Int32) setting is ignored. You can use a positive TriggerDelay to create a delay between the acquisition of each line.",			
		},

		TriggerFromEncoder: {
			description: 'Read or set whether triggering is from an encoder or from an external trigger.',
			type: 'Boolean',
			remark: 'Sets or gets whether triggering is from an encoder or from an external trigger. Encoder triggering is only valid for Auto and Semi. When the trigger model is Manual, you can set this property, but it is ignored. ResetCounterOnHardwareTrigger should be True when this property is True. Default Value: True'
		},

		UseSingleChannel: {
			description: 'Reads or sets whether only one channel of the encoder signal is used for acquisition.',
			type: 'Boolean',
			remark: "If UseSingleChannel is set to true, it might be possible to connect only a single encoder channel as encoder input instead of using a quadrature dual channel type encoder. The channel that is used depends upon the setting of AcquireDirectionPositive. If AcquireDirectionPositive is true, channel A is used. If false, channel B is used. One advantage of using a single channel encoder input is in some cases it might be desirable to acquire without considering the direction of motion of the encoder.  This property applies only to the MVS-8600/8600e platform. Only the MVS-8600/8600e platform supports this function. Default Value: The default value is false indicating both encoder channels A and B are considered for acquisition on platforms supporting this functionality."
		}
	},

	RangeImageParams: {
		AutoCorrectPixelRowOrder: {
			description: 'Whether or not to automatically set the row-order of the acquired image to match the actual appearance of the scene. If this value is set to false, then images acquired in the LaserToLens direction will have an inverted pixel row order (the image will appear mirrored top-to-bottom with respect to the actual appearance of the scene).',
			type: 'Boolean',
			remark: 'This currently only applies to the profile camera. Default Value: True'
		},

		FirstPixelLocation: {
			description: 'Reads or sets the offset of the output image relative to the center, in mm. Negative offsets are allowed. The default value corresponds to the leftmost location that can be seen at the furthest distance from the camera. Default value is negative, which indicates the left side of the center line.',
			type: 'Double',
			remark: 'Offset of the output image relative to the center, in mm.'
		},

		FirstPixelLocationRange: {
			description: 'Defines the range of offset of the output image relative to the center, in mm. The offset must reside within the range. -FirstPixelLocationRange <= FirstPixelLocation <= FirstPixelLocationRange.',
			type: 'Double',
			remark: 'The range of offset of the output image relative to the center, in mm.',
			defined: ''
		},

		XScale: {
			description: 'Reads or sets the dimensions of image pixels in mm in the X direction. Defaults to the scaling of sensor pixels at the closest. Used when generating range images. Defaults to the scaling of sensor pixels at the closest distance from the camera that can be measured. This insures no information is lost in range images for objects close to the camera, but it increases the width of the range image.',
			type: 'Double',
			remark: 'The dimensions of image pixels in mm in the X direction.'
		},

		XScaleMax: {
			description: 'Maximum X scale value allowed.',
			type: 'Double',
			defined: 0,
			remark: 'The maximum value can be applied to XScale.'
		},

		XScaleMin: {
			description: 'Minimum X scale value allowed.',
			type: 'Double',
			defined: 0,
			remark: 'The minimum value can be applied to XScale.'
		},

		YScale: {
			description: 'Reads the dimensions of image pixels in mm in the Y direction. Unlike XScale and ZScale, the YScale setting is not used by the acq system. It is only used to set the Y component of the transform of the acquired image. The default is 1.0.',
			type: 'Double',
			remark: 'The dimensions of image pixels in mm in the Y direction.'
		},

		ZOffset: {
			description: 'Reads or sets the distance, in mm, relative to the camera defined origin of the Z axis, which corresponds to a pixel value of 0. Used when generating range images. The default is 0.0.',
			type: 'Double',
			remark: 'Defines a distance, in mm, relative camera defined origin of the Z axis, which corresponds to a pixel value of 0.'
		},

		ZOffsetMax: {
			description: 'Maximum ZOffset value allowed.',
			type: 'Double',
			defined: 0
		},

		ZOffsetMin: {
			description: 'Minimum ZOffset value allowed.',
			type: 'Double',
			defined: 0
		},

		ZScale: {
			description: 'Reads or sets the scaling in mm of pixel values. Used when generating range images with a camera. When ZScale is positive, pixel values increase towards the camera. When ZScale is negative, pixel values increase away from the camera. Values near 0.0 will be rounded to 0.00001 or -0.00001. Default is 0.01 mm/pixel value. The default was picked to be a round number which also provides the full accuracy of the camera.',
			type: 'Double',
			remark: ''
		}

	},

	HardwareImagePoolParams: {
		HardwareImagePool: {
			description: 'Get or set number of images in the pool. A larger value provides more reliable acquisition, but uses more memory. You will only be able to set the value if CustomHardwareImagePool is set true.',
			type: 'Integer'
		},
		RecommendedHardwareImagePool: {
			description: 'Get the recommended number of images in the pool. Note that the recommended value may change when the region of interest changes.',
			type: 'Integer'
		},
		UseCustomHardwareImagePool: {
			description: 'When False (the default), HardwareImagePool will always be the same as RecommendedHardwareImagePool. When True, you can set HardwareImagePool to a specific value. Normally, you would only set CustomHardwareImagePool to True if you are acquiring very large images and the recommended setting requires too much memory.',
			type: 'Boolean'
		}
	}


}