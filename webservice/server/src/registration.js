import Vue from 'vue'
import VueWebsocket from './plugins/websocket'
import list from './components/registration/view.vue'

Vue.use(VueWebsocket, "ws://192.168.215.65:9099")

const app = new Vue({
	el: '#app',
	components: {
		list
	},
})