export default {
	install(Vue, url) {

		var ws = new WebSocket(url);
		var events = {
			open: [],
			close: [],
			subscribe: {},
		};

		ws.onopen = function() {

			for (var key in events.subscribe) {
				ws.send(JSON.stringify({des: key, tag: 'subscribe'}));
			}
			events.open.forEach((key) => {
				key.call(Vue.prototype.$socket);
			});
		}

		ws.onclose = function() {
			events.close.forEach((key) => {
				key.call(Vue);
			});
		}

		ws.onmessage = function(e) {
			var data = JSON.parse(e.data);

			if (data.des) {				
				if (events.subscribe[data.des]) {
					events.subscribe[data.des].call(Vue.prototype.$socket, data.for, data.data);
				}
			} else {
				console.log('Bad request: ' + data);
			}
		}


		Vue.prototype.$socket = {
			

			send(des, act, data) {

				if (ws.readyState == 0) {
					console.warn('Отсутствует соединение с сервером!')
					return ;
				}

				ws.send(JSON.stringify({
					des,
					action: act,
					data
				}));
			}

		};

		let addListeners = function() {
			if (this.$options["socket"]) {
				let conf = this.$options.socket;
				if (conf) {
					Object.keys(conf).forEach((key) => {
						if (events[key]) {
							if (key === 'subscribe') {
								for (var sub in conf[key]) {
									events[key][sub] = conf[key][sub];
								}
							} else {
								events[key].push(conf[key]);
							}
						} else {
							console.warn('Event [' + key + '] not register');
						}
					});
				}
			}
		};

		let removeListeners = function() {
			for (var event in events) {
				event = [];
			}
		};

		Vue.mixin({
			created: addListeners,
			beforeDestroy: removeListeners
		});

	}
}