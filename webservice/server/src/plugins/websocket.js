export default {
	install(Vue, url) {

		var ws = new WebSocket(url);
		var self = undefined;
		var events = {
			open: [],
			close: [],
			data: [],
		};

		ws.onopen = function() {
			events.open.forEach((key) => {
				key.call(Vue.prototype.$socket);
			});
		}

		ws.onclose = function() {
			events.close.forEach((key) => {
				key.call(Vue);
			});
		}

		ws.onmessage = function(e) {
			var data = JSON.parse(e.data);
			events.data.forEach((key) => {
				key.call(self, data)
			})
		}


		Vue.prototype.$socket = {
			

			send(data) {

				if (ws.readyState == 0) {
					console.warn('Отсутствует соединение с сервером!')
					return ;
				}

				ws.send(JSON.stringify(data));
			}

		};

		let addListeners = function() {			
			if (this.$options["socket"]) {
				self = this;
				let conf = this.$options.socket;
				if (conf) {
					Object.keys(conf).forEach((key) => {
						if (events[key]) {
							events[key].push(conf[key]);
						} else {
							console.warn('Event [' + key + '] not register');
						}
					});
				}
			}
		};

		let removeListeners = function() {
			for (var event in events) {
				event = [];
			}
		};

		Vue.mixin({
			created: addListeners,
			beforeDestroy: removeListeners
		});

	}
}