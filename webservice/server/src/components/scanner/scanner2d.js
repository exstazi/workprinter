
function Scanner2d(options) {
	this.options = options;
	this.container = document.getElementById(options.container);
	this.container.width = options.w;
	this.container.height = options.h;
	this.ctx = this.container.getContext('2d');
	if (this.ctx) {
		console.log("Canvas ready!");
		this.init();
		this.drawScan();
	}
}

Scanner2d.prototype.init = function() {
	//this.ctx.fillRect(0, 0, this.options.w, this.options.h);
}

Scanner2d.prototype.drawScan = function() {
	this.ctx.fillRect(0, 0, this.options.w, this.options.h);
	this.ctx.fillStyle = 'red';
	this.ctx.strokeStyle = "red";
	for (var i = 0; i < this.options.w; i++) {
		this.ctx.fillRect(i, Math.random() * 768, 2, 2);
	}


	var ctx = this.ctx;
}

Scanner2d.prototype.drawScanForData = function(data) {
	var ctx = this.ctx;

	var image = new Image();
	image.onload = function() {
		ctx.drawImage(image, 0, 0);
	};
	image.src = "data:image/jpg;base64," + data;
}


export default Scanner2d;