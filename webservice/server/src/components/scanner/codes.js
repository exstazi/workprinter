export default {
	ConnectedStatus: {
		0: 'Не подключен',
		1: 'Подключен'
	},

	State: {
		0: 'Не подключен',
		1: 'Ожидание команды',
		2: 'Acquire',
		3: 'Continue',
		4: 'Range'
	}
}