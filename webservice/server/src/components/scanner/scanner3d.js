
//1000x850x700

function Scanner3d(options)
{
	this.options = options;
	this.container = document.getElementById(options.container);

	this.engine = new BABYLON.Engine(this.container, true);

	this.scene = undefined;
	this.camera = undefined;
	this.light = undefined;

	this.createScene();

	this.engine.runRenderLoop(() => {
		this.scene.render();
	});
}

Scanner3d.prototype.init = function()
{

}


Scanner3d.prototype.createScene = function() 
{
	this.scene = new BABYLON.Scene(this.engine);
	this.camera = new BABYLON.ArcRotateCamera("ArcRotareCamera", 1, 0.8, 10, new BABYLON.Vector3(700, 1000, 700), this.scene) 
	this.camera.setTarget(new BABYLON.Vector3(0, 0, 0));
	this.camera.attachControl(this.container, true);
	this.light = new BABYLON.HemisphericLight("light1", new BABYLON.Vector3(0, 1, 0), this.scene);
	this.light.intensity = 0.7;
    var sphere = BABYLON.Mesh.CreateSphere("sphere1", 16, 2, this.scene);
    sphere.position.y = 1;
    var ground = BABYLON.Mesh.CreateGround("ground1", 1000, 850, 2, this.scene);

    var lines = BABYLON.Mesh.CreateLines("lines1", [
    	new BABYLON.Vector3(500, 0, 425),
    	new BABYLON.Vector3(500, 700, 425),
    	new BABYLON.Vector3(-500, 700, 425),
    	new BABYLON.Vector3(-500, 0, 425),
    	new BABYLON.Vector3(-500, 700, 425),
    	new BABYLON.Vector3(-500, 700, -425),
    	new BABYLON.Vector3(-500, 0, -425)
    	], this.scene);
}

export default Scanner3d;
