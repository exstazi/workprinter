import Vue from 'vue'
import store from './store/store'
import machine from './components/machine.vue'
import socket from './plugins/socket'

Vue.use(socket, 'ws://192.168.214.65:9090')

const app = new Vue({
	el: '#app',
	store,
	components: {
		machine
	},

	socket: {
		open() {
			//store.commit('connectionState', 1);
			this.send('Machine', 'get-state-connect');
			//this.send('Machine', 'get-actual-state');
		},

		close() {
			store.commit('connectionState', 0);
		},

		subscribe: {
			'Machine': function (action, data)  {
				console.log('Machine action [' + action + ']');
				if (action == 'state-connect') {
					store.commit('connectionState', 2);
					this.send('Machine', 'get-actual-state');
				} else if (action == 'get-actual-state') {
					store.commit('initStore', data);
				} else if (action == 'error') {
					store.commit('errorMessages', data);
				} else {
					store.commit('changeState', {variable: action, value: data});
				}
			},

			'Scanner': (action, data) => {
				console.log('Scanner action [' + action + ']');
				if (act == 0x20) {
					canvas2d.drawScanForData(data);
				}
			}
		}
	},

	mounted() {
		
	}
	
});