import Vue from 'vue'
import Vuex from 'vuex'

import machine from './modules/machine'
import scanner from './modules/scanner'

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		modal: {
			active: false,
			title: '...',
			description: '...'
		}
	},

	modules: {
		machine,
		scanner
	},

	mutations: {

	}
});

export default store;
