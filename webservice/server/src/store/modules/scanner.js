export default {

	state: {
		status: 0,
		state: 0,

		cameraMode: 0,
		laserMode: 0,
	},

	mutations: {
		updateLaserMode(state, payload) {
			state.laserMode = payload;
		},
		updateCameraMode(state, payload) {
			state.cameraMode = payload;
		}
	},

	actions: {

	},

	getters: {

	}
}