
export default {

	state: {
		status: 0,
		errStatus: 0,
		errId: 0,
		axisActive: {x1: 0, x2: 0, y: 0, z: 0, h: 0, q: 0},

		plc: 0,
		connect: 0,

		laser: {
			state: 0
		},

		Axis: {
			x1: {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0, errCode: 0},
			x2: {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0, errCode: 0},
			y:  {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0, errCode: 0},
			z:  {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0, errCode: 0},
			h:  {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0, errCode: 0},
			q:  {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0, errCode: 0},
		},

		NCI: {
			status: 0,
			work: 0,
			path: 'undefined',
			errChannel: 0,
			errStatus: 0,
			errId: 0,
			files: [],
			chooseAxis: [],
			programm: ''
		}
	},

	mutations: {
		connectionState(state, status) {
			console.log('connection state = ' + status);
			state.connect = status;
			/*if (status == 0) {
				state.modal.title = 'Отсутствует соединение с сервером';
				state.modal.description = '<b>Отсутствует соединение с сервером</b><br /> Проверьте запущен ли сервер? После обновите эту страницу.'
				state.modal.active = true;
			} else {
				store.dispatch('send', { action: 'get-actual-state' });
				state.modal.active = false;
			}*/
		},

		errorMessages(state, status) {
			/*state.modal.title = 'Ошибка соединения со станком';
			state.modal.description = status;
			state.modal.active = true;*/
			console.error(status);
		},

		changeState(state, payload) {
			if (payload.variable == 'axisActive') {
				state.axisActive.x1 = payload.value.x1;
				state.axisActive.x2 = payload.value.x2;
				state.axisActive.y = payload.value.y;
				state.axisActive.z = payload.value.z;
				state.axisActive.h = payload.value.h;
				state.axisActive.q = payload.value.q;
				return ;
			}

			if (payload.variable == 'NCI.files') {
				state.NCI.files.splice(0, state.NCI.files.length);
				state.NCI.files.push(...payload.value);
				return ;
			}

			if (payload.variable == 'NCI.chooseAxis') {
				state.NCI.chooseAxis.splice(0, state.NCI.chooseAxis.length);
				state.NCI.chooseAxis.push(payload.value[0]);
				state.NCI.chooseAxis.push(payload.value[1]);
				state.NCI.chooseAxis.push(payload.value[2]);
				return ;
			}

			if (payload.variable == 'get-nc-program') {
				state.NCI.programm = payload.value;
				return ;
			}

			if (payload.variable == 'onLaser') {
				state.laser.state = payload.value;
				return;
			}

			var arr = payload.variable.split('.');
			var obj = state;
			var count = arr.length;

			arr.forEach(function(item, i) {
				if (i < count-1) {
					obj = obj[item];
				}
			});

			obj[arr[arr.length-1]] = (payload.value);
		},

		initStore(state, data) {
			state.status = data.status;
			state.errStatus = data.errStatus;
			state.errId = data.errId;
			state.plc = data.plcState;
			state.laser.state = data.onLaser;


			state.NCI.status = data.NCI.status;
			state.NCI.work = data.NCI.work;
			state.NCI.path = data.NCI.path;
			state.NCI.errChannel = data.NCI.errChannel;
			state.NCI.errStatus = data.NCI.errStatus;
			state.NCI.errId = data.NCI.errId;
			state.NCI.chooseAxis.push(data.NCI.chooseAxis[0]);
			state.NCI.chooseAxis.push(data.NCI.chooseAxis[1]);
			state.NCI.chooseAxis.push(data.NCI.chooseAxis[2]);

			['x1', 'x2', 'y', 'z', 'h', 'q'].forEach(function (i) {
				state.axisActive[i] = data.axisActive[i];

				state.Axis[i].actPos = data.Axis[i].actPos;
				state.Axis[i].actVelo = data.Axis[i].actVelo;
				state.Axis[i].errCode = data.Axis[i].errCode;
				state.Axis[i].ncErrId = data.Axis[i].ncErrId;
				state.Axis[i].ncErrStatus = data.Axis[i].ncErrStatus;
				state.Axis[i].ncStatus = data.Axis[i].ncStatus;
				state.Axis[i].setAcc = data.Axis[i].setAcc;
				state.Axis[i].setPos = data.Axis[i].setPos;
				state.Axis[i].setVelo = data.Axis[i].setVelo;
				state.Axis[i].state = data.Axis[i].state;

				//state.NC.point1[i] = data.NC.point1[i];
				//state.NC.point2[i] = data.NC.point2[i];
				//state.NC.velocity[i] = data.NC.velocity[i];
				//state.NC.jogDistance[i] = data.NC.jogDistance[i];
			});
		}
	},

	actions: {

	},

	getters: {

	}	
}
