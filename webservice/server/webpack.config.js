

module.exports = {
	entry: {
		machine: './src/machine.js',
		registration: './src/registration.js'
	},

	output: {
		path: __dirname + '/assets/',
		filename: '[name].bundle.js',
		library: 'app'
	},

	watch: true,

	module: {
		rules: [
		{
			test: /\.vue$/,
			exclude: [/node_modules/],
			loader: 'vue-loader'
		},
		{
			test: /\.js$/,
			exclude: [/node_modules/],
			use: [{
				loader: 'babel-loader',
				options: { presets: ['es2015'] }
			}]
		},
		]
	},

	resolve: {
		alias: {
			'vue$': 'vue/dist/vue.esm.js' // 'vue/dist/vue.common.js' для webpack 1
		}
	}
}
