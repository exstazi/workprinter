$(function() {
	var w = $('#canvas').width();
	var h = $('#canvas').height();
	var canvas = document.getElementById('canvas');

	canvas.setAttribute('width', w);
	canvas.setAttribute('height', h);

	var renderer = new THREE.WebGLRenderer({canvas});
	renderer.setClearColor(0xffffff);

	var scene = new THREE.Scene();
	var camera = new THREE.PerspectiveCamera(45, w / h, 0.1, 5000);
	var light = new THREE.AmbientLight(0xffffff);
	camera.position.set(0, 0, 1000);
	scene.add(light);

	var g = new THREE.SphereGeometry(200, 12, 12);
	var m = new THREE.MeshBasicMaterial({color: 0x00ff00, wireframe: true});

	var mesh = new THREE.Mesh(g, m);
	scene.add(mesh);

	renderer.render(scene, camera);
});

