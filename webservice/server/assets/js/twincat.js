$(function() {

	const store = new Vuex.Store({
		state: {

			status: 0,
			errStatus: 0,
			errId: 0,
			axisActive: {x1: 0, x2: 0, y: 0, z: 0, h: 0, q: 0},

			Axis: {
				x1: {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0},
				x2: {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0},
				y:  {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0},
				z:  {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0},
				h:  {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0},
				q:  {state: 0, errCode: 0, actPos: 0, actVelo: 0, setPos: 0, setVelo: 0, setAcc: 0, ncStatus: 0, ncErrStatus: 0, ncErrId: 0},
			},

			NC: {
				point1: {x1: 0, x2: 0, y: 0, z: 0, h: 0, q: 0},
				point2: {x1: 0, x2: 0, y: 0, z: 0, h: 0, q: 0},
				jogDistance: {x1: 0, x2: 0, y: 0, z: 0, h: 0, q: 0},
				velocity: {x1: 0, x2: 0, y: 0, z: 0, h: 0, q: 0},
			},

			NCI: {
				state: 0,
				err: 0,
				path: '',
				axis: [],
			},


			main: {
				state: 0,
				error: 0,				
			},

			plc: 0,

			connect: 0,

			nci: {
				state: 0,
				error: 0,
				path: '',
				axis: [],
			}
		},
		mutations: {
			updateValue(state, val) {


			},


			changeMainState(state, n) {
				state.main.state = n;
			},

			changeMainError(state, err) {
				state.main.error = err;
			},

			changeNCIState(state, n) {
				state.nci.state = n;
			},

			selectedAxis(state, n) {
				state.nci.axis = [Axis[n[0]], Axis[n[1]], Axis[n[2]]];
			},

			selectedPath(state, path) {
				var arr = path.split('\\');
				state.nci.path = arr[arr.length-1];
			},

			changePLCState(state, n) {
				state.plc = n;
			},

			changeConnectState(state, n) {
				state.connect = n;
			}
		}

	});
	

	var Machine = {

		axisActive: {x1: false, x2: false, y: false, z: false, h: false, q: false},
		
		state: {
			plc: 'undefined',
			main: 'undefined',
			nci: 'undefined'
		},

		Axis: {
			x1: {
				state: 1,
				errorCode: 12,
				actPos: 0.00,
				actVelo: 0.00,
				setPos: 0.00,
				setVelo: 0.00,
				setAcc: 0.00,
				ncStatus: 0,
				ncErrStatus: 0,
				ncErrId: 0
			},	

			x2: {
				state: 1,
				errorCode: 12,
				actPos: 0.00,
				actVelo: 0.00,
				setPos: 0.00,
				setVelo: 0.00,
				setAcc: 0.00,
				ncStatus: 0,
				ncErrStatus: 0,
				ncErrId: 0
			},

			y: {
				state: 1,
				errorCode: 12,
				actPos: 0.00,
				actVelo: 0.00,
				setPos: 0.00,
				setVelo: 0.00,
				setAcc: 0.00,
				ncStatus: 0,
				ncErrStatus: 0,
				ncErrId: 0
			},

			z: {
				state: 1,
				errorCode: 12,
				actPos: 0.00,
				actVelo: 0.00,
				setPos: 0.00,
				setVelo: 0.00,
				setAcc: 0.00,
				ncStatus: 0,
				ncErrStatus: 0,
				ncErrId: 0
			},

			h: {
				state: 1,
				errorCode: 12,
				actPos: 0.00,
				actVelo: 0.00,
				setPos: 0.00,
				setVelo: 0.00,
				setAcc: 0.00,
				ncStatus: 0,
				ncErrStatus: 0,
				ncErrId: 0
			},

			q: {
				state: 1,
				errorCode: 12,
				actPos: 0.00,
				actVelo: 0.00,
				setPos: 0.00,
				setVelo: 0.00,
				setAcc: 0.00,
				ncStatus: 0,
				ncErrStatus: 0,
				ncErrId: 0
			}
		},

		NC: {
		},

		NCI: {
			chooseAxis: {}
		}
	};


	var viewNCAxis = new Vue({
		el: '#control-nc',
		data: {
			titleAxis: 'X',
			axis: Machine.Axis.x2,

			point1: 100,
			point2: 200,
			velocity: 150,
			jogDistance: 10,
			acceleration: 100,
			deceleration: 120,
		},
		methods: {
			changeAxis: function(axis) {
				this.titleAxis = axis.toUpperCase()[0];
				this.axis = Machine.Axis[axis];
			},

			toPoint1: function() {
				socket.send({
					des: 'Machine', action: 'to-point1',
					data: {
						axis: this.titleAxis.toLowerCase(),
						point: this.point1,
						velo: this.velocity
					}
				});
			},

			toPoint2: function() {
				socket.send({
					des: 'Machine', action: 'to-point2',
					data: {
						axis: this.titleAxis.toLowerCase(),
						point: this.point2,
						velo: this.velocity
					}
				});
			},

			betweenPoint: function() {
				socket.send({
					des: 'Machine', action: 'between-point',
					data: {
						axis: this.titleAxis.toLowerCase(),
						point1: this.point1,
						point2: this.point2,
						velo: this.velocity
					}
				});
			},

			oneJogForward: function(n) {
				socket.send({
					des: 'Machine', action: 'one-jog-forward',
					data: {
						axis: this.titleAxis.toLowerCase(),
						jogDistance: this.jogDistance,
						velo: this.velocity,
					}
				});
			},

			oneJogBackward: function(n) {
				socket.send({
					des: 'Machine', action: 'one-jog-backward',
					data: {
						axis: this.titleAxis.toLowerCase(),
						jogDistance: this.jogDistance,
						velo: this.velocity,
					}
				});
			},

			stopAxis: function() {
				socket.send({
					des: 'Machine', action: 'stop-axis',
					data: this.titleAxis.toLowerCase()
				})
			},

			resetAxis: function() {
				socket.send({
					des: 'Machine', action: 'reset-axis',
					data: this.titleAxis.toLowerCase()
				})
			}
		},
		computed: {
			setTextNCStatus: function() {
				return NCStatus[ this.axis.ncStatus ];
			}
		}
	});

	var stateAxis = new Vue({
		el: '#aside-container',
		store,
		data: {
			machinePicked: 'vm',
			axisActive: Machine.axisActive,
			selectedAxis: Machine.NCI.chooseAxis,

			states: Machine.state,

			listAxis: ['X', 'Y', 'Z', 'H', 'Q'],
			listSelectedAxis: {x: 'X', y: 'Y', z: 'Z'},

			x1: Machine.Axis.x1,
			x2: Machine.Axis.x2,
			y: Machine.Axis.y,
			z: Machine.Axis.z,
			h: Machine.Axis.h,
			q: Machine.Axis.q,
		},
		methods: {

			connectTo: function() {
				socket.connect("ws://localhost:9090");
				
			},

			plcStart: function() {
				if (store.state.plc != 5) {
					socket.send({
						des: 'Machine',
						action: 'plc-start'
					});
				} else {
					socket.send({
						des: 'Machine',
						action: 'plc-stop'
					});
				}
			},

			plcReset: function() {
				socket.send({
					des: 'Machine',
					action: 'plc-reset'
				});
			},

			setAxisActive: function() {
				socket.send({
					des: 'Machine',
					action: 'active-axis',
					data: this.axisActive	
				});
			},

			makeChannel: function() {
				socket.send({
					des: 'Machine',
					action: 'make-channel',
					data: this.listSelectedAxis
				});				
			},
		},

		computed: {

			system: function() {
				return {
					mainState: MainStatus[store.state.main.state],			
					nciState: NCIStatus[store.state.nci.state],
					nci: store.state.nci,
					plc: ADSState[store.state.plc],
					plcState: store.state.plc,
					connect: ConnecteStatus[store.state.connect],
				}
			},

			state: function() {
				return {
					x1: AxisStatus[this.x1.state],
					x2: AxisStatus[this.x2.state],
					y:  AxisStatus[this.y.state],
					z: AxisStatus[this.z.state],
					h: AxisStatus[this.h.state],
					q:  AxisStatus[this.q.state]
				}
			}
		},

		watch: {
			'listSelectedAxis.x': function(val, oldVal) {
				if (this.listSelectedAxis.y == val) {
					this.listSelectedAxis.y = oldVal;
				} else if (this.listSelectedAxis.z == val) {
					this.listSelectedAxis.z = oldVal;
				}
			},

			'listSelectedAxis.y': function(val, oldVal) {
				if (this.listSelectedAxis.x == val) {
					this.listSelectedAxis.x = oldVal;
				} else if (this.listSelectedAxis.z == val) {
					this.listSelectedAxis.z = oldVal;
				}
			},

			'listSelectedAxis.z': function(val, oldVal) {
				if (this.listSelectedAxis.x == val) {
					this.listSelectedAxis.x = oldVal;
				} else if (this.listSelectedAxis.y == val) {
					this.listSelectedAxis.y = oldVal;
				}
			}
		}
	});

	var viewNCI = new Vue({
		el: '#nci',
		data: {
			files: [],
			selected: -1
		},
		methods: {
			changeFile: function(n) {
				this.selected = n;
				socket.send({
					des: 'NCI',
					action: 'get-nc-program',
					data: this.files[this.selected]
				})
			},

			start: function() {
				socket.send({
					des: 'Machine',
					action: 'nci-start',
					data: this.files[this.selected]
				});
			},

			oneLine: function() {
				socket.send({
					des: 'Machine',
					action: 'nci-one-line',
					data: this.files[this.selected]
				});
			},

			nextLine: function() {
				socket.send({
					des: 'Machine',
					action: 'nci-next-line'
				});
			},

			pause: function() {
				socket.send({
					des: 'Machine',
					action: 'nci-pause'
				});
			},

			stop: function() {
				socket.send({
					des: 'Machine',
					action: 'nci-stop'
				})
			},

			confirmationError: function() {
				socket.send({
					des: 'Machine',
					action: 'confirmation-error'
				})
			},

			getListNCProgram: function() {
				socket.send({
					des: 'NCI',
					action: 'get-list-nc-program'
				});
			},

			save: function() {
				socket.send({
					des: 'NCI',
					action: 'update-nc-program',
					data: {
						file: this.files[this.selected],
						content: $('#nc-program').val()
					}
				});
			},
		}
	});

	$('#emergency-stop').on('click', function(e) {
		socket.send({
			des: 'Machine',
			action: 'emergency-stop'
		});
	});

	$('#reconnect-to-server').on('click', function(e) {
		socket.connect("ws://localhost:9090");
	});

	function writeVal(path, val) {
		if (path.length < 3) {
			console.log("writeVal incorrect path: " + path);
			return ;
		}
		var par = path.split('.');
		var obj = Machine;
		var count = par.length;
		par.forEach(function(item, i) {
			if (i < count-1 )
				obj = obj[item];
		});
		obj[par[par.length-1]] = val;
	}

	function showError(title, data) {
		$('#block-monitor').addClass('in');
		$('#block-modal').addClass('in');
		$('#block-modal').css('display', 'block');
		$('#block-monitor').css('display', 'block');

		$('#block-title').html(title);
		$('#block-description').html(data);
	}

	function hideError() {
		$('#block-monitor').removeClass('in');
		$('#block-modal').removeClass('in');
		$('#block-modal').css('display', 'none');
		$('#block-monitor').css('display', 'none');
	}

	hideError();
	
	var socket = (function() {

		var ws = undefined;

		function connect(host) {
			ws = new WebSocket(host);
			ws.onopen = function() {
				hideError();
				store.commit('changeConnectState', 1);
				send({
					tag: 'subscribe',
					des: 'Machine'
				});
				send({
					des: 'Machine',
					action: 'get-actual-state'
				});
			};
			ws.onclose = function() {
				showError('Отсутствует подключение к серверу', '');
				hideError();
				store.commit('changeConnectState', 0);
			}
			ws.onerror = function() {
				console.log("Error connection");
			}
			ws.onmessage = process;
		}

		function process(e) {
			var data = JSON.parse(e.data);
			
			if (data.error) {
				showError(data.error.title, data.error.description);
				return ;
			} 

			if (data.des == 'NCI') {
				if (data.action == 'get-list-nc-program') {
					viewNCI.files = data.val;
				} else if (data.action == 'get-nc-program') {
					$('#nc-program').val(data.val);
				}
				return ;
			}

			var emitForStore = {
				status: 'changeMainState',
				'errorId': 'changeMainError',
				'NCI.status': 'changeNCIState',
				'NCI.chooseAxis': 'selectedAxis',
				'NCI.path': 'selectedPath',
				'plcState': 'changePLCState',
			};

			if (data.des == 'Machine') {
				if (data.for) {
					for (var key in emitForStore) {
						if (data.for == key) {
							store.commit(emitForStore[key], data.val);
							return ;
						}
					}					
					writeVal(data.for, data.val);					
				} else {

					store.commit('changeMainState', data.val.status);
					store.commit('changeMainError', data.val.errorId);

					store.commit('changeNCIState', data.val.NCI.status);
					store.commit('selectedAxis', data.val.NCI.chooseAxis);
					store.commit('selectedPath', data.val.NCI.path);

					store.commit('changePLCState', data.val.plcState);

					['x1', 'x2', 'y', 'z', 'h', 'q'].forEach(function(item) {
						Machine.Axis[item].actPos = data.val.Axis[item].actPos;
						Machine.Axis[item].actVelo = data.val.Axis[item].actVelo;
						Machine.Axis[item].errorCode = data.val.Axis[item].errorCode;
						Machine.Axis[item].setAcc = data.val.Axis[item].setAcc;
						Machine.Axis[item].setPos = data.val.Axis[item].setPos;
						Machine.Axis[item].setVelo = data.val.Axis[item].setVelo;
						Machine.Axis[item].state = data.val.Axis[item].state;
						Machine.Axis[item].status = data.val.Axis[item].status;

						Machine.axisActive[item] = data.val.axisActive[item];
					})
				}
			}
		}

		function send(e) {
			ws.send(JSON.stringify(e));
		}

		return {
			connect: connect,
			send: send
		}
	})();

	socket.connect("ws://localhost:9090");
});

var MainStatus = {
	0: 'Ожидание',
	1: 'Сброс осей',
	2: 'Активация осей и хоминг',
	3: 'Задание абсолютной позиции',
	4: 'станок в работе',
	10: 'Аварийная остановка',
	11: 'Ожидание окончания NCI при аварийной остановке',
	40: 'Ошибка'
}

var NCIStatus = {
	0: 'Ожидание',
	1: 'Главная программа стартует',
	2: 'Ожидание запуска главной программы',
	3: 'Главная программа в работе',
	4: 'Старт главной программы в режиме OneLine',
	5: 'Работа главного канала в режиме OneLine',
	6: 'Главный канал в паузе',
	7: 'Работа главного канала прервана',
	8: 'Деактивация М-функций',
	9: 'Деактивация Основного канала',
	10: 'Ожидание деактивация основного канала',
	11: 'Запуск программы Вспомогательного канала',
	12: 'Ожидание работы вспомогательного канала',
	13: 'Работа вспомогательного канала',
	14: 'Вспомогательный канал в паузе',
	15: 'Вспомогательный канал прерван',
	16: 'Деактивация вспомогательного канала',
	17: 'Ожидание деактивации вспомогательного канала',
	18: 'Препроцесс после завершения работы вспомогательного канала',
	19: 'Ожидание повторной активации главного канала',
	20: 'Восстановление М-функций',
	21: 'Процесс перед остановкой',
	22: 'Остановка',
	23: 'Остановка М-функций',
	24: 'Программа выполнилась корректно',
	25: 'Ошибка'
}

var AxisStatus = {
	0: 'init',
	1: 'Отключена',
	2: 'Активация',
	3: 'slave',
	4: 'unslave',
	5: 'NC',
	6: 'NCI',
	7: 'Error'
}

var NCStatus = {
	0: 'Ожидание комманды',
	1: 'Движение к точке 1',
	2: 'Движение к точке 2',
	3: 'Одиночный jog',
	4: 'Остановка jog',
	5: 'Движение с заданной скоростью',
	6: 'Остановка',
	7: 'Задание позиции',
	40: 'Ошибка'
}


var Axis = {
	1: 'y',
	2: 'x2',
	3: 'x1',
	4: 'z',
	5: 'h',
	6: 'q'
}

var ADSState = {
	0: 'INVALID',
	1: 'IDLE',
	2: 'RESET',
	3: 'INIT',
	4: 'START',
	5: 'RUN',
	6: 'STOP',
	7: 'SAVECFG',
	8: 'LOADCFG',
	9: 'POWERFAILURE',
	10: 'POWERGOOD',
	11: 'ERROR',
	12: 'SHUTDOWN',
	13: 'SUSPEND',
	14: 'RESUME',
	15: 'CONFIG',
	16: 'RECONFIG'
}

var ConnecteStatus = {
	0: 'Отключено',
	1: 'Virtual',
	2: 'Real'
}
