var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', { title: 'NevaOil - Machine' });
});

router.get('/registration', function(req, res, next) {
  res.render('registration', { title: 'NevaOil - Registration', cognex: require('../views/data/cognex-property') });
});

router.get('/cognex', function(req, res, next) {
  res.render('cognex', { title: 'NevaOil - Description Cognex param', cognex: require('../views/data/cognex-property') });
});


module.exports = router;
