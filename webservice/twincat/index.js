var route = require('./route');
var Machine = require('./machine');
var Scanner = require('./scanner');

/*
	host: "192.168.215.68",
	amsNetIdTarget: "10.1.33.177.1.1",
	amsNetIdSource: "192.168.214.65.1.1"


	host: "192.168.214.190",
	amsNetIdTarget: "192.168.214.190.1.1",
	amsNetIdSource: "192.168.214.65.1.1"
	*/



const machine = new Machine({
	host: "192.168.215.68",
	amsNetIdTarget: "10.1.33.177.1.1",
	amsNetIdSource: "192.168.214.65.1.1",

	user: 'Administrator',
	password: '1',

	connected() {
		console.log('Connected to machine!!!');
		route.send('Machine', 'state-connect', true)
	},

	notification(prop, val) {
		route.send('Machine', prop, val);
	},

	error(e) {
		console.log(e);
		console.log("------------------");
		route.send('Machine', 'error', e);
	},

	ftpConnected() {
		console.log(machine.getListNCProgram());
	},

	ftpError(e) {
		console.log('Ftp error: ' + e);
	}
});

const scanner = new Scanner({
	host: "localhost",
	port: 12321,

	connected: () => console.log('Connected to scanner server'),
	notification: (command, data) => route.send('Scanner', command, data),
	error: (e) => console.log('Scanner error: ' + e)
});


machine.connect();
//scanner.connect();

route.add('Machine', require('./machine/api')(machine), function(data, next) {
	if (machine.isConnected()) {		
		next.call(this);	
	} else if (data.action == 'connect') {
		machine.connect();
	} else {
		route.send('Machine', 'error', 'Ошибка соединения со станком')
	}
});

route.add('Scanner', require('./scanner/api')(scanner), function(data, next) {
	next.call(this);
});
