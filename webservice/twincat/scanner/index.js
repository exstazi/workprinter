var net = require('net')
var prop = require('./property')
var code = require('./codeheader')


function Scanner(options) {
    this.on = {
        connected: options.connected,
        notification: options.notification,
        error: options.error,
        close: options.close
    }

	this.client = undefined;
    this.options = options;
    this.stream = null;
}

Scanner.prototype.connect = function() {
    var self = this;

    if (this.client !== undefined) {
        return ;
    }

	this.client = net.connect(
        this.options.port,
        this.options.host, 
        this.on.connected
    );

    this.client.on('data', function(data) {
        if (self.stream === null) {
            self.stream = data;
        } else {
             self.stream = Buffer.concat([self.stream, data]);
        }
        self.checkResponseStream();
    });

    this.client.on('error', this.on.error);

    this.client.setNoDelay(true);
}

Scanner.prototype.checkResponseStream = function() {
    var self = this;
    if (self.stream !== null) {
        if (self.stream.length > 5) {
            var command = self.stream.readUInt8(0);
            var size = self.stream.readUInt32LE(1);

            //console.log("Command: " + command + "; Size: " + size + "; Total size: " + self.stream.length);

            if (self.stream.length >= size + 5) {
                var data = new Buffer(size);
                self.stream.copy(data, 0, 5, 5 + size);
                if (self.stream.length > 5 + size) {
                    var nextdata = new Buffer(self.stream.length - 5 - size);
                    self.stream.copy(nextdata, 0, 5+size);
                    self.stream = nextdata;
                } else {
                    self.stream = null;
                }
                self.analyzeResponse(command, data);                
            }
        }
    }
}

Scanner.prototype.analyzeResponse = function(command, buffer) {
    switch (command) {
        case code.LIVE_FRAME:
            this.on.notification.call(this, command, buffer.toString('base64'));
        break;


    }
}

Scanner.prototype.send = function(command, data) {

}

Scanner.prototype.disconnect = function() {

}

Scanner.prototype.checkState = function() {
    var buf = new Buffer(5);
    buf.writeUInt8(code.STATUS, 0);
    buf.writeUInt32LE(0, 1);

    this.client.write(buf);
}

Scanner.prototype.acquireImage = function() {
    var buf = new Buffer(5);
    buf.writeUInt8(code.ACQUIRE, 0);
    buf.writeUInt32LE(0, 1);

    this.client.write(buf);
}

Scanner.prototype.continueImage = function() {
    var buf = new Buffer(5);
    buf.writeUInt8(code.CONTINUE, 0);
    buf.writeUInt32LE(0, 1);

    this.client.write(buf);
}

Scanner.prototype.stopImage = function() {
    var buf = new Buffer(5);
    buf.writeUInt8(code.STOP, 0);
    buf.writeUInt32LE(0, 1);

    this.client.write(buf);
}

/////////////////////// Profile Params /////////////////////////

Scanner.prototype.writeParams = function(command, data) {
    var buf = new Buffer(9);
    buf.writeUInt8(command, 0);
    buf.writeUInt32LE(4, 1);
    buf.writeUInt32LE(data, 5);

    this.client.write(buf);
}

Scanner.prototype.setCameraMode = function(val) {
    val = Number(val);
    if (val >= 0 && val <= 2) {        
        this.writeParams(code.CAMERA_MODE, val);
    } else {
        console.log("Wrong value for set camer mode = " + val);
    }
}

Scanner.prototype.setDetectionSensitivity = function(val) {
    val = Number(val);
    if (val > 0) {        
        this.writeParams(code.DETECTION_SENSITIVITY, val);
    }
}

Scanner.prototype.setHighDynamicRange = function(val) {
    this.writeParams(code.HIGH_DYNAMIC_RANGE, Boolean(val));
}

Scanner.prototype.setLaserMode = function(val) {
    val = Number(val);
    if (val >= 0 && val <= 2) {        
        this.writeParams(code.LASER_MODE, val);
    } else {
        console.log("Wrong value for set laser mode = " + val);
    }
}

Scanner.prototype.setZDetectionBase = function(val) {
    val = Number(val);
    if (val >= 0) {
        this.writeParams(code.DETECTION_BASE, val);        
    }
}

Scanner.prototype.setZDetectionHeight = function(val) {
    val = Number(val);
    if (val >= 0) {
        this.writeParams(code.DETECTION_HEIGHT, val);        
    }
}

Scanner.prototype.setZDetectionSampling = function(val) {
    val = Number(val);
    if (val >= 0) {
        this.writeParams(code.DETECTION_SAMPLING, val);        
    }
}



/////////////////////// MAIN Params /////////////////////////

Scanner.prototype.changeExposure = function(val) {
    //check val
    this.writeParams(code.EXPOSURE, val);
}



Scanner.prototype.changeConstrast = function(val) {
    //check val
    this.writeParams(code.CONTRAST, val);
}



Scanner.prototype.changeTriggerMode = function(val) {
    //check val
    this.writeParams(code.TRIGGER_MODE, val);
}

/////////////////////// Encoder Params /////////////////////////

Scanner.prototype.changeDistancePerCycle = function(val) {
    //check val
    this.writeParams(code.DISTANCE_PER_CYCLE, val);
}

Scanner.prototype.changePositiveEncoderDirection = function(val) {
    //check val
    this.writeParams(code.POSITIVE_ENCODER_DIRECTION, val);
}

Scanner.prototype.changeResolution = function(val) {
    //check val
    this.writeParams(code.RESOLUTION, val);
}

/////////////////////// RANGE Params /////////////////////////



Scanner.prototype.changeOriginX = function(val) {
    //check val
    this.writeParams(code.ORIGIN_X, val);
}

Scanner.prototype.changeWidthX = function(val) {
    //check val
    this.writeParams(code.WIDTH_X, val);
}

Scanner.prototype.changeHeightY = function(val) {
    //check val
    this.writeParams(code.HEIGHT_Y, val);
}

Scanner.prototype.changeFirstPixelLocation = function(val) {
    //check val
    this.writeParams(code.FIRST_PIXEL_LOCATION, val);
}

Scanner.prototype.changeXScale = function(val) {
    //check val
    this.writeParams(code.X_SCALE, val);
}

Scanner.prototype.changeZScale = function(val) {
    //check val
    this.writeParams(code.Z_SCALE, val);
}

Scanner.prototype.changeZOffset = function(val) {
    //check val
    this.writeParams(code.Z_OFFSET, val);
}


module.exports = Scanner;