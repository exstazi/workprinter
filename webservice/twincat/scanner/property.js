module.exports = {

	ProfileCameraParams: {

		CameraMode: {
			description: "Reads or sets the operational mode of the camera. The camera mode setting will affect the type of image acquired. Intensity and IntensityWithGraphics images are intended for diagnostic purposes only. Range is for calibrated height profile images. Default mode is IntensityWithGraphics.",
			translate: "",
			remark: "",
			type: 'CogAcqCameraModeConstants',
			values: {
				Range: {
					value: 2,
					description: 'Camera operates as a linescan camera and outputs range data in real world units. Output format is 16-bit greyscale.'
				},
				IntensityWithGraphics: {
					value: 1,
					description: 'Camera outputs intensity image from sensor with added graphics showing the results of the laser peak detection algorithm. Output format is RGB planar.'
				},
				Intensity: {
					value: 0,
					description: 'Camera outputs intensity image from sensor. Ouput format is 8-bit greyscale.'
				}
			}
		},

		DetectionSensitivity: {
			description: 'Reads or sets the sensitivity of the laser detection. A higher value makes the laser easier to find and allows a lower exposure setting, which in turn allows a higher line rate. A lower value will prevent mistaking noise for the laser in areas where the laser is obscured.',
			type: 'Integer'
		},

		DetectionSensitivityMax: {
			description: 'Defines the maximum threshold used for detecting the laser. Maximum value will vary with camera model and firmware version.',
			type: 'Integer'
		},

		HighDynamicRange: {
			description: 'Read or sets high dynamic range operation. When enabled, the camera uses multiple exposures to increase the dynamic range. This is useful when the laser line passes over both dark and highly reflective regions. Enabling high dynamic range will reduce the maximum line rate. Default value is False.',
			type: 'Boolean'
		},

		LaserMode: {
			description: 'Reads or sets the mode of the laser. Default is Strobed. Laser is on when an image is acquired. "On" and "Off" are intended for setup and diagnostic purposes only.',
			type: 'CogAcqLaserModeConstants',
			values: {
				Strobed: {
					value: 2,
					description: 'Laser only on during image exposure.'
				},
				Off: {
					value: 0,
					description: 'Laser always off.'
				},
				On: {
					value: 1,
					description: 'Laser always on.'
				}
			}
		},

		ZDetectionBase: {
			description: 'Reads or sets the area of the sensor used for laser detection. ZDetectionBase is the distance, in mm, relative camera defined origin of the Z axis, where the detection range begins. When converting ZDetectionBase to discrete sensor rows, the range may be expanded slightly to match the sensor capabilities. The range will only be reduced if it extends off the end of the sensor. This function can be used to ignore reflections and other artifacts that are outside the expected range of heights.',
			type: 'Double'
		},

		ZDetectionHeight: {
			description: 'Reads or sets the area of the sensor used for laser detection. ZDetectionHeight is the height, in mm, of the detection range stating at ZDetectionBase. A smaller ZDetectionHeight increases the maximum line rate when acquiring range images, but reduces the measurement range. When converting ZDetectionHeight to discrete sensor rows, the range may be expanded slightly to match the sensor capabilities. The range will only be reduced if it extends off the end of the sensor. This function can be used to ignore reflections and other artifacts that are outside the expected range of heights.',
			type: 'Double',
		},

		ZDetectionHeightMax: {
			description: 'Defines the maximum range of heights (in mm) that can be detected.',
			type: 'Double'
		},

		ZDetectionSampling: {
			description: 'Reads or sets the number of rows used for laser finding to be reduced. 1 processes every row, 2 processes every other row, 3 processes every third row, and so forth. Increasing ZDetectionSampling increases the maximum line rate when acquiring range images, but reduces the measurement precision. The increase in maximum line rate is in addition to the increase associated with adjusting ZDetectionBase and ZDetectionHeight. Default value is 1.',
			types: 'Integer'
		},

		ZDetectionSamplingMax: {
			description: 'Defines the maximum number of rows can be reduced.',
			types: 'Integer'
		}
	},

	LineScanParams: {

		AcquireDirectionPositive: {
			description: 'Reads or sets whether the Acquire direction is positive.',
			type: 'Boolean'
		},

		CurrentEncoderCount: {
			description: 'Current value of the encoder.',
			type: 'Integer'
		},

		DistancePerCycle: {
			description: 'Specifies the distance moved for each cycle of the encoder signal.',
			type: 'Double'
		},

		DistancePerCycleMax: {
			description: 'Maximum DistancePerCycle value allowed.',
			type: 'Double'
		},

		DistancePerCycleMin: {
			description: 'Minimum DistancePerCycle value allowed.',
			type: 'Double'
		},

		EncoderOffset: {
			description: 'Used to calculate the encoder count at which line zero of the region of interest occurs.',
			type: 'Integer'
		},

		EncoderPort: {
			description: 'Port for Encoder.',
			type: 'Integer'
		},

		EncoderResolution: {
			description: 'Controls the number of edges required for each encoder count.',
			type: 'CogEncoderResolutionConstants',
			values: {
				cogEncoderResolution4x: {
					value: 2,
					description: 'One count on every encoder edge (recommended, behavior of 8120/CVM11).'
				},
				cogEncoderResolution2x: {
					value: 1,
					description: 'One count every 2 edges.'
				},
				cogEncoderResolution1x: {
					value: 0,
					description: 'One count every 4 edges (default for 8600).'
				}
			}
		}


	},

	HardwareImagePoolParams: {
		HardwareImagePool: {
			description: 'Get or set number of images in the pool. A larger value provides more reliable acquisition, but uses more memory. You will only be able to set the value if CustomHardwareImagePool is set true.',
			type: 'Integer'
		},
		RecommendedHardwareImagePool: {
			description: 'Get the recommended number of images in the pool. Note that the recommended value may change when the region of interest changes.',
			type: 'Integer'
		},
		UseCustomHardwareImagePool: {
			description: 'When False (the default), HardwareImagePool will always be the same as RecommendedHardwareImagePool. When True, you can set HardwareImagePool to a specific value. Normally, you would only set CustomHardwareImagePool to True if you are acquiring very large images and the recommended setting requires too much memory.',
			type: 'Boolean'
		}
	}


}