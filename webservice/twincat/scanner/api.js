module.exports = function(scanner) {

	return {
		'connect': () => scanner.connect(),

		'acquire': () => scanner.acquireImage(),
		'continue': () => scanner.continueImage(),
		'stop': () => scanner.stopImage(),

		'change-camera-mode': (d) => scanner.setCameraMode(d),
		'change-detection-sensitivity': (d) => scanner.setDetectionSensitivity(d),
		'change-high-dynamic-range': (d) => scanner.setHighDynamicRange(d),
		'change-laser': (d) => scanner.setLaserMode(d),
		'change-detection-base': (d) => scanner.setZDetectionBase(d),
		'change-detection-height': (d) => scanner.setZDetectionHeight(d),
		'change-detection-sampling': (d) => scanner.setZDetectionSampling(d),

		'change-exposure': (d) => scanner.changeExposure(d),
		'change-contrast': (d) => scanner.changeContrast(d),
		'change-trigger-mode': (d) => scanner.changeTriggerMode(d),

		'change-distance-per-cycle': (d) => scanner.changeDistancePerCycle(d),
		'change-positive-encoder-direction': (d) => scanner.changePositiveEncoderDirection(d),
		'change-resolution': (d) => scanner.changeResolution(d),

		'change-origin-x': (d) => scanner.changeOriginX(d),
		'change-width-x': (d) => scanner.changeWidthX(d),
		'change-height-y': (d) => scanner.changeHeightY(d),
		'change-first-pixel-location': (d) => scanner.changeFirstPixelLocation(d),
		'change-x-scale': (d) => scanner.changeXScale(d),
		'change-z-scale': (d) => scanner.changeZScale(d),
		'change-z-offset': (d) => scanner.changeZOffset(d),
	}
}