var WebSocket = require('ws');

var routes = {};
var wss = new WebSocket.Server({port: 9090});

wss.on('connection', function(ws) {

	ws.__subscribe = [];
	ws.__send = function(des, action, data) {
		ws.send(JSON.stringify({
			des,
			'for': action,
			data
		}));
	}

	ws.on('message', function(data) {
		onMessage.call(ws, JSON.parse(data));
	});

});

wss.on('error', function(e) {
	console.log('Error webscoket' + e);
});

var add = function(tag, rules, pre) {
	routes[tag] = {
		actions: rules,
		main: pre,
	}
}

var onMessage = function(data) {

	if (data.tag == 'subscribe' && data.des) {
		this.__subscribe.push(data.des);
	} else {
		if (this.__subscribe.indexOf(data.des) !== -1) {
			var des = data.des;
			var action = data.action;

			if (routes[des] && routes[des].actions[action]) {

				if (routes[des].main) {
					routes[des].main.call(this, data, function() {
						routes[des].actions[action].call(this, data.data);
					});
				} else {
					routes[des].actions[action].call(this, data.data);
				}

			} else {
				console.log('Route not found [' + action + ']');
			}

		} else {
			this.send(JSON.stringify({
				error: 'Access denied',
				data: data
			}));
		}
	}
}

var send = function(des, action, data) {

	wss.clients.forEach(function (ws) {
		if (ws.readyState === WebSocket.OPEN) {
			if (ws.__subscribe.indexOf(des) !== -1) {
				ws.send(JSON.stringify({
					des,
					'for': action,
					data
				}));
			}
		}
	});
}


module.exports = {
	add,
	send,
	route: routes
}