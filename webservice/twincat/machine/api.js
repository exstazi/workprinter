module.exports = function(machine) {

	return {

		'connect': () => machine.connect(),
		'close': function() { machine.close(); this.__send('Machine', 'state-connect', false)},

		'get-state-connect': function() { this.__send('Machine', 'state-connect', machine.isConnected() ? true : false)},

		'plc-start': () => machine.plcStart(),
		'plc-stop': () => machine.plcStop(),
		'plc-reset': () => machine.plcReset(),
		'plc-state': () => machine.plcReadState(),

		'emergency-stop': () => machine.emergencyStop(),
		'get-actual-state': function () { this.__send('Machine', 'get-actual-state', machine.getActualState())},
		'active-axis': (d) => machine.activeAxis(d),

		/////////////////////// NC FUNCTIONS ///////////////////////
		'to-point1': d => machine.toPoint1(d),
		'to-point2': d => machine.toPoint2(d),
		'between-point': d => machine.betweenPoint(d),
		'one-jog-forward': (d) => machine.oneJogForward(d),
		'one-jog-backward': (d) => machine.oneJogBackward(d),
		'stop-axis': d => machine.stopAxis(d),
		'reset-axis': d => machine.resetAxis(d),

		/////////////////////// FTP FUNCTIONS ///////////////////////
		'get-list-nc-program': function() { this.__send('Machine', 'NCI.files', machine.getListNCProgram())},

		'get-nc-program': function(d) {
			this.__send('Machine', 'get-nc-program', machine.getNCProgram(d));
		},

		'update-nc-program': d => machine.updateNCProgram(d.file, d.content),

		/////////////////////// NCI FUNCTIONS ///////////////////////
		'make-channel': function(d) {
			var numeric = {X: 2, Y: 1, Z: 4, H: 5, Q: 6}
			machine.setAxis([
				numeric[d.x],
				numeric[d.y],
				numeric[d.z],
			]);
			console.log(d);

		},

		'nci-start': d => machine.nciStart(d),
		'nci-one-line': d => machine.nciOneLineMode(d),
		'nci-next-line': () => machine.nciNextBlock(),
		'nci-pause': () => machine.nciPause(),
		'nci-stop': () => machine.nciStop(),
		'confirmation-error': () => machine.nciConfirmationError(),

		'laser-start': d => machine.laserStart(d)
	}
}