var fs = require('fs')
var byline = require('byline')

var stream = byline(fs.createReadStream('code.txt', { encoding: 'utf8' }));

var obj = {};

var state = 0;
var code = 0;
var count = 1;

stream.on('data', function(line) {

	if (state == 0) {
		state++;
	} else if (state == 1) {
		code = line;
		obj[code] = {};
		state++;
	} else if (state == 2) {
		obj[code]['type'] = line;
		obj[code]['des'] = '';
		state++;
	} else if (state == 3) {
		if (line.length != 4) {
			obj[code]['des'] += line;
			if (code == 17151) {
				fs.writeFileSync('test.txt', JSON.stringify(obj));
				for (var key in obj) {
					console.log(key);
				}
			}
		} else {
			state = 1;
		}
	}
});
