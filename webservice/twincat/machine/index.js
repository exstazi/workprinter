'use strict';

var events = require('events');
var ads = require('./ads');
var ftp = require('ftp');
var fs = require('fs');

module.exports = Machine;

function Machine(options) {
	
	this.amsOpt = {
		host: options.host,
		amsNetIdTarget: options.amsNetIdTarget,
		amsNetIdSource: options.amsNetIdSource
	}

	this.on = {
		connected: options.connected,
		notification: options.notification,
		error: options.error,

		ftpConnected: options.ftpConnected,
		ftpError: options.ftpError,
	}

	this.ftpOpt = {
		host: options.host,
		user: options.user,
		password: options.password
	}

	this.deviceInfo = null;

	this.options = options;
	this.struct = require('./struct')();
	this.ads = undefined;
	this.ftp = undefined;
	this.ncProgram = [];

	this.plcState = undefined;
}

Machine.prototype.connect = function() {
	var self = this;

	this.connectToFtp();

	this.ads = ads.connect(this.amsOpt, function() {
		this.readDeviceInfo(function(err, res) {
			if (err) {
				self.on.error.call(self, 'Error read device info');
				return ;
			}

			console.log(res);

			self.deviceInfo = res;
			self.on.connected.call(self);

			this.notifyAll([
				self.struct.emStop, 
				self.struct.axisActive, 
				self.struct.status, 
				self.struct.errStatus, 
				self.struct.errId,

				self.struct.NCI.work, 
				self.struct.NCI.mainChannel, 
				self.struct.NCI.status,
				self.struct.NCI.errChannel,
				self.struct.NCI.errId,

				self.struct.NCI.serviceType,
				self.struct.NCI.path,
				self.struct.NCI.serviceProgramPath,
				self.struct.NCI.chooseAxis,
				self.struct.NCI.chooseSubAxis,
				self.struct.onLaser
			]);

			this.readState(function(err, res) {
				self.plcState = res.state;
			});

			['x1', 'x2', 'y', 'z', 'h', 'q'].forEach(function(item) {
				self.ads.notifyAll([
					self.struct.Axis[item].state,
					self.struct.Axis[item].errCode,
					self.struct.Axis[item].actPos,
					self.struct.Axis[item].actVelo,
					self.struct.Axis[item].setPos,
					self.struct.Axis[item].setVelo,
					self.struct.Axis[item].setAcc,
					self.struct.Axis[item].ncStatus,
					self.struct.Axis[item].ncErrStatus,
					self.struct.Axis[item].ncErrId
				]);
			});

			self.readAll(self.struct);
		});
	});

	this.ads.on('notification', function(handle) {
		self.on.notification.call(self, handle.nameHandle, handle.value);
	});

	this.ads.on('error', function(e) {
		self.on.error.call(self, e);
		this.deviceInfo = null;
		this.end();
	});
}

Machine.prototype.isConnected = function() {
	return this.deviceInfo;
}

Machine.prototype.close = function(cb) {
	console.log("Close application");
	this.deviceInfo = null;
	this.ads.end();
}

Machine.prototype.plcStart = function() {
	this.ads.writeControl({state: 5, deviceState: 0}, function() {});
	this.plcReadState();
}

Machine.prototype.plcStop = function() {
	this.ads.writeControl({state: 6, deviceState: 0}, function() {});
	this.plcReadState();
}

Machine.prototype.plcReset = function() {
	this.ads.writeControl({state: 2, deviceState: 0}, function() {});
	this.plcReadState();
}

Machine.prototype.plcReadState = function() {
	var self = this;
	this.ads.readState(function(err, res) {
		self.plcState = res.state;
		self.on.notification.call(self, 'plc', res.state);
	});
}

Machine.prototype.readAll = function(obj) {
	for (var key in obj) {
		if (obj[key].symname) {
			this.ads.read(obj[key], function(err, handle) {
				if (err) console.log(err);
			});
		} else if (Object.keys(obj[key]).length != 0) {
			this.readAll(obj[key]);
		}
	}
}

Machine.prototype.getActualState = function() {
	var struct = this.struct;
	var obj = {
		emStop: struct.emStop.value,
		axisActive: struct.axisActive.value,
		status: struct.status.value,
		errStatus: struct.errStatus.value,
		errId: struct.errId.value,
		plcState: this.plcState,
		onLaser: struct.onLaser.value,

		Axis: {
			x1: {},
			x2: {},
			y: {},
			z: {},
			h: {},
			q: {}
		},

		NC: {
			point1: struct.NC.point1.value,
			point2: struct.NC.point2.value,
			jogDistance: struct.NC.jogDistance.value,
			velocity: struct.NC.velocity.value,
			acceleration: struct.NC.acceleration.value,
			deceleration: struct.NC.deceleration.value
		},

		NCI: {
			serviceType: struct.NCI.serviceType.value,
			path: struct.NCI.path.value,
			serviceProgramPath: struct.NCI.serviceProgramPath.value,
			chooseAxis: struct.NCI.chooseAxis.value,
			chooseSubAxis: struct.NCI.chooseSubAxis.value,
			work: struct.NCI.work.value,
			mainChannel: struct.NCI.mainChannel.value,
			status: struct.NCI.status.value,
			errChannel: struct.NCI.errChannel.value,
			errStatus: struct.NCI.errStatus.value,
			errId: struct.NCI.errId.value
		}
	};

	['x1', 'x2', 'y', 'z', 'h', 'q'].forEach(function(i) {
		obj.Axis[i].state = +struct.Axis[i].state.value;
		obj.Axis[i].errCode = +struct.Axis[i].errCode.value;
		obj.Axis[i].actPos = +struct.Axis[i].actPos.value;
		obj.Axis[i].actVelo = +struct.Axis[i].actVelo.value;
		obj.Axis[i].setPos = +struct.Axis[i].setPos.value;
		obj.Axis[i].setVelo = +struct.Axis[i].setVelo.value;
		obj.Axis[i].setAcc = +struct.Axis[i].setAcc.value;
		obj.Axis[i].ncStatus = +struct.Axis[i].ncStatus.value;
		obj.Axis[i].ncErrStatus = +struct.Axis[i].ncErrStatus.value;
		obj.Axis[i].ncErrId = +struct.Axis[i].ncErrId.value;
	});

	return obj;
}

Machine.prototype.emergencyStop = function() {
	this.write(this.struct.emStop, !this.struct.emStop.value, function() {
		console.log("Emergency stop: " + this.struct.emStop);
	});
}

Machine.prototype.write = function(handle, value, cb) {
	handle.value = value;

	this.ads.write(handle, function(err) {
		if (!cb) cb.call(this);
	});
}

Machine.prototype.activeAxis = function(axis) {
	
	this.struct.axisActive.value = axis;
	this.struct.start.value = true;
	var start = this.struct.start;

	console.log(this.struct.axisActive);

	this.ads.write(this.struct.axisActive, function(err) {
		console.log( err );
		this.write(start, function(err) {
			console.log( err );
			console.log("Start main!!!!");
		});
	});
}

Machine.prototype.cWrite = function(arr) {
	var self = this;

	arr.forEach(function(i) {
		self.ads.write(i, function(err) {});
	});
}

/////////////////////// NC FUNCTIONS ///////////////////////

Machine.prototype.toPoint1 = function(data) {
	this.struct.NC.point1.value[data.axis] = data.point;
	this.struct.NC.velocity.value[data.axis] = data.velo;
	this.struct.NC.toPoint1.value = {x1: false, x2: false, y: false, z: false, h: false, q: false};
	this.struct.NC.toPoint1.value[data.axis] = true;

	console.log(data);
	console.log(this.struct.NC.toPoint1);

	this.ads.write(this.struct.NC.point1, function(err) {});
	this.ads.write(this.struct.NC.velocity, function(err) {});
	this.ads.write(this.struct.NC.toPoint1, function(err) {});
}

Machine.prototype.toPoint2 = function(data) {
	this.struct.NC.point2.value[data.axis] = data.point;
	this.struct.NC.velocity.value[data.axis] = data.velo;
	this.struct.NC.toPoint2.value = {x1: false, x2: false, y: false, z: false, h: false, q: false};
	this.struct.NC.toPoint2.value[data.axis] = true;

	this.ads.write(this.struct.NC.point2, function(err) {});
	this.ads.write(this.struct.NC.velocity, function(err) {});
	this.ads.write(this.struct.NC.toPoint2, function(err) {});
}

Machine.prototype.betweenPoint = function(data) {
	this.struct.NC.point1.value[data.axis] = data.point1;
	this.struct.NC.point2.value[data.axis] = data.point2;
	this.struct.NC.velocity.value[data.axis] = data.velo;
	this.struct.NC.betweenPoint.value = {x1: false, x2: false, y: false, z: false, h: false, q: false};
	this.struct.NC.betweenPoint.value[data.axis] = true;

	this.ads.write(this.struct.NC.point1, function(err) {});
	this.ads.write(this.struct.NC.point2, function(err) {});
	this.ads.write(this.struct.NC.velocity, function(err) {});
	this.ads.write(this.struct.NC.betweenPoint, function(err){});
}

Machine.prototype.oneJogForward = function(data) {
	this.struct.NC.jogDistance.value[data.axis] = data.jogDistance;
	this.struct.NC.velocity.value[data.axis] = data.velo;
	this.struct.NC.oneJogForward.value = {x1: false, x2: false, y: false, z: false, h: false, q: false};
	this.struct.NC.oneJogForward.value[data.axis] = true;

	console.log(data);
	console.log(this.struct.NC.oneJogForward);

	this.ads.write(this.struct.NC.jogDistance, function(err) {});
	this.ads.write(this.struct.NC.velocity, function(err) {});
	this.ads.write(this.struct.NC.oneJogForward, function(err) {});
}

Machine.prototype.oneJogBackward = function(data) {
	this.struct.NC.jogDistance.value[data.axis] = data.jogDistance;
	this.struct.NC.velocity.value[data.axis] = data.velo;
	this.struct.NC.oneJogBackward.value = {x1: false, x2: false, y: false, z: false, h: false, q: false};
	this.struct.NC.oneJogBackward.value[data.axis] = true;

	console.log(this.struct.NC.oneJogBackward);

	this.ads.write(this.struct.NC.jogDistance, function(err) {});
	this.ads.write(this.struct.NC.velocity, function(err) {});
	this.ads.write(this.struct.NC.oneJogBackward, function(err) {});
}

Machine.prototype.jogForward = function(data) {
	this.struct.NC.jogForward.value = {x1: false, x2: false, y: false, z: false, h: false, q: false};
	this.struct.NC.jogForward.value[data.axis] = true;

	this.ads.write(this.struct.NC.jogForward, function(err) {});	
}

Machine.prototype.jogBackward = function(data) {
	this.struct.NC.jogBackward.value = {x1: false, x2: false, y: false, z: false, h: false, q: false};
	this.struct.NC.jogBackward.value[data.axis] = true;

	this.ads.write(this.struct.NC.jogBackward, function(err) {});
}

Machine.prototype.stopAxis = function(axis) {	
	this.struct.NC.stop.value = {x1: false, x2: false, y: false, z: false, h: false, q: false};
	this.struct.NC.stop.value[axis] = true;

	this.ads.write(this.struct.NC.stop, function(err) {
		if (err) console.log(err);
	});
}

Machine.prototype.resetAxis = function(axis) {
	this.struct.NC.reset.value = {x1: false, x2: false, y: false, z: false, h: false, q: false};
	this.struct.NC.reset.value[axis] = true;

	this.ads.write(this.struct.NC.reset, function(err) {});
}

/////////////////////// FTP FUNCTIONS ///////////////////////

Machine.prototype.connectToFtp = function() {
	var self = this;

	if (!this.ftp) {
		this.ftp = new ftp();

		this.ftp.on('ready', function() {
			this.list(function(err, list) {
				if (err) console.log(err);

				list.forEach(function(i) {
					var r = i.name.split('.');
					if (r[r.length-1] == 'nc') {
						self.ncProgram.push(i.name);

						self.ftp.get(i.name, function(err, stream) {
							if (err) throw err;
							stream.pipe(fs.createWriteStream('./machine/folder_ftp/' + i.name));
						})
					}
				});

				self.on.ftpConnected.call(this);
			});
		});

		this.ftp.on('error', this.on.ftpError);
		this.ftp.connect(this.ftpOpt);
	}
}

Machine.prototype.getListNCProgram = function() {
	return this.ncProgram;
}

Machine.prototype.getNCProgram = function(name, cb) {
	var contents = fs.readFileSync('./machine/folder_ftp/' + name, 'utf8');
	return contents;
}

Machine.prototype.createNCProgram = function(filename, content) {

}

Machine.prototype.updateNCProgram = function(file, content) {
	var n = './machine/folder_ftp/' + file;
	fs.writeFileSync(n, content, 'utf8');

	this.ftp.put(n, file, function(err, list) {
		if (err) console.log(err);
	});
}

/////////////////////// NCI FUNCTIONS ///////////////////////

Machine.prototype.setAxis = function(data) {
	var self = this;

	data.forEach(function(item, index) {
		self.struct.NCI.chooseAxis.value[index] = item;
	});
	console.log(this.struct.NCI.chooseAxis);

	this.ads.write(this.struct.NCI.chooseAxis, function() {});
}

Machine.prototype.nciPause = function() {
	this.struct.NCI.pause.value = true;

	this.ads.write(this.struct.NCI.pause, function(err) {});
}

Machine.prototype.nciInterrupt = function() {
	this.struct.NCI.interrupt.value = true;

	this.ads.write(this.struct.NCI.interrupt, function(err) {});
}

Machine.prototype.nciOneLineMode = function(filename) {
	this.struct.NCI.oneLineMode.value = true;
	this.struct.NCI.path.value = "C:\\Inetpub\\ftproot\\" + filename;

	this.ads.write(this.struct.NCI.path, function(err) {});
	this.ads.write(this.struct.NCI.oneLineMode, function(err) {});
}

Machine.prototype.nciNextBlock = function() {
	this.struct.NCI.nextBlock.value = true;

	this.ads.write(this.struct.NCI.nextBlock, function(err) {});
}

Machine.prototype.nciStart = function(filename) {
	this.struct.NCI.nciStart.value = true;
	this.struct.NCI.path.value = "C:\\Inetpub\\ftproot\\" + filename;

	this.ads.write(this.struct.NCI.path, function(err) {});
	this.ads.write(this.struct.NCI.nciStart, function(err) {});
}

Machine.prototype.nciStop = function() {
	this.struct.NCI.nciStop.value = true;

	this.ads.write(this.struct.NCI.nciStop, function(err) {});
}

Machine.prototype.nciService = function() {
	this.struct.NCI.service.value = true;

	this.ads.write(this.struct.NCI.service, function(err) {});
}

Machine.prototype.nciConfirmationError = function() {
	this.struct.NCI.confirmationError.value = true;

	this.ads.write(this.struct.NCI.confirmationError, function(err) {});
}

Machine.prototype.nciConfirmationService = function() {
	this.struct.NCI.confirmationService.value = true;

	this.ads.write(this.struct.NCI.confirmationService, function(err) {});
}

/////////////////////// ANY FUNCTIONS ///////////////////////

Machine.prototype.setEncoderFrequency = function(val) {
	this.struct.ANY.encoderFrequency.value = val;

	this.ads.write(this.struct.ANY.encoderFrequency, function(err) {});
}

Machine.prototype.getEncoderFrequency = function(val) {
	return this.ads.ANY.encoderFrequency.value;
}

Machine.prototype.laserStart = function(val) {
	//{on: true|false, value: 0..255}
	var self = this;
	this.struct.laserPower.value = val.value * 3276;
	this.struct.onLaser.value = val.on;

	this.ads.write(this.struct.onLaser, function(err) {
		self.ads.write(self.struct.laserPower, function(err) {});
	});
}