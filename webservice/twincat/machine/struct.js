var ads = require('./ads');

var Handle = {
	//Запуск основной программы(Сбрасывается после установки)
	start: 			{symname: 'MAIN.bBegin', bytelength: ads.BOOL},
	emStop: 		{symname: 'MAIN.bEmStop', bytelength: ads.BOOL},
	axisActive: 	{symname: '.AxisActive', bytelength: ads.AXIS_BOOL},
	//Состояние основной программы
	status: 		{symname: '.Axis_status', bytelength: ads.UINT},
	//Состояние state машины в котором случилась ошибка
	errStatus:  	{symname: '.Error_axis_status', bytelength: ads.UINT},
	//Номер ошибки, расшифровка в документации
	errId: 	 		{symname: '.Error_axis_id', bytelength: ads.UDINT},
	selectedVm: 	{symname: '.B_DEBUG_VM', bytelength: ads.BOOL},

	laserPower: 	{symname: 'PRG_Lazer.Power', bytelength: ads.INT},
	onLaser: 		{symname: '.LazerOn', bytelength: ads.BOOL},

	Axis: {
		x1: {
			//Состояние оси(init, отключена, NC, NCI....)
			state: 		{symname: '.fb_axis[3].state', bytelength: ads.INT},
			errCode:    {symname: '.iAxis[3].NcToPlc.ErrorCode', bytelength: ads.DWORD},
			actPos: 	{symname: '.iAxis[3].NcToPlc.ActPos', bytelength: ads.LREAL},
			actVelo: 	{symname: '.iAxis[3].NcToPlc.ActVelo', bytelength: ads.LREAL},
			setPos: 	{symname: '.iAxis[3].NcToPlc.SetPos', bytelength: ads.LREAL},
			setVelo: 	{symname: '.iAxis[3].NcToPlc.Setvelo', bytelength: ads.LREAL},
			setAcc: 	{symname: '.iAxis[3].NcToPlc.SetAcc', bytelength: ads.LREAL},

			ncStatus: 	{symname: '.NC_status[3]', bytelength: ads.INT},
			ncErrStatus:{symname: '.Error_nc_status[3]', bytelength: ads.INT},
			ncErrId: 	{symname: '.Error_nc_id[3]', bytelength: ads.UDINT},
		}, 
		x2: {
			state: 		{symname: '.fb_axis[2].state', bytelength: ads.INT},
			errCode:    {symname: '.iAxis[2].NcToPlc.ErrorCode', bytelength: ads.DWORD},
			actPos: 	{symname: '.iAxis[2].NcToPlc.ActPos', bytelength: ads.LREAL},
			actVelo: 	{symname: '.iAxis[2].NcToPlc.ActVelo', bytelength: ads.LREAL},
			setPos: 	{symname: '.iAxis[2].NcToPlc.SetPos', bytelength: ads.LREAL},
			setVelo: 	{symname: '.iAxis[2].NcToPlc.Setvelo', bytelength: ads.LREAL},
			setAcc: 	{symname: '.iAxis[2].NcToPlc.SetAcc', bytelength: ads.LREAL},

			ncStatus: 	{symname: '.NC_status[2]', bytelength: ads.INT},
			ncErrStatus:{symname: '.Error_nc_status[2]', bytelength: ads.INT},
			ncErrId: 	{symname: '.Error_nc_id[2]', bytelength: ads.UDINT},
		}, 
		y: {
			state: 		{symname: '.fb_axis[1].state', bytelength: ads.INT},
			errCode:  	{symname: '.iAxis[1].NcToPlc.ErrorCode', bytelength: ads.DWORD},
			actPos: 	{symname: '.iAxis[1].NcToPlc.ActPos', bytelength: ads.LREAL},
			actVelo: 	{symname: '.iAxis[1].NcToPlc.ActVelo', bytelength: ads.LREAL},
			setPos: 	{symname: '.iAxis[1].NcToPlc.SetPos', bytelength: ads.LREAL},
			setVelo: 	{symname: '.iAxis[1].NcToPlc.Setvelo', bytelength: ads.LREAL},
			setAcc: 	{symname: '.iAxis[1].NcToPlc.SetAcc', bytelength: ads.LREAL},

			ncStatus: 	{symname: '.NC_status[1]', bytelength: ads.INT},
			ncErrStatus:{symname: '.Error_nc_status[1]', bytelength: ads.INT},
			ncErrId: 	{symname: '.Error_nc_id[1]', bytelength: ads.UDINT},
		}, 
		z: {
			state: 		{symname: '.fb_axis[4].state', bytelength: ads.INT},
			errCode:  	{symname: '.iAxis[4].NcToPlc.ErrorCode', bytelength: ads.DWORD},
			actPos: 	{symname: '.iAxis[4].NcToPlc.ActPos', bytelength: ads.LREAL},
			actVelo: 	{symname: '.iAxis[4].NcToPlc.ActVelo', bytelength: ads.LREAL},
			setPos: 	{symname: '.iAxis[4].NcToPlc.SetPos', bytelength: ads.LREAL},
			setVelo: 	{symname: '.iAxis[4].NcToPlc.Setvelo', bytelength: ads.LREAL},
			setAcc: 	{symname: '.iAxis[4].NcToPlc.SetAcc', bytelength: ads.LREAL},

			ncStatus: 	{symname: '.NC_status[4]', bytelength: ads.INT},
			ncErrStatus:{symname: '.Error_nc_status[4]', bytelength: ads.INT},
			ncErrId: 	{symname: '.Error_nc_id[4]', bytelength: ads.UDINT},
		}, 
		h: {
			state: 		{symname: '.fb_axis[5].state', bytelength: ads.INT},
			errCode: 	{symname: '.iAxis[5].NcToPlc.ErrorCode', bytelength: ads.DWORD},
			actPos: 	{symname: '.iAxis[5].NcToPlc.ActPos', bytelength: ads.LREAL},
			actVelo: 	{symname: '.iAxis[5].NcToPlc.ActVelo', bytelength: ads.LREAL},
			setPos: 	{symname: '.iAxis[5].NcToPlc.SetPos', bytelength: ads.LREAL},
			setVelo: 	{symname: '.iAxis[5].NcToPlc.Setvelo', bytelength: ads.LREAL},
			setAcc: 	{symname: '.iAxis[5].NcToPlc.SetAcc', bytelength: ads.LREAL},

			ncStatus: 	{symname: '.NC_status[5]', bytelength: ads.INT},
			ncErrStatus:{symname: '.Error_nc_status[5]', bytelength: ads.INT},
			ncErrId: 	{symname: '.Error_nc_id[5]', bytelength: ads.UDINT},
		}, 
		q: {
			state: 		{symname: '.fb_axis[6].state', bytelength: ads.INT},
			errCode:  	{symname: '.iAxis[6].NcToPlc.ErrorCode', bytelength: ads.DWORD},
			actPos: 	{symname: '.iAxis[6].NcToPlc.ActPos', bytelength: ads.LREAL},
			actVelo: 	{symname: '.iAxis[6].NcToPlc.ActVelo', bytelength: ads.LREAL},
			setPos: 	{symname: '.iAxis[6].NcToPlc.SetPos', bytelength: ads.LREAL},
			setVelo: 	{symname: '.iAxis[6].NcToPlc.Setvelo', bytelength: ads.LREAL},
			setAcc: 	{symname: '.iAxis[6].NcToPlc.SetAcc', bytelength: ads.LREAL},

			ncStatus: 	{symname: '.NC_status[6]', bytelength: ads.INT},
			ncErrStatus:{symname: '.Error_nc_status[6]', bytelength: ads.INT},
			ncErrId: 	{symname: '.Error_nc_id[6]', bytelength: ads.UDINT},
		}
	},

	NC: {
		stop:               {symname: '.NC_stop', bytelength: ads.AXIS_BOOL},
	    toPoint1:           {symname: '.NC_to_point1', bytelength: ads.AXIS_BOOL},
	    toPoint2:           {symname: '.NC_to_point2', bytelength: ads.AXIS_BOOL},
	    betweenPoint:       {symname: '.NC_between_point', bytelength: ads.AXIS_BOOL},
	    oneJogForward:      {symname: '.NC_one_jog_forward', bytelength: ads.AXIS_BOOL},
	    oneJogBackward:     {symname: '.NC_one_jog_backward', bytelength: ads.AXIS_BOOL},
	    jogForward:         {symname: '.NC_jog_forward', bytelength: ads.AXIS_BOOL},
	    jogBackward:        {symname: '.NC_jog_backward', bytelength: ads.AXIS_BOOL},
	    setVelocityForward: {symname: '.NC_set_velocity_forward', bytelength: ads.AXIS_BOOL},
	    setVelocityBackward:{symname: '.NC_set_velocity_backward', bytelength: ads.AXIS_BOOL},
	    setNewPosition:     {symname: '.NC_set_new_position', bytelength: ads.AXIS_BOOL},
	    reset:              {symname: '.NC_bReset', bytelength: ads.AXIS_BOOL},

	    point1:             {symname: '.NC_point1', bytelength: ads.AXIS_LREAL},
	    point2:             {symname: '.NC_point2', bytelength: ads.AXIS_LREAL},
	    jogDistance:        {symname: '.NC_jog_distance', bytelength: ads.AXIS_LREAL},
	    velocity:           {symname: '.NC_velocity', bytelength: ads.AXIS_LREAL},
	    acceleration:       {symname: '.NC_acceleration', bytelength: ads.AXIS_LREAL},
	    deceleration:       {symname: '.NC_deceleration', bytelength: ads.AXIS_LREAL},
	    newPosition:        {symname: '.NC_new_position', bytelength: ads.AXIS_LREAL},
	},

	NCI: {
		//Пауза без разборки канала (работает для обоих каналов)
		pause:            	{symname: '.bExError', bytelength: ads.BOOL},
		//Прерывание канала – пауза с разборкой канала и переводом осей в NC режим
	    interrupt:          {symname: '.bInterrupt', bytelength: ads.BOOL},
	    //Запуск программы в режими OneLine
	    oneLineMode:        {symname: '.bOneLineMode', bytelength: ads.BOOL},
	    //Выполнение следующего блока в режиме OneLine
	    nextBlock:          {symname: '.bNextBlock', bytelength: ads.BOOL},
	    //Обычный запуск программы
	    nciStart:           {symname: '.bNciStart', bytelength: ads.BOOL},
	    //остановка каналов с полным разбором и освобождением осей, при повторном запуске программа начнется с начала. Если NCI машина находится в ошибки, то данная кнопка сбросит ошибку.
	    nciStop:            {symname: '.bNciStop', bytelength: ads.BOOL},
	    //выполнение выбранной сервис программы (iServiceType)
	    service:            {symname: '.bService', bytelength: ads.BOOL},
	    //квитирование ошибки или прерывание, если было прерывание, то станок сначала вернется в исходное состояние, а затем продолжит выполнение основной программы
	    confirmationError:  {symname: '.bConfirmationError', bytelength: ads.BOOL},
	    //квитирование выполнение сервисной программы (по идеи может выполнятся автоматически), возможно только после того, как сервисная программа закончит свое выполнение.
	    confirmationService:{symname: '.bConfirmationService', bytelength: ads.BOOL},

	    //выбранная сервисная программа
	    serviceType:        {symname: '.iServiceType', bytelength: ads.INT},
	    //путь для основной программы
	    path:               {symname: '.sPath', bytelength: ads.STRING},
	    //путь для сервистных программ
	    serviceProgramPath: {symname: '.ServiceProgrammPath', bytelength: ads.STRING}, //array string
	    chooseAxis:         {symname: 'PRG_NCI_Control.ChooseAxis', bytelength: ads.SERVICE_UINT}, //array uint
	    chooseSubAxis:      {symname: 'PRG_NCI_Control.ChooseSubAxis', bytelength: ads.SERVICE_UINT}, //array uint

	    work:            {symname: '.bNciWork', bytelength: ads.BOOL},
	    mainChannel:     {symname: '.bNciMainChannel', bytelength: ads.BOOL},
	    status:          {symname: '.NCI_status', bytelength: ads.INT},
	    errChannel:    	 {symname: '.Error_nci_channel', bytelength: ads.UINT},
	    errStatus:       {symname: '.Error_nci_status', bytelength: ads.UINT},
	    errId:           {symname: '.Error_nci_id', bytelength: ads.UDINT},
	},

}

function addNameHandle(obj, str) {
	for (var key in obj) {
		if (obj[key].symname) {
			obj[key].nameHandle = str + key;
		} else if (Object.keys(obj[key]).length != 0) {
			addNameHandle(obj[key], str + key + '.');
		}
	}
}

addNameHandle(Handle, "");


module.exports = function() {

	var obj = JSON.stringify(Handle);
	obj = JSON.parse(obj);

	return obj;
}

