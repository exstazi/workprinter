#ifndef VIEWMAIN_H
#define VIEWMAIN_H

#include <QWidget>
#include <QTimer>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>

namespace Ui {
class ViewMain;
}

class ViewMain : public QWidget
{
    Q_OBJECT

public:
    explicit ViewMain(QWidget *parent = 0);
    ~ViewMain();

protected slots:
    void queryData();

private slots:
    void on_btnStartStop_clicked();
    void on_btnReverse_clicked();
    void on_btnKlapan1_clicked();
    void on_btnKlapan2_clicked();
    void on_btnKlapan3_clicked();
    void on_lnSpeedMotor_textChanged(const QString &arg1);
    void on_lnSpeedNasos_textChanged(const QString &arg1);

    void on_readyRead();

    void on_btnConnect_clicked();

    void on_btnReflsesh_clicked();

private:
    Ui::ViewMain *ui;

    char data;
    unsigned char speedMotor;
    unsigned char speedNasos;

    QTimer *timer;
    QSerialPort *port;
};

#endif // VIEWMAIN_H
