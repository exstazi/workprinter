#-------------------------------------------------
#
# Project created by QtCreator 2016-07-04T16:34:16
#
#-------------------------------------------------

QT       += core gui
QT       += serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = TerminalControlPump
TEMPLATE = app


SOURCES += main.cpp\
        viewmain.cpp

HEADERS  += viewmain.h

FORMS    += viewmain.ui
