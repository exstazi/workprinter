#include "viewmain.h"
#include "ui_viewmain.h"

#include <QDebug>

ViewMain::ViewMain(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ViewMain)
{
    ui->setupUi(this);

    port = new QSerialPort(this);

    connect(port, SIGNAL(readyRead()), this, SLOT(on_readyRead()) );

    foreach (QSerialPortInfo port, QSerialPortInfo::availablePorts()) {
        ui->listComPort->addItem( port.portName() );
    }

    timer = new QTimer(this);
    connect(timer,SIGNAL(timeout()),this,SLOT(queryData()));
    timer->start(1000);

    data = 0x00;
    speedMotor = 0x00;
    speedNasos = 0x00;
}

void ViewMain::queryData()
{
    if (port->isOpen()) {
        QByteArray arr;
        arr.append(data);
        arr.append(speedMotor);
        arr.append(speedNasos);
        port->write(arr);
        port->flush();
        qDebug() << "send";
    }
}

void ViewMain::on_readyRead()
{
    QByteArray arr = port->readAll();
    unsigned char answer = arr.at(arr.size()-1);
    if (answer & (1<<7)) {
        ui->btnStartStop->setText("Старт");
    } else {
        ui->btnStartStop->setText("Стоп");
    }
    if (answer & (1<<6)) {
        ui->btnReverse->setText("Прямой");
    } else {
        ui->btnReverse->setText("Обратный");
    }
    if (answer & (1<<5)) {
        ui->btnKlapan1->setText("Открыть");
    } else {
        ui->btnKlapan1->setText("Закрыть");
    }
    if (answer & (1<<4)) {
        ui->btnKlapan2->setText("Открыть");
    } else {
        ui->btnKlapan2->setText("Закрыть");
    }
    if (answer & (1<<3)) {
        ui->btnKlapan3->setText("Открыть");
    } else {
        ui->btnKlapan3->setText("Закрыть");
    }
    if (answer & (1<<2)) {
        ui->lbBobber->setText("Замкнут");
    } else {
        ui->lbBobber->setText("Разомкнут");
    }
}


ViewMain::~ViewMain()
{
    delete ui;
    port->close();
    delete port;
}

void ViewMain::on_btnStartStop_clicked()
{
    if (data & (1<<7)) {
        data &= ~(1<<7);
        ui->btnStartStop->setText("Старт");
    } else {
        data |= (1<<7);
        ui->btnStartStop->setText("Стоп");
    }
}

void ViewMain::on_btnReverse_clicked()
{
    if (data & (1<<6)) {
        data &= ~(1<<6);
        ui->btnReverse->setText("Прямой");
    } else {
        data |= (1<<6);
        ui->btnReverse->setText("Обратный");
    }
}

void ViewMain::on_btnKlapan1_clicked()
{
    if (data & (1<<5)) {
        data &= ~(1<<5);
        ui->btnKlapan1->setText("Открыть");
    } else {
        data |= (1<<5);
        ui->btnKlapan1->setText("Закрыть");
    }
}

void ViewMain::on_btnKlapan2_clicked()
{
    if (data & (1<<4)) {
        data &= ~(1<<4);
        ui->btnKlapan2->setText("Открыть");
    } else {
        data |= (1<<4);
        ui->btnKlapan2->setText("Закрыть");
    }
}

void ViewMain::on_btnKlapan3_clicked()
{
    if (data & (1<<3)) {
        data &= ~(1<<3);
        ui->btnKlapan3->setText("Открыть");
    } else {
        data |= (1<<3);
        ui->btnKlapan3->setText("Закрыть");
    }
}


void ViewMain::on_lnSpeedMotor_textChanged(const QString &arg1)
{
    speedMotor = arg1.toInt();
    ui->lnSpeedMotor->setText( QString::number(speedMotor) );
}

void ViewMain::on_lnSpeedNasos_textChanged(const QString &arg1)
{
    speedNasos = arg1.toInt();
    ui->lnSpeedNasos->setText( QString::number(speedNasos));
}

void ViewMain::on_btnConnect_clicked()
{
    if (port->isOpen() ) {
        port->close();
        ui->btnConnect->setText("Connect");
    } else {
        port->setPortName(ui->listComPort->currentText());
        if (port->open(QIODevice::ReadWrite)) {
            if (port->setBaudRate(QSerialPort::Baud9600)
                    && port->setDataBits(QSerialPort::Data8)
                    && port->setParity(QSerialPort::NoParity)
                    && port->setStopBits(QSerialPort::OneStop)
                    && port->setFlowControl(QSerialPort::NoFlowControl))
            {
                if (port->isOpen())
                {
                    qDebug() << (port->portName() + " >> Открыт!");
                    ui->btnConnect->setText("Disconnect");
                }
            } else {
                port->close();
                qDebug() << port->errorString();
              }
        } else {
            port->close();
            qDebug() << port->errorString();
        }
    }
}

void ViewMain::on_btnReflsesh_clicked()
{
    ui->listComPort->clear();
    foreach (QSerialPortInfo port, QSerialPortInfo::availablePorts()) {
        ui->listComPort->addItem( port.portName() );
    }
}
