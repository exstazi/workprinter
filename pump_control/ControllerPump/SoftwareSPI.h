#ifndef SOFTWARE_SPI_H
#define SOFTWARE_SPI_H

#define SPI_CS 10
#define SPI_MOSI 11 
#define SPI_MISO 12
#define SPI_SCK 13

#define ADDRESS 0x11

class SoftwareSPI
{
public:
	static void init();
	static void write(uint8_t value);

private:
	static bool isInit;
};

bool SoftwareSPI::isInit = false;

void SoftwareSPI::init() {
	if (!isInit) {
		pinMode( SPI_CS, OUTPUT );
		pinMode( SPI_MOSI, INPUT );
		pinMode( SPI_MISO, OUTPUT );
		pinMode( SPI_SCK, OUTPUT );

		digitalWrite( SPI_CS, 1 );
		digitalWrite( SPI_MOSI, 1 );
		digitalWrite( SPI_CS, 1 );
	}
}

void SoftwareSPI::write(uint8_t value) {
	if (isInit) {
		digitalWrite( SPI_SCK, 0 );
		digitalWrite( SPI_CS, 0 );
		uint8_t addr = ADDRESS;
		for(uint8_t i = 0; i < 8; i++) {
		    if(((addr >> 7) & 1)) {
			    digitalWrite( SPI_MOSI, 1 );
		    } else {
		    	digitalWrite( SPI_MOSI, 0 );
		    }
		    digitalWrite( SPI_SCK, 1 );  
		    delayMicroseconds(1);
		    digitalWrite( SPI_SCK, 0 );   
		    delayMicroseconds(1);
		    addr <<= 1;
		}
		for(uint8_t i = 0; i < 8; i++) {
		    if(((value >> 7) & 1)) {
			    digitalWrite( SPI_MOSI, 1 );
		    } else {
		    	digitalWrite( SPI_MOSI, 0 );
		    }
		    digitalWrite( SPI_SCK, 1 );  
		    delayMicroseconds(1);
		    digitalWrite( SPI_SCK, 0 );   
		    delayMicroseconds(1);
		    value <<= 1;
		}
		digitalWrite( SPI_CS, 1 );
	}
}

#endif
