#include "ControlAlgoritms.h"

Pump *_pump = new Pump();
MaterialSupplySystem *_system = new MaterialSupplySystem( _pump );
ControlAlgrotims _algoritm( _system );

#define PIN_LOGIC_1 A3
#define PIN_LOGIC_2 A2
#define PIN_LOGIC_3 A1
#define PIN_LOGIC_4 A0

uint8_t data;

void setup() {
  Serial.begin(9600);
  pinMode( PIN_LOGIC_1, INPUT );
  pinMode( PIN_LOGIC_2, INPUT );
  pinMode( PIN_LOGIC_3, INPUT );
  pinMode( PIN_LOGIC_4, INPUT );
}

void loop() {
  _system->startMotor( 230 );
  data = 0x00;
  data = digitalRead( PIN_LOGIC_1 );
  data |= digitalRead( PIN_LOGIC_2 ) << 1;
  data |= digitalRead( PIN_LOGIC_3 ) << 2;
  data |= digitalRead( PIN_LOGIC_4 ) << 3;

  _algoritm.setAlgoritm( data );
  Serial.print( "Selected algoritm = " );
  Serial.println( data );

  _algoritm.execute();
  delay( 1000 );
}

/*
unsigned char data;
unsigned char speedMotor;
unsigned char speedNasos;

#define STATE_MANUAL_CONTROL 0x01
#define STATE_CHANGE_SPEED_MOTOR 0x02
#define STATE_CHANGE_SPEED_PUMP 0x04
uint8_t state = 0b00000000;


#define COMMAND_MANUAL_CONTROL 0xE0
#define COMMAND_AUTO_CONTROL 0xC0
#define COMMAND_WRITE_DATA 0xA0
#define COMMAND_WRITE_SPEED_MOTOR 0x80
#define COMMAND_WRITE_SPEED_NASOS 0x60

void checkSerial() {
  if (Serial.available() > 0 ) {
    uint8_t command = Serial.read();
    if (command == COMMAND_MANUAL_CONTROL) {
      state |= 0x01;
    } else if (command == COMMAND_AUTO_CONTROL) {
      state &= ~(0x01);
    } else if (Serial.available() > 0) {
      if (command = COMMAND_WRITE_DATA ) {
        data = Serial.read();
      } else if (command == COMMAND_WRITE_SPEED_MOTOR) {
        speedMotor = Serial.read();
      } else if (command == COMMAND_WRITE_SPEED_NASOS) {
        speedNasos = Serial.read();
      }
    }    
  }  
}
*/
