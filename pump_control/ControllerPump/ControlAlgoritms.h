#ifndef CONTROL_ALGORITMS_H
#define CONTROL_ALGORITMS_H

#define DELAY_ON_VALVE 5000
#include "MaterialSupplySystem.h"

class ControlAlgrotims
{
public:
	ControlAlgrotims(MaterialSupplySystem *systems);

	void execute();
	void setAlgoritm(uint8_t algoritm);

private:
	void executeFillingAlgoritm();
	void executeWorkAlgrotim();
	void executeCleaningAlgrotim();

public:
	static const uint8_t ALGORTIM_FILLING = 0x01;
	static const uint8_t ALGORTIM_WORK = 0x02;
	static const uint8_t ALGORITM_CLEANING = 0x03;
	static const uint8_t ALGORTIM_CLEANING_BANKS = 0x04;

private:
	uint8_t _currentAlgoritm;
	MaterialSupplySystem *_system;
  
  unsigned long _time;
	uint8_t _state;
};

ControlAlgrotims::ControlAlgrotims(MaterialSupplySystem *systems) {
	_system = systems;
	_currentAlgoritm = 0x00;
}

void ControlAlgrotims::setAlgoritm(uint8_t algoritm) {
  if (_currentAlgoritm != algoritm ) {
    _state = 0xFF;
    execute();
    _currentAlgoritm = algoritm;
    _state = 0x00;
  }	
}

void ControlAlgrotims::execute() {
	_system->execute();
	switch (_currentAlgoritm) {
		case ALGORTIM_FILLING:
			executeFillingAlgoritm();
		break;

		case ALGORTIM_WORK:
			executeWorkAlgrotim();
		break;

		case ALGORITM_CLEANING:
			executeCleaningAlgrotim();
		break;

    default:
      _system->stopPump();
      _system->stopMotor();
      _system->openValve( MaterialSupplySystem::VALVE_1 );
      _system->closeValve( MaterialSupplySystem::VALVE_2 );
      _system->openValve( MaterialSupplySystem::VALVE_3 );
    break;
	}
}

void ControlAlgrotims::executeFillingAlgoritm() {
	switch (_state) {
		case 0x00:
      Serial.println( "Filling algoritm. State 0" );
			_system->openValve( MaterialSupplySystem::VALVE_1 );
			_system->closeValve( MaterialSupplySystem::VALVE_2 );
			_state = 0x01;
      _time = millis();
		break;

    case 0x01:
      if (micros() - _time > DELAY_ON_VALVE ) {
        _state = 0x02;
      }
    break;

		case 0x02:    
      Serial.println( "Filling algoritm. State 1" );
			_system->startPump( MaterialSupplySystem::FORWARD_DIRECTION );

			if (_system->getStateBobber()) {
				_state = 0x03;
			}
		break;

		case 0x03:    
      Serial.println( "Filling algoritm. State 2" );
      if (_system->getPump()->getStatusWork() 
              && _system->getPump()->getStatusReverse() == MaterialSupplySystem::REVERSE_DIRECTION) {
         _state = 0x04;
         _time - millis();
         break;  
      }
      _system->startPump( MaterialSupplySystem::REVERSE_DIRECTION );   
			
		break;

    case 0x04:
      if ( (millis() - _time) > DELAY_ON_VALVE  ) {
        _system->closeValve( MaterialSupplySystem::VALVE_1 );
        _system->openValve( MaterialSupplySystem::VALVE_2 );
        _system->openValve( MaterialSupplySystem::VALVE_3 );
        _state = 0x05;
      }
    break;

    case 0x05:
      if (!_system->getStateBobber()) {
        _state = 0x06;
        _system->stopPump();
        _time = millis();
      }
    break;

    case 0x06:
      if (millis() - _time > DELAY_ON_VALVE) {
        _state = 0x00;
      }
    break;
    
    case 0xFF:
      _system->stopPump();
    break;
	}
}

void ControlAlgrotims::executeWorkAlgrotim() {
	switch (_state) {
		case 0x00:
			_system->openValve( MaterialSupplySystem::VALVE_1 );
			_system->openValve( MaterialSupplySystem::VALVE_2 );
			_system->closeValve( MaterialSupplySystem::VALVE_3 );
			_state = 0x01;
		break;

		case 0x01:
			if (_system->getStateBobber()) {
				_system->stopPump();
			} else {
				_system->startPump( MaterialSupplySystem::FORWARD_DIRECTION );
			}
		break;

    case 0xFF:
      _system->stopPump();
      _system->stopMotor();
    break;
	}	
}

void ControlAlgrotims::executeCleaningAlgrotim() {
	_system->closeValve( MaterialSupplySystem::VALVE_1 );
	_system->openValve( MaterialSupplySystem::VALVE_2 );
	_system->closeValve( MaterialSupplySystem::VALVE_3 );
	_system->startPump( MaterialSupplySystem::REVERSE_DIRECTION );
}

#endif
