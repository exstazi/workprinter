#ifndef PUMP_H
#define PUMP_H

#include "SoftwareSPI.h"

#define PIN_CONTROL_START 7
#define PIN_CONTROL_DIRECTION 8

#define PIN_STATUS_WORK A7
#define PIN_STATUS_DIRECTION A6

#define BORDER 800

class Pump 
{
public:
  Pump();  
  void execute();

  void setWork(bool value);
  void setDirection(bool value);
  void setSpeed(uint8_t value);

  bool getStatusWork();
  bool getStatusReverse();

private:
  bool _work;
  bool _direction;
  uint8_t _speed;
};

Pump::Pump() {
  pinMode( PIN_CONTROL_START, OUTPUT );
  pinMode( PIN_CONTROL_DIRECTION, OUTPUT );

  pinMode( PIN_STATUS_WORK, INPUT );
  pinMode( PIN_STATUS_DIRECTION, INPUT );
  _work = _direction = false;
  _speed = 0;
  digitalWrite( PIN_CONTROL_START, 0 );
  digitalWrite( PIN_STATUS_DIRECTION, 1 );
  SoftwareSPI::init();
}

bool Pump::getStatusWork() {
  return ( analogRead( PIN_STATUS_WORK ) < BORDER );
}

bool Pump::getStatusReverse() {
  return ( analogRead( PIN_STATUS_DIRECTION ) > BORDER );
}

void Pump::setWork(bool value) {
  _work = value;
}

void Pump::setDirection(bool value) {
  _direction = value;
}

void Pump::setSpeed(uint8_t value) {
  _speed = value;
  SoftwareSPI::write( _speed );
}

void Pump::execute() {  
    Serial.print( "Work = " );
    Serial.print( _work );
    Serial.print( " status = " );
    Serial.print( getStatusWork() );
    Serial.print( " Direction = " );
    Serial.print( _direction );
    Serial.print( " status = " );
    Serial.println( getStatusReverse() );
  
  if (getStatusReverse() != _direction) {
    while( getStatusWork() ) {
      digitalWrite( PIN_CONTROL_START, 1 );
      delay( 10 );
      digitalWrite( PIN_CONTROL_START, 0 );
      delay( 100 );
    }
    digitalWrite( PIN_CONTROL_DIRECTION, 0 );
    delay( 10 );
    digitalWrite( PIN_CONTROL_DIRECTION, 1 );
    
    return ;
  }
  if (getStatusWork() != _work) {    
    digitalWrite( PIN_CONTROL_START, 1 );
    delay( 10 );
    digitalWrite( PIN_CONTROL_START, 0 );
  }
}

#endif
