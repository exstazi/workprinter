#ifndef MATERIAL_SUPPLY_SYSTEM
#define MATERIAL_SUPPLY_SYSTEM

#include "Pump.h"

#define PIN_BOBBER A5
#define TIME_BOBBER 100

#define PIN_MOTOR 9

#define VALUE_OPEN_VALVE_1 1
#define VALUE_CLOSE_VALVE_1 0
#define VALUE_OPEN_VALVE_2 0
#define VALUE_CLOSE_VALVE_2 1
#define VALUE_OPEN_VALVE_3 1
#define VALUE_CLOSE_VALVE_3 0


class MaterialSupplySystem 
{
public:
	MaterialSupplySystem(Pump *pump);

	void startPump(bool direction = FORWARD_DIRECTION, uint8_t speed = 0);
	void stopPump();
  Pump* getPump();
  
	void startMotor(uint8_t speed);
	void stopMotor();

	bool getStateBobber();

	void openValve(uint8_t valve);
	void closeValve(uint8_t valve);

	void execute();

public:
	static const bool FORWARD_DIRECTION = true;
	static const bool REVERSE_DIRECTION = false;

	static const uint8_t VALVE_1 = 2;
	static const uint8_t VALVE_2 = 3;
	static const uint8_t VALVE_3 = 4; 

private:
	bool _stateBobber;
	uint8_t _speedMotor;
	Pump *_pump;

	bool _lastValueBobber;
	unsigned int _lastTimeBobber;
};

Pump* MaterialSupplySystem::getPump() {
  return _pump;
}

MaterialSupplySystem::MaterialSupplySystem(Pump *pump) {
  _pump = pump;
	pinMode( PIN_MOTOR, OUTPUT );
	pinMode( PIN_BOBBER, INPUT );
	digitalWrite( PIN_BOBBER, 0 );
	pinMode( VALVE_1, OUTPUT );
	pinMode( VALVE_2, OUTPUT );
	pinMode( VALVE_3, OUTPUT );

  openValve( VALVE_1 );
  closeValve( VALVE_2 );
  openValve( VALVE_3 );
  

	_stateBobber = _lastValueBobber = false;
	_lastTimeBobber = micros();
}


void MaterialSupplySystem::startPump(bool direction, uint8_t speed) {
	if (speed != 0) {
		_pump->setSpeed( speed );
	}
	_pump->setDirection( direction );
	_pump->setWork( true );
}

void MaterialSupplySystem::stopPump() {
	_pump->setWork( false );
}

void MaterialSupplySystem::startMotor(uint8_t speed) {
	analogWrite( PIN_MOTOR, speed );
}

void MaterialSupplySystem::stopMotor() {
	analogWrite( PIN_MOTOR, 0 );
}

bool MaterialSupplySystem::getStateBobber() {
	return _stateBobber;
}

void MaterialSupplySystem::openValve(uint8_t valve) {
	switch (valve) {
		case VALVE_1:
			digitalWrite( VALVE_1, VALUE_OPEN_VALVE_1 );
		break;
		case VALVE_2:
			digitalWrite( VALVE_2, VALUE_OPEN_VALVE_2 );
		break;
		case VALVE_3:
			digitalWrite( VALVE_3, VALUE_OPEN_VALVE_3 );
		break;
	}
}

void MaterialSupplySystem::closeValve(uint8_t valve) {
	switch (valve) {
		case VALVE_1:
			digitalWrite( VALVE_1, VALUE_CLOSE_VALVE_1 );
		break;
		case VALVE_2:
			digitalWrite( VALVE_2, VALUE_CLOSE_VALVE_2 );
		break;
		case VALVE_3:
			digitalWrite( VALVE_3, VALUE_CLOSE_VALVE_3 );
		break;
	}
}

void MaterialSupplySystem::execute() {
	_pump->execute();

	bool bobber = digitalRead( PIN_BOBBER );
	if (bobber != _lastValueBobber ) {
		_lastValueBobber = _lastValueBobber?false:true;
		_lastTimeBobber = micros();
	}

	if (micros() - _lastTimeBobber > TIME_BOBBER ) {
		_stateBobber = bobber;
	}
}

#endif
