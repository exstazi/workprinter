void setup() {
  pinMode(9, OUTPUT);
  pinMode(A0, INPUT);
  Serial.begin(9600);
  TCCR1B = TCCR1B & 0b11111000 | 0x01;
  analogWrite(9, 250);
}

void loop() {
  Serial.println(analogRead(A0));
} 
